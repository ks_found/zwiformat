# Zwiformat notes

## Intro (what the script does)
The `zwiformat.rb` script accepts raw HTML output (plus scans and images) from ABBYY FineReader PDF 15-16 OCR Editor and fixes particular problems with it, hopefully outputting a well-formatted HTML file and ZWI file. It fixes a wide variety of issues, but requires a rather specific kind of input. Some edits will still have to be performed by hand and cannot be feasibly automated, and proofreading will still be recommended since it is highly unlikely that any scan, treated by the ABBYY OCR process and then by this script, will be mistake-free without proofreading.

At present, you might not be able to use this without guidance from the script's author. The script works without issue only when given very carefully-prepared input, and is not particularly forgiving when given files that are not prepared correctly. That said, when the files are prepared correctly, it can produce good ZWI files, and submit them to an encyclosphere aggregator, very quickly.

## How to use (including how to install and run)
### Prerequisites
- This is a command-line program running at the BASH prompt. There is no GUI yet.
- `zwiformat.rb` was written and tested in Linux (Ubuntu), and it has been tested in macOS; not sure whether it would work in Windows. Please let me know!
- Confirm that Ruby is installed: `ruby -v` (or `rvm current` if you have installed Ruby using rvm) should show a version more recent than 3.0.

### Prepare your directory and files
- **These instructions are only an outline.** See under the "Script file structure/components" heading below for an illustration of the following! For more details on precisely what the script does, what the user should have done with AFR before running the script, and what files and directories are expected to be in place, see "[OEDP Policies and Procedures.pdf](https://gitlab.com/ks_found/zwiformat/-/blob/main/OEDP%20Policies%20and%20Procedures.1.3.pdf)" as well as "[Pseudo-tags.pdf](https://gitlab.com/ks_found/zwiformat/-/blob/main/Pseudo-tags.pdf)"—without which you will not be able to prepare the necessary files.
- **Prepare your directories and subdirectories** as follows. The script assumes that the general organizational structure of the editorial system for a particular encyclopedia volume, including these components:
  - **An `originals/` folder,** which contains the original articles or groups of articles.
  - **Many subfolders of `originals/`,** one per article or group of articles. By "group of articles" we mean a single HTML page containing a bunch of different shorter articles.
  - Each article subfolder (that you want to use `zwiformat.rb` to process) should have
    - **An HTML file.** This is the raw HTML output of an ABBYY OCR editing process (.htm is the default extension, but .html is fine too). Note, it *is* acceptable for an HTML file to contain many different articles, as long as each as enclosed in `[[article]]...[[/article]]` before you run `zwiformat.rb` on the file. The script will be automatically dividing the page into multiple different article folders, one for each article, inside the `zwifolders/` directory.
    - **A `scans/` folder,** with a different image scan corresponding to each page from which the HTML was generated (the scan pages *can* include other articles, but must include all of the article(s) in the HTML file). The folder name must be lowercase. These must be one image per page, and they must named in the same order as the pages to which they correspond, but do not need to have the page numbers on them. There should not be any extraneous pages (even if they are pages from the original text) *if they do not have page numbers,* in the folder.
    - **An images folder** (if there are images linked within the HTML) with the same name that ABBYY gave it. To test that this has been named correctly, view the HTML in a browser: if the images can be seen in the article when viewed in the browser, good.
  - In the top-level directory (so, just inside the parent directory of `originals/`), place:
    - **A `metadata.json` file.** This is in the *grandparent* directory of each article directory (and the parent directory of both `originals` and `zwifolders`), representing a template for each individual metadata.json file for each article in the encyclopedia. (See below for more detailed notes.)
    - **An `info.json` file** as a sibling of the `metadata.json` template; this can also be created by the script. (See below for more detailed notes.)
  - **Place a DID-compliant `identity/` folder** in the same directory in which Zwiformat is installed, containing `identity.json`, `publish.private.jwk`, and `publish.public.jwk`. Of these three, only `publish.private.jwk` is used at present, but the others might be used in the future. See [the DID:PSQR website](https://vpsqr.com/) for tools to generate these three files for your domain.
- Note, the script will create `zwifolders/` for you, as a sibling of `originals/`.
- **"Mark up" the text with pseudo-tags**, either while still editing the file in ABBYY, or editing the HTML, such as `[[article]]`, `[[desc]]`, `[[title]]`, and `[[author]]`. Careful: the script can easily crash if it it encounters a pseudo-tag without an expected match. See [Pseudo-tags.pdf](https://gitlab.com/ks_found/zwiformat/-/blob/main/Pseudo-tags.pdf).
- **You might want a searchable PDF version of the article pages** for your own use, or maybe run the script while ABBYY is open. The script does not use such a PDF (yet!), but you might want to refer to it.

### Starting and using the script
- **Install gems.** You will also have to install the required "gems" (Ruby libraries). To do this, execute `bundle install` on the command line. Before doing that, you might or might not need to install or update the Ruby "bundler" gem itself (at the command line, execute `sudo gem install bundler`).
- **To run the script,** execute `ruby zwiformat.rb path/to/folder`, at the BASH prompt, in the directory where `zwiformat.rb` (and its components, and your HTML file etc.) is installed. The `path/to/folder/` should give the path to a subdirectory of `originals/` or `zwifolders/` (depending on what you're working on at the time). You might have named the folder after a particular article or group of articles (but all contained within a single HTML file). After you have run the script once on a particular folder, you should have access to it via the paths you have input, stored in the `paths` file (created by the script in the same folder where you have installed `zwiformat.rb`).
- **The `paths` file.** You can leave off `path/to/folder/` in the above for any article (or group of articles, in `originals/`) you are revisiting, because any previously-visited article will be included shown as an option at startup time. The list of paths to articles is maintained in `paths`, in the same directory where you have installed `zwiformat.rb`. You can feel free to edit the `paths` document, which is a text file with the URLs of folders that `zwiformat.rb` expects to be able to edit.
- Dos and don'ts:
  - **Do** feel free to edit the most recent draft, between executions of the script. The script assumes it's working with messy (but ABBYY-generated) HTML, and it takes whatever it finds in the highest-numbered draft.
  - **Please do not** create new, higher-numbered drafts yourself, because that will probably cause the script to stop functioning.
  - **Do not** edit the draft you are working on by hand while the script is running (before a new draft is saved, at the end), because the script will ignore any changes you make by hand. If you find you need to make changes by hand, make a list of them and make them after the script is done; or, stop the script (with CTRL-C; you won't hurt anything) and make the changes, and then re-run the script.
  - **Do** carefully follow the prompts and on-screen commentary. Note, `<Enter>` does not always mean "Yes"; it generally means "do the thing you'll want to do next, probably." You will want to read the prompts carefully until you're familiar with them.

## Steps executed by the script (with notes for the user)
This script
1. loads the most recent draft in the present (or a supplied) directory, or a file according to path and name,
2. does other preparatory work on css, scans, etc., directories, creating various if they do not exist, starting interpretation of the pseudo-tags and particularly `[[article]]`, grabbing the `metadata.json` for the ZWI file from the grandparent directory, etc.,
3. if the document is marked up as having more than one article, it is split into separate directories, which are placed in `zwifolders/`, and individual articles too are placed in `zwifolders/` after preparation, and immediately after that, interprets `[[title]]` and `[[desc]]` markup,
4. repeatedly iterates over a plaintext-only version and fixes some obvious and easily-fixable issues with the text (including fixing problems in which ABBYY missed dehyphenating words broken across lines, or wrongly dehyphenated them),
5. repeatedly iterates over a full HTML version of the file, line by line, and fixes simpler issues with AFR's messy and old-fashioned HTML,
6. makes more complex changes to the HTML by manipulating the object created by Nokogiri, the big Ruby scraper, especially page numbering, fixing broken paragraphs, renaming, marking up, footnotes, placing images properly, and spell checking, and finally
7. saves the results to a new draft<n>.html file on prompt, and (if the user wishes) to a new, properly-signed ZWI file for final drafts. Users who wish to make changes by hand are expected to find the latest draft by searching for draft<n>.html for the highest *n*; but it is also acceptable and supported to edit old drafts you can find in `archive/drafts/`. Note also, article.html is *not* overwritten with each draft; it is overwritten only when the user says to save a new ZWI file.

## Summary of functions
- **Makes metadata.json.** Grabs template from a parent directory and, throughout the script, inserts data into it as appropriate. In the end, properly formats and saves the file in `zwi/`.
- **Does rudimentary version control.** The script pays attention to what it has done with each draft and saves old drafts and associated data in an `archive/` folder.
- **Makes directories.** Creates various directories, including all directories needed for ZWI files.
- **Organizes scans.** Renames scans into standardized names and deposits them in `zwi/data/media/images/scans/` folder.
- **Converts article and description markup.** Inserts `<article>` tag as appropriate, and uses markup to determine where to start copying text for an article description.
- **Manages `<head>` meta tags.** This includes author, generator, and description `<meta>` tags.
- **Pagination.** Automatically determines page numbers and places corresponding HTML markup in the right place in the text; when it fails to do this, it helps the user determine where to hand-place the markup.
- **Article separation.** Separates individual articles into separate folders, after creating `zwifolders/` if necessary. Creates needed folders and copies all and only needed files. The originals remain in `originals/`. Later, renames the created article folder based on the article title.
- **Creates links to images per pseudo-tags.** In the relatively rare case in which the article needs to refer to something like a pronunciation guide, script grabs a copy of the needed image, saves it in the `zwi/` directory, and links to it in the text.
- **Marks up title and author.** Working with user input and pseudo-tags, the script extracts the correct title(s) and author data and (a) marks them up in the article body, (b) adds them as appropriate to the `<head>` tags, and (c) saves the appropriate data in several metadata.json fields.
- **Makes various text changes.** Based on our experience with AFR output, the script fixes or prompts the user to fix hyphenation, some numeral and letter issues, and punctuation issues.
- **Updates HTML.** The HTML output of AFR 16 is quite messy and old-fashioned. The script converts markup for nonbreaking spaces, italics, bold, small caps, and font-related markup, removes cruft, and removes some inconsistent indentation.
- **Updates CSS.** The stylesheet that AFR prepares is in the HTML `<head>` and is badly-made. The script deletes this outdated CSS, copies a new stylesheet, and links to it from the HTML.
- **Fixes broken paragraphs.** Especially useful when broken across columns and pages.
- **Formats footnotes/endnotes.** The script works with user-supplied pseudo-tags to move footnotes and endnotes to an inline position inside nicely-formatted boxes.
- **Prepares images.** Wraps images in `<figure>` and converts `[[cap]]` pseudo-tags into `<figcaption>` tags. Renames image files based on article name and caption, changes the names in the HTML, and moves images where they belong in the `zwi/` folder (while saving old copies in `archive/`). Also makes images clickable.
- **Interprets subarticle markup.** When a file has one or more self-contained articles within a longer article (complete with title), they can be marked up; the script will then create an anchor link to the article and put the title and description in metadata so the subarticles can appear properly in searches.
- **Checks spelling.** Catches issues that AFR 16 misses.
- **Preps article plaintext.** And saves it in `zwi/`.
- **Prepares/copies other ZWI files.** Moves a copy of the latest draft into `zwi/` and prepares media.json (a required list of the various `data/` directory files).
- **Signs the ZWI file.** Works with the contents of Zwiformat’s identity/ directory to make a DID:JSON-compliant signature, which it saves zwi/ as signature.json.
- **Creates ZWI file.** Actually zips up the directory to make a KSF standards-compliant ZWI file.
- **Review-and-approve sequence.** Enables the user to review each scan in turn and remove the file from the paths (like “Recent Documents”) list.
- **Pushes ZWI to aggregator.** User defines aggregator (in `info.json`; see below) and aggregator password, enabling user to to push ZWI file to aggregator and automatically navigate to the article on the aggregator website.

## Special required data files
These are basically encyclopedia volume-specific configuration files that are required in order for Zwiformat to work. The following are some notes on how to prepare these files.
### metadata.json
The best way to create a `metadata.json` file for a particular volume is to work from a good example, which someone from the KSF should be able to give you. You can also try constructing one from scratch by consulting [the ZWI documentation site](https://docs.encyclosphere.org). Basically, you should fill out all information that applies to all articles for a particular volume. It *must* be placed in the grandparent (parent-of-the-parent) directory of any HTML file that is being worked on by Zwiformat.
### info.json
This file must contain the following information, in json format:
- `NextID`: The ID that will be assigned to the *next* article. For now, this corresponds simply to the `<article>` class. We do not yet know how these IDs might be used, but it seems like a good idea to track them in the HTML (and possibly, in the future, in a ZWI's metadata as well). This number is managed by Zwiformat. Note, if you ever have to re-separate articles (i.e., move them from `originals/` to `zwifolders/`), this number should be reset by hand; it will not be reset by the script.

The file must contain these fields if you wish to push ZWI files to an aggregator:
- `AggregatorURL`: This is the URL of an encyclosphere aggregator that accepts submitted ZWI files via KSF networking standards.
- `AggregatorPasswordIdentifier`: This is either the password itself (not recommended) that was assigned to you (or, probably, to this particular encyclopedia) by the aggregator itself; or (better) it is the environment variable that you (i.e., the person running Zwiformat) have put in a file such as `.bashrc`, where you are saving said password.

These fields are used/defined by KSF programming personnel:
- `ImageLinkPseudoTags`: These are ad hoc pseudo-tags (for details on which, see the last section of "[Pseudo-tags.pdf](https://gitlab.com/ks_found/zwiformat/-/blob/main/Pseudo-tags.pdf)").
- `Regexen`: These will have to be maintained by a Ruby developer (probably Larry). They are volume-specific regexen (and one boolean) that help Zwiformat to determine what the `ShortTitle`s of articles in a volume probably are.

## Script file structure/components
### An example file directory
- `A_Big_Encyclopedia`
  - `originals` [the starting-point of your post-ABBYY edits]
    - `Article_Title_1` [not much here]
      - `OriginalUglyFilename_files` [original pix with original filenames]
      - `scans` [your scans, best if they're tiffs]
        - `long_ugly_scan_name1`.tiff
        - `long_ugly_scan_name2`.tiff [etc.]
      - `OriginalUglyFilename.htm` [output by AFR]
    - `Article_Title_2`
      - `SameRequirementsAsAbove.html`
    - `WhateverYouWant`
    - `titleheredoesnotmatter`
    - `ContentsVaryButThereAreMinimumRequirements`
  - `zwifolders` [zwiformat.rb-created working directories]
    - `Article_Title_1` [named after article; lots here; created by script]
      - `archive`
        - `drafts`
          - `OriginalUglyFilename_files` [original pix with original names]
          - `draft1.html`
          - `draft2.html` [etc.]
          - `OriginalUglyFilename.htm`
        - `oldscans` [your scans with original filenames]
          - `long_ugly_scan_name1.tiff` [etc.]
        - `tesstexts`
          - `34.txt` [etc.; based on page numbers; used by script]
        - `tesstext_with_cols.txt` [used by script, ignore]
      - `data` [copy of `zwi/data/`; needed to make latest draft looks nice]
        - etc.
      - `zwi`
        - `data`
          - `css`
            - `poststyle.css`
          - `media`
            - `images`
              - `images-renamed-by-script-1.jpg`
              - `no-touchee-pleasee-2.jpg`
            - `scans`
              - `23.tiff` [only scans of pages for *this* article]
              - `24.tiff`
              - `25.tiff`
        - `article.html` [the final produced HTML]
        - `article.txt` [plaintext, for search engines]
        - `media.json` [a list of media files and hashes thereof]
        - `metadata.json` [data about the article; essential to ZWI file]
        - `signature.json` [the DID-compliant signature]
      - `Article_Title.zwi` [the zwi output!]
      - `draft3.html` [based on original HTML file (which is in drafts/)]
      - `editdata.json` [tracks whether article has finished script sections]
      - `spellerrors.txt` [as it says]
      - `separation_data.json` [used by script]
    - `Article_Title_2`
    - `Article_Title_3`
    - `Article_Another`
    - `Titles_Are_Based_on_Title_of_Article_in_Metadata`
    - `Created_by_the_Script`
    - `Do_Not_Attempt_to_Create_by_Hand`
    - `One_Article_Only_in_Each_Zwifolder`
  - `info.json` [configuration file for this particular encyclopedia]
  - `metadata.json` [template to be copied into ZWI files created for this encyclopedia]
  - `feel`
  - `free`
  - `to`
  - `add`
  - `more_files`
  - `and_folders` [for your own, or the project's, convenience]
- `MoreHereIfYouLike` [top-level encyclopedia directory can be quite varied]


## Maintenance notes
**Thoroughly commented!**

Note that certain parts of the script are quite complex, which explains the quantity of comments there. If this script is successful, it will be helpful in fixing a very wide variety of common problems with HTML files generated by ABBYY, particularly problems with public domain (old) encyclopedias. With each new article and especially each new encyclopedia, we can expect this script to get longer and more complex. Especially in early stages of development, many changes will need to be made, as more and more issues and edge cases are discoveres and fixed automatically. Hence it is very important that everything be very well commented to permit easy updating.
