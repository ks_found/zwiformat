def prepare_metadata
# This does a few things:
# (1) Converts [[title]] & [[author]] -> <title> & <section class="author">
# (2) Converts image-linking pseudo-tags.
# (3) Tries to populate, as appropriate, the metadata fields ShortTitle, Disambiguator, CrossReference, CrossReferenceTitle
# (4) Deals with subarticles: [[section]] and [[subtitle]] tags.


# (1) AUTHOR & TITLE.
# Possibilities to cover include: (a) neither HTML5 nor pseudo-tags exist
# (so, the user should be asked to supply the title, and is asked to add
# <span>s himself); (b) the pseudo-tags are present (thus the data should
# be extracted, confirmed, and saved in $metadata, and the tags should be
# replaced by the HTML5 tags); (c) the HTML5 tags are present but the data
# is not in $metadata (thus it should be saved); (d) finally, if the HTML5
# tags are present and the data that they indicate matches that currently
# in $metadata, report the fact to the user and otherwise do nothing. The
# order in which these things should be done are: check for/load [[]],
# HTML5 tags, and $metadata; then do in order (a), (b), (c), and then (d).
#
# Thus this is one of the most logically complex parts of the script, and
# might still have unaddressed edge cases.


# COLLECT TITLE AND AUTHOR DATA. Check for and load pseudo-tags, HTML tags,
# and $metadata. Do nothing with this yet.
puts "\nAnalyzing title and author data..."
metadata_title_confirmed = false
metadata_authors_confirmed = false
title_span_tag_present = false
authors_span_tag_present = false
title_pseudo_tags_present = false
authors_pseudo_tags_present = false
metadata_title = ''                 # To be used for extracted info below.
metadata_authors = []               # Ditto.
tag_title = ''                      # Ditto.
tag_author = ''                     # Ditto.
# (a) Check for title and authors. (If not found, we'll go on.)
if ! $metadata['Title'].nil? and $metadata['Title'].length > 0
  metadata_title = $metadata['Title']
  metadata_title_confirmed = true
end
# Confirm author(s).
if ! $metadata['ContributorNames'].nil? and
     $metadata['ContributorNames'].count > 0
  metadata_authors_confirmed = true
  metadata_authors = $metadata['ContributorNames']
end
# (b) Check for pseudo-tags. Report to user only if found.
if ! $textblob.match(/(\[\[title\]\](.+?)\[\[\/title\]\])/i).nil? and
     $textblob.match(/(\[\[title\]\](.+?)\[\[\/title\]\])/i)[2].length
  title_pseudo_tags_present = true
  title_pseudo_tag_to_code = $1
  title_pseudo_tag_content = $2
  puts "Found [[title]] pseudo-tag."
end
if ! $textblob.match(/\[\[author\]\](.+?)\[\[\/author\]\]/i).nil? and
     $textblob.match(/(\[\[author\]\](.+?)\[\[\/author\]\])/i)[2].length
  authors_pseudo_tags_present = true
  authors_pseudo_tag_to_code = $1
  authors_pseudo_tag_content = $2
  puts "Found [[author]] pseudo-tag."
end
# (c) Check for HTML5 tags. Script assumes that at least the title will
# be there; it will check for author again.
if ! $doc.css('span.title').empty?
  tag_title = $doc.at_css('span.title').content
  title_span_tag_present = true
end
if ! $doc.css('span.author').empty?
  tag_author = $doc.at_css('span.author').content
  authors_span_tag_present = true
end
unless metadata_title_confirmed and title_span_tag_present
  puts "OK, we have work to do.\n"
else
  puts "The essential work is done.\n"
end


# (a) TITLE/AUTHOR DATA ALL MISSING: GET FROM USER
# If title/author data are missing from metadata or tags, get from user.
if ! title_span_tag_present and ! title_pseudo_tags_present
  puts rap %{*** We could not find any title markup (of the form [[title]] or <span class="title") in the text. We will not assume that whatever is in <title> is correct. A marked-up title (not just a <title> tag) is absolutely necessary for this script to continue.\n\nPlease edit the latest draft of the HTML, and add [[title]] and [[/title]] precisely where it occurs in the text (i.e., probably inside an <h1> tag or other heading, or perhaps among the very first words of a one-paragraph article.\n\nQuitting. Sorry.}
  exit
end
if ! metadata_authors_confirmed and ! authors_span_tag_present and
   ! authors_pseudo_tags_present
  puts rap %{\n*** We could not find any author markup (of the form [[author]] or <span class="author") in the text.}
  if ! $doc.at_css('meta[@name="author"]').nil? and
       $doc.at_css('meta[@name="author"]')['content'].length
    puts "\nCurrent <meta name=\"author\"> contents: #{$doc.at_css('meta[@name="author"]')['content']}"
    print "Accept with <Enter>, 'x' to reject, or type a new author: "
    answer = gets.chomp
    answer = $doc.at_css('meta[@name="author"]')['content'] if answer == ''
  else
    print "\nType the name of an author, or leave blank if none: "
    answer = gets.chomp
  end
  if answer == 'x'
    metadata_authors_confirmed = false
    $doc.at_css('meta[@name="author"]')['content'] = ''
    puts "*** Deleting author name given in <meta name=\"author\"> tag."
  else
    puts rap (answer == '') ? "Author left blank." :
                        %{Accepted '#{answer}' as the author.}
    unless answer == ''
      # Create <meta name="author"> if it doesn't exist yet.
      if $doc.at_css('meta[@name="author"]').nil?
        author_head_tag = $doc.create_element('meta')
        author_head_tag["name"] = 'author'
        author_head_tag["content"] = ''
        $doc.at_css('title').before(author_head_tag)
      end
      $doc.at_css('meta[@name="author"]')['content'] = answer
      $metadata['ContributorNames'] = [answer]
      metadata_author_confirmed = true
      authors_span_tag_present = true
    end
  end
end

# (b) PSEUDO-TAGS DETECTED; REPLACE THEM WITH HTML5 TAGS.
# Note, this is going to apply regardless of what else is done.
if title_pseudo_tags_present or authors_pseudo_tags_present
  puts "Since we detected at least one pseudo-tag, we will replace it/them."
end
if title_pseudo_tags_present
  puts "Detected [[title]]: #{title_pseudo_tag_content}"
  corrected_title = make_phrase_Title_Case(title_pseudo_tag_content)
  corrected_title = remove_pseudotags(corrected_title)
  puts "Standards-fixed title: #{corrected_title}"
  # Confirm with user that this data should be (a) converted into a <span>,
  # (b) put in <title>, and (c) put in the metadadata.
  print rap %{\n<Enter> to proceed or supply a different corrected title: }
  answer = gets.chomp
  corrected_title = answer unless answer == ''
  # Replace [[title]] with <span class="title">.
  tnode = $doc.css('p, h1, h2, h3, h4, h5, h6')
          .select{ |node| node.content.include?(title_pseudo_tag_to_code)}[0]
  tnode.inner_html = tnode.inner_html.gsub(/\[\[title\]\]/i,
                                           '<span class="title">')
  tnode.inner_html = tnode.inner_html.gsub(/\[\[\/title\]\]/i, '</span>')
  # Create <title> if it doesn't exist yet.
  if $doc.at_css('title').nil?
    new_title = $doc.create_element('title')
    new_title.content = ''
    # After the second item in the <head> ought to be the correct place.
    $doc.at_css('head').children[1].after(new_title)
  end
  # Whether or not <title> existed, replace it with content of [[title]].
  $doc.at_css('title').content = corrected_title
  $metadata['Title'] = corrected_title
  $metadata['SourceURL'] = "https://oldpedia.org/article/#{$metadata['Publisher']}/#{name_zwi}"
end
if authors_pseudo_tags_present
  puts "\nDetected [[author]]: #{authors_pseudo_tag_content}"
  corrected_authors = make_phrase_Title_Case(authors_pseudo_tag_content)
  puts "Standards-fixed byline: #{corrected_authors}"
  # Confirm with user that this data should be (a) converted into a <span>,
  # (b) put in <meta name="author">, and (c) put in the metadadata.
  print rap %{\n<Enter> to proceed or supply a different corrected author: }
  answer = gets.chomp
  corrected_authors = answer unless answer == ''
  # Replace [[author]] with <span class="author">.
  tnode = $doc.css('p, h1, h2, h3, h4, h5, h6').select{ |node|
                      node.content.include?(authors_pseudo_tag_to_code)}[0]
  tnode.inner_html = tnode.inner_html.gsub(/\[\[author\]\]/i,
                                           '<span class="author">')
  tnode.inner_html = tnode.inner_html.gsub(/\[\[\/author\]\]/i, '</span>')
  # Add to <head> in <meta name="author">.
  if ! $doc.at_css('meta[@name="author"]').nil?
    $doc.at_css('meta[@name="author"]')['content'] = corrected_authors
  # Create the node if necessary.
  else
    metael = $doc.create_element("meta")
    metael['name'] = "author"
    metael['content'] = corrected_authors
    # Don't forget to append the new element just before <title>.
    $doc.at_css('title').previous = metael
  end
  $metadata['ContributorNames'] = [corrected_authors]
end

# (c) A probably-rare case. The title_span_tag_present, but corresponding
# metadata is not in metadata.json. Maybe the <head> data doesn't match,
# either. So: ask user to confirm fixed title, move to <title>, and
# save in $metadata.
if title_span_tag_present and ! metadata_title_confirmed
  # Extract title from <span>.
  tag_title = $doc.at_css('span[@class="title"]').content
  tag_title_fixed = make_phrase_Title_Case(tag_title)
  puts "Title found in <span class=\"title\">: #{tag_title}"
  puts "In metadata, this will be: #{tag_title_fixed}"
  print rap %{\nThis needs to be copied into the metadata and <title>. Press <Enter> to approve, or anything else to quit and edit by hand: }
  answer = gets.chomp
  exit unless answer == ''    # Must approve to continue in this situation.
  $doc.at_css('title').content = tag_title_fixed
  $metadata['Title'] = tag_title_fixed
  $metadata['SourceURL'] = "https://oldpedia.org/article/#{$metadata['Publisher']}/#{name_zwi}"
  metadata_title_confirmed = true
end
# Do something similar when authors_span_tag_present.
if authors_span_tag_present and ! metadata_authors_confirmed
  if ! $doc.at_css('span[@class="author"]')
    puts rap %{NOTE: no author markup found in the article text. We will retain the data you have indicated in the metadata, but if the author's name is in the text, it should be marked up.}
  else
    # Extract author from <span>.
    tag_authors = $doc.at_css('span[@class="author"]').content
    tag_authors_fixed = make_phrase_Title_Case(tag_authors)
    puts "Author(s) found in <span class=\"author\">: #{tag_authors}"
    puts "In metadata, this will be: #{tag_authors_fixed}"
    print rap %{\nThis needs to be copied into the metadata and <meta name="author">. Press <Enter> to approve, or anything else to quit and edit by hand: }
    answer = gets.chomp
    exit unless answer == ''    # Must approve to continue in this situation.
    $doc.at_css('meta[@name="author"]')['content'] = tag_authors_fixed
    $metadata['ContributorNames'] = [tag_authors_fixed]
    metadata_authors_confirmed = true
  end
end

# (d) if metadata_title_confirmed and title_span_tag_present, check that
# they match. This is also the time to check on and edit the corresponding
# <head> meta tags; these are not treated as independent data points but
# must always be edited in keeping with whatever the user stores in metadata.
# NOTE: also checks and reports on author tag.
if title_span_tag_present and metadata_title_confirmed
  # First, process tag_title so that it matches the expected (but not forced)
  # format of the metadata.
  # Put in title case, strip any concluding period.
  little_words = %w{the a an and but of or to in by with from for as if that which}
  # If all caps, make it all lowercase. (Could be a bit better.) Assume
  # title is properly titleized unless all uppercase.
  tag_title_words = tag_title.split(' ')
  unless tag_title.gsub(/[^a-zA-Z0-9]/, '').match(/[a-z]/)
    tag_title_words.map! do |word|
      word.downcase!
      unless little_words.include? word
        word = titleize(word)
      end
      word
    end
  end
  # Rejoin words after titleizing properly.
  tag_title_edited = tag_title_words.join(' ')
  # Remove any final period.
  tag_title_edited.gsub!(/\.$/, '')
  # Ensure the first letter of the phrase is capitalized.
  tag_title_edited = tag_title_edited[0].upcase + tag_title_edited[1..-1]
  # First, see if the edited title matches the metadata. If so, tell user.
  if metadata_title == tag_title_edited
    puts "The title in <span class=\"title\"> matches the title in the metadata; good."
  else
    puts rap %{\nWe detected a discrepancy between the title in <span class=\"title\"> and the title saved in the metadata:}
    puts "Metadata title: #{metadata_title}"
    puts "Original title in text: #{tag_title}"
    puts "Would be *after expected edits*: #{tag_title_edited}"
    puts "You might want to edit one of these by hand."
  end
  # Copy metadata title into <title> if necessary, or vice-versa.
  if metadata_title != $doc.at_css('title').content
    if metadata_title.length > 0
      puts "NOTE: replacing <title> contents with title from metadata."
      puts "WAS: #{$doc.at_css('title')}"
      puts "NOW: #{metadata_title}"
      title_tag = $doc.at_css('title')
      title_tag.content = metadata_title
    else
      puts "Since metadata title was blank, changing it to <title> contents."
      $metadata['Title'] = $doc.at_css('title').content
      $metadata['SourceURL'] = "https://oldpedia.org/article/#{$metadata['Publisher']}/#{name_zwi}"
    end
  end
end
if authors_span_tag_present and metadata_authors_confirmed
  # First, process tag_authors so that it matches the expected (but not
  # forced) format of the metadata.
  tag_authors_edited = make_phrase_Title_Case(tag_author)
  # First, see if the edited author matches the metadata. If so, tell user.
  if metadata_authors.include? tag_authors_edited
    puts rap %{The marked-up author in <span class="author"> matches an author in the metadata; good.}
  else
    # NOTE: this tests only the first author. Support for multiple authors
    # will have to be added later.
    puts rap %{\nWe detected a discrepancy between the marked-up author in <span class="author"> and the author saved in the metadata:}
    puts "Metadata author(s): #{metadata_authors[0]}"
    puts "Original byline (in text): #{tag_author}"
    puts "Would be, *after expected edits*: #{tag_authors_edited}"
    puts "You might want to edit one of these by hand."
  end
  # Copy metadata title to <meta name="author"> if necessary, or vice-versa.
  unless metadata_authors[0] == $doc.at_css('meta[@name="author"]')['content']
    if metadata_authors.count > 0
      puts rap %{NOTE: replacing <meta name=\"author\"> contents with author(s) from metadata.}
      puts "WAS: #{$doc.at_css('meta[@name="author"]')['content']}"
      puts "NOW: #{metadata_authors[0]}"
      authors_tag = $doc.at_css('meta[@name="author"]')
      authors_tag['content'] = metadata_authors[0]
    elsif ! $doc.at_css('span[@class="author"]').nil?
      puts "Since metadata author was blank, changing it to <span> contents."
      corrected_authors = make_phrase_Title_Case($doc
                                    .at_css('span[@class="author"]').content)
      $metadata['ContributorNames'] = [corrected_authors]
    end
  end
end

# Commit changes.
from_doc_to_html_and_text_globals



# (2) CONVERT AD-HOC/PER-ENCYCLOPEDIA IMAGE-LINKING PSEUDOTAGS.
# These are pseudotags, specified in info.json, which specify a pointed-to
# image and some "alt" text (albeit made the content of a "title" attribute).
# For now, this is a fairly straightforward find-and-replace.
xtra = ''
# First, reload any existing info.json for this encyclopedia.
if File.exist?("#{$wkdir}../../info.json")
  $info = JSON.parse(File.read("#{$wkdir}../../info.json"))
  info_groovy = true
elsif File.exist?("#{$wkdir}../../../info.json")
  $info = JSON.parse(File.read("#{$wkdir}../../../info.json"))
  xtra = "../"          # Shameful little hack.
  info_groovy = true
else
  puts rap %{No info.json found for this encyclopedia. Any image-linking pseudo-tags will be ignored.}
end

if info_groovy
  # If there are image-linking pseudo-tags in info.json, look for such tags
  # in the HTML.
  if $info['ImageLinkPseudoTags']
    tags = $info['ImageLinkPseudoTags']
    tags.each do |tag, attribute|
      # Look for this tag.
      replaced = false # Don't tell user twice.
      $doc.css('h1, h2, h3, h4, h5, h6, p, div, span').each do |node|
        # Look for matching pair of pseudo tags.
        if node.content.match(/\[\[#{tag}\]\].+\[\[\/#{tag}\]\]/i)
          # Prepare contents of HTML tags.
          html_addition_1 =
                    %{<a href="data/media/images/#{attribute['href']}" title="#{attribute['title']}" class="clickable">}
          html_addition_2 = '</a>'
          # Actually insert HTML tags.
          node.to_html.match(/(\[\[#{tag}\]\])(.+)(\[\[\/#{tag}\]\])/i)
          node.inner_html =
                node.to_html.gsub(/(\[\[#{tag}\]\])(.+)(\[\[\/#{tag}\]\])/i, "#{html_addition_1}#{$2}#{html_addition_2}")
          puts "Replaced [[#{tag}]] with proper HTML." unless replaced
          replaced = true
          # Test if needed file exists at data/media/images/ .
          unless File.exist?("#{$wkdir}zwi/data/media/images/#{attribute['href']}")
            # If not, test to see if it exists at source location; if so,
            # move it.
            if File.exist?("#{$wkdir}#{xtra}../../#{attribute['href']}")
              FileUtils.cp("#{$wkdir}#{xtra}../../#{attribute['href']}",
                "#{$wkdir}zwi/data/media/images/#{attribute['href']}")
              puts "Copied #{attribute['href']} to images/."
            else
              puts rap %{*** WARNING: this file's [[#{tag}]] requires a file in the grandparent (the volume's top-level) directory called '#{attribute['href']}', but no such file could be found. Recommend you quit now, put the file in the needed location, and restart the script.}
              go_on
            end
          end
        end
      end
    end
  end
end

# Commit changes.
from_doc_to_html_and_text_globals



# (3) MOSTLY TITLE-RELATED METADATA FIELDS
#
# This edits just $metadata. Thus this section could be put anywhere
# after the determination of $metadata['Title'].
#
# Try to populate, as appropriate, the metadata fields ShortTitle,
# Disambiguator, CrossReference, CrossReferenceTitle
#
# The general strategy here is to do simple algorithmic tests on
# $metadata['Title'] to make suggestions for ShortTitle and Disambiguator,
# but accept only user input for these fields. The two CrossReference
# fields are also to be determined by algorithmic tests, but on
# $metadata['Description'].
#
# Specifically, this looks things up $info['Regexen'] and applies them.

#### ShortTitle ####
# This uses the $info['Regexen']['Shorttitle'] saved for this encyclopedia;
# otherwise, it looks for parentheses as the last bit of the title. If
# found, it proposes a ShortTitle and Disambiguator.

# Load $info (again).
if File.exist?("#{$wkdir}../../info.json")
  $info = JSON.parse(File.read("#{$wkdir}../../info.json"))
elsif File.exist?("#{$wkdir}../../../info.json")
  $info = JSON.parse(File.read("#{$wkdir}../../../info.json"))
end

# If no info.json here, something is wrong; script ends.
unless $info
  puts rap %{\n*** ERROR: info.json is missing but required. Please rectify the situation and re-start the script.}
  exit
end

# Extract short title; get inverted version if necessary.
# If ST already prepared, skip.
short_title = ($metadata['ShortTitle'] ? $metadata['ShortTitle'] :
               $metadata['Title'])
if $info['Regexen'] and $info['Regexen']['ShortTitle'] and
   $metadata['ShortTitle'] == ''
  # Load regex used to extract this encyclopedia's short titles.
  shorttitle_regex = Regexp.new($info['Regexen']['ShortTitle'])
  # Apply regex to extract short_title.
  $metadata['Title'].match(shorttitle_regex)
  short_title = $1 if $1
end

# If no short title yet, copy from long title.
short_title = $metadata['Title'] if short_title == ''

# If short_title is inverted, de-invert it.
if $info['Regexen'] and $info['Regexen']['ShortTitleInverted']
  short_title.match(/^(.+?)\, (.+)$/)
  short_title = $2 + " " + $1 if $1 and $2
end

# If short_title has anything to systematically exclude, apply regexen.
# Note, 'ExcludeFromShortTitle' is an ARRAY of regexen.
if $info['Regexen'] and $info['Regexen']['ExcludeFromShortTitle'] and
   short_title != ''
  excluders = $info['Regexen']['ExcludeFromShortTitle']
  excluders.each do |excluder|
    excluder = Regexp.new(excluder)
    # 'while' in order to repeatedly apply gsub.
    while short_title.match(excluder) # Regex must define a capture group.
      short_title = short_title.gsub($1, '')
    end
  end
end

# If 'LastNameCaps' is true, then last word (the family name) in short_title
# should be lowercased.
if $info['Regexen'] and $info['Regexen']['LastNameCaps'] and
   short_title != ''
  short_title_words = short_title.split(' ')
  short_title_words[-1] = titleize(short_title_words[-1])
  short_title = short_title_words.join(' ')
end

# Operations might have resulted in stray spaces.
short_title.strip!
short_title.gsub!('  ', ' ')

# Get user confirmation/correction.
puts "\nI have short title as: #{short_title}"
print "<Enter> to accept or type corrected: "
answer = gets.chomp
$metadata['ShortTitle'] = answer == '' ? short_title : answer

#### Cross-References ####
# If there is a cross-reference field among Regexen, look to see if
# article is one (i.e., matches regex). Then ask for confirmation.
if $info['Regexen'] and $info['Regexen']['CrossReference']
  xref_regex = Regexp.new($info['Regexen']['CrossReference'])
  if $doc.text.strip.match(xref_regex)
    xreftitle = $1
    puts "\nContents of this entry:\n==========\n     #{$doc.text.strip}\n=========="
    puts rap %{This entry appears to be, or to contain, a cross-reference to a "#{xreftitle}" entry. If this entry is indeed only a cross-reference (with no substantive content of its own), then please confirm. You will be able to separately edit/confirm the title.}
    print "\nIs this a cross-reference (only)? <Enter> for 'yes': "
    answer = gets.chomp
    if answer == ''
      $metadata['Redirect'] = true
      puts "\nThe title of the cross-referenced article is: '#{xreftitle}'"
      print "Correct? <Enter> for 'yes' or type title: "
      answer = gets.chomp
      $metadata['RedirectTitle'] = (answer == '' ? xreftitle : answer)
      puts "\nCross-reference title saved to the metadata: '#{$metadata['RedirectTitle']}'"
      xreftitle_confirmed = $metadata['RedirectTitle'].dup
      $metadata['RedirectURL'] =
        "https://oldpedia.org/article/#{$metadata['Publisher']}/#{name_zwi(xreftitle_confirmed)}"
      puts "Cross-reference URL saved to metadata:\n#{$metadata['RedirectURL']}"
    end
  end
end



# (4) SUBARTICLES
# Strategy:
#     (a) Replace [[subart]] pseudo-tags with <section>.
#     (b) Replace [[subtitle]] pseudotags with <span class="subtitle">.
#     (c) Add subtitle to metadata 'Subarticles' array.
#     (d) Add subarticle description to metadata too.

# (a) Replace [[subart]].
# The existing helpers0.rb method, &convert_pseudotag, should do this.
while $doc.text.match?(/\[\[subart\]\]/i)
  puts "\nFound [[subart]] pseudo-tag."
  convert_pseudotag('section')
  # Commit changes.
  from_doc_to_html_and_text_globals
end

# (b) Replace [[subtitle]] pseudotags with <span class="subtitle">.
# NOTE: This is much more easily done in html_fixes4.rb, so you'll find that
# there.

# (c) Add subtitle to metadata 'Subarticles' array.
# (d) Add subarticle description to metadata too.
# NOTE: Since these can be done only after <span class="subtitle"> is in
# place, this is saved for make_nokogiri_changes5.rb.

# Set this module as finished. Can be redone by request.
$editdata[$v]['preparedMetadata'] = true




end
