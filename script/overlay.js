    <script>
      function showOverlay(imageUrl) {
        // Create the overlay element
        var overlayElement = document.createElement("div");
        overlayElement.id = "overlay";

        // Set position fixed and full page width and height
        overlayElement.style.position = "fixed";
        overlayElement.style.top = 0;
        overlayElement.style.left = 0;
        overlayElement.style.width = "100%";
        overlayElement.style.height = "100%";

        // Create the close button element
        var closeButton = document.createElement("span");
        closeButton.innerHTML = "&times;";
        closeButton.onclick = hideOverlay;

        // Create the image element
        var imageElement = document.createElement("img");
        imageElement.src = imageUrl;

        // Add the elements to the overlay
        overlayElement.appendChild(closeButton);
        overlayElement.appendChild(imageElement);

        // Add a click listener to close the overlay when clicked outside the image
        overlayElement.addEventListener("click", function(event) {
          if (event.target === overlayElement) {
            hideOverlay();
          }
        });

        // Add the overlay to the page
        document.body.appendChild(overlayElement);

        // Show the overlay
        overlayElement.style.display = "block";
      }

      function hideOverlay() {
        // Hide the overlay
        var overlayElement = document.getElementById("overlay");
        overlayElement.style.display = "none";

        // Remove the overlay from the page
        overlayElement.parentNode.removeChild(overlayElement);
      }

    </script>
