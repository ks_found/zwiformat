def insert_page_numbers


# INSERT PAGE NUMBERS (e.g.: <a name="pg45"></a> ) ###########################
#
# Strategy: (1) use Tesseract to grab the first and last ten words of the page
# (after subtracting any matches of the title and page number), put them into
# a hash; (2) iterate over pages; in the plaintext of the current draft, look # for matches of the string; if not found, redo using Levenstein distance; if
# still not found, ask user for the last three words on p. N.; (3) insert.
#
# Generally, the strategy is to grab the first n words from each
# Tesseract output file (in 'tesstexts'). Then I'll be calculating the
# Levenshtein distance of those strings to n-word long strings taken from
# the plaintext of the draft<n>.html file. Generally speaking, I'll use a
# "good enough" match of the first n words of page x as a reason to insert
# <a name="p23"></a> or whatever. IF not found, the user is asked to
# confirm and is informed and instructed what to do (by hand). Once all
# matches are made and confirmed unique, then the text of '$doc' is edited.

# Prepare tesstext data.
tesstextfiles = Dir.glob("#{$wkdir}archive/tesstexts/*")
tesstexts = {}      # Hash: tesstexts[page number, to_i] = 'text of page'
tesstextfiles.each do |filename|
  # Extract page number as integer.
  filename.match(/archive\/tesstexts\/(\d+?)\.\w+$/)
  num = $1.to_i
  # Extract the text itself.
  thistext = File.read(filename)
  # Delete short lines at top of file: likely header stuff.
  textlines = thistext.split("\n")
  textlines.delete('')    # Delete blank lines.
  # Delete all the starting lines that are very short, unless they
  # start with "-".
  delete_these = []
  textlines.each_with_index do |l, i|
    next unless i < 5     # Consider first five lines only.
    delete_these << i if (l.length < 20 && l[0] != '-')
  end
  delete_these.reverse.each{|d| textlines.delete_at(d)}
  tesstexts[num] = textlines.join("\n")
end
tesstexts = tesstexts.sort.to_h   # Sort by page numbers.

# Get "plaintext" of current article.
fixtext = $doc.css('body').text


# Strategy pt. 1: iterate through scans; find first 6 words for each page;
# assign to pagenum.
pgstrings = {}         # Hash of tess-generated strings:
                       # pgstrings[pagenum] = [array of first six "words"]
tesstexts.keys.each do |pgnum|
  # Assign strings from current article to page numbers.
  # Get the first six "words."
  six_words = tesstexts[pgnum].split(/[\s\n]+/)[0..5]
  # Get indexes of each word.
  offset = 0
  six_words.each do |word|
    offset = tesstexts[pgnum].index(word, offset) # Finds them in order.
  end
  # This is a single, whole string, NOT an array of six strings.
  pgstrings[pgnum] = tesstexts[pgnum][0..offset+six_words[-1].length-1]
end


# Strategy pt. 2: get an array of "words" (not in tags) in the HTML.
# First, use regexen to grab all tags: <...> . Get indexes of all matches.
#
# Generate hash of all tags, keyed by index in HTML.
# First, make array of unique tags.
tags = $doc.to_html.scan(/\<.+?\>/).uniq
# Construct hash of tags, keyed to indexes.
# tagstrs[index] = '<matching string>'
tagstrs = {}
html = $doc.to_html.dup      # Do once for speed.
tags.each do |tag|
  # This magic actually finds the indexes of this word.
  indexes = html.enum_for(:scan,tag).map { Regexp.last_match.begin(0) }
  indexes.each do |i|
    # Construct the hash.
    tagstrs[i] = tag
  end
end

# Construct array of "words" in original HTML. A bit complex.
text_no_markup = $doc.to_html.dup
# Replace markup with three carets. (Helps capture whitespace properly.)
tags.each do |tag|
  text_no_markup.gsub!(tag, '^^^')
end
words = text_no_markup.split(/(?<=[^a-zA-Z0-9])\b/)
# Now remove each set of carets (etc.) from the front...
words.map! do |word|
  word.match(/^.*?\^{3,}(.+)$/m)
  if $1.nil?
    word
  else
    $1
  end
end
words = words.delete_if{|w| w == '' or w.nil?}
# ...and from the back of the "words."
words.map! do |word|
  word.match(/^(.+?)\^\^\^/)
  if $1.nil?
    word
  else
    $1
  end
end
words = words.delete_if{|w| w == '' or w.nil?}
# Now all the "words" in the article are in an array, in order, minus tags.

# Next, find the index of each "word," constructing a new hash.
# Strategy is different here. Iterate words. Find *next* matching index
# for this word (or for the first matching /\w+/).
# wordstr[index] = '<matching string>'
print "\nCollecting word indexes..."
wordstrs = {}
offset = 0
ind = 0
error = false
errors = []
words.each_with_index do |word, i|
  unless word.match(/\w/)
    offset += word.length
    next
  end
  word.match(/^(\w+)/)
  matchdis = $1
  next if matchdis.nil?
  found_index = ind
  loop do
    found_index = html.index(matchdis, offset)
    found_index.nil? ? matchdis.chop! : ind = found_index
    break if matchdis == ''
    break if ! ind.nil?
  end
  wordstrs[ind] = word
  offset = ind + word.length
  print '.' if i % 1000 == 0
  if html[ind..ind+word.length-1] != word
    error = true
    errors << [html[ind..ind+word.length-1], word]
  end
end
puts "\nDone."

# Maybe not necessary. But this is a hash of all words + tags, with
# indexes.
allstrs = tagstrs.merge(wordstrs).sort_by{|k,v| k}.to_h

# Next, make a six-word string combos (no tags) found in the HTML.
# Note, this is a string constructed from strings, not an array.
stringcombos = {}     # Hash of all six-word combos in article:
                      # stringcombos[index in HTML] = "string at that index"
(wordstrs.length - 6).times do |i|
  indexes_arr = wordstrs.keys.slice(i, 6)
  # Hopefully, the HTML index of the first word.
  top_index = wordstrs.keys[i]
  stringcombo = ''    # Build a string of words for each slice of indexes.
  # Iterate array of indexes of this slice of six.
  indexes_arr.each do |ind|
    stringcombo += wordstrs[ind]
  end
  stringcombos[top_index] = stringcombo
end


# Strategy pt. 3: iterate pagenums once more, now comparing each pgstring
# (tess-generated first six-word string) with all the stringcombos (all
# six-word combos in the HTML article); compile and sort LD scores.
# This is the main event of this tool.
fixmatches = {}       # fixmatches[pagenum] = [array of words in lowest
                      #                        LD-scoring phrase; insert
                      #                        pgnum before this]
puts "\nInserting page numbers.\nLocating start-of-page phrases..."
puts "Want to review low-confidence matches of top-of-page OCR words?"
print "<Enter> for 'yes': "
answer = gets.chomp
review_scans = (answer == '' ? true : false)
mismatch_list = []    # List of page numbers without approved matches.
pgstrings.keys.each_with_index do |pgnum, i|
  print "."
  next if i == 0      # Skip the first page, which is automatic and easy.
  ldscores = {}       # ldscores[stringcombos] = [score, index where found]
  lowscore_found = false
  lowscore_index = 0
  try_five_more_index = 0
  # Test the string for a page against all the combos, stopping five indexes
  # after a lowscore was found; generate LD scores.
  found_at_location = 0     # Used to delete indexes below this.

  # Again, this is a hash of ABBYY-outputted HTML strings:
  #   stringcombos[key, index in HTML blob] = 'string starting at that index'
  stringcombos.keys.sort.each do |key|
    htmlposs =
        stringcombos[key].downcase.gsub(/[\n\r]/, ' ').gsub('  ', ' ').strip
    tesstring =
        pgstrings[pgnum].downcase.gsub(/[\n\r]/, ' ').gsub('  ', ' ').strip
    ldscore = ld(htmlposs, tesstring)
    # I am guessing that an LD score under 12 is a sufficient upper bound to
    # find all (but not only) matches.
    ldscores[stringcombos[key]] = [ldscore, key] if ldscore < 12
    (lowscore_found = true and lowscore_index = i and
                               found_at_location = key) if ldscore < 5
    try_five_more_index += 1 if lowscore_found
    break if (lowscore_found and try_five_more_index > 5)
    break if i > 4000    # Fixes spending too much time on image pages.
                         # If too low (like 2000), whenever one appears that
                         # is over the limit, all the rest are not found!
  end

  # Shorten list of combos to test.
  if lowscore_found
    # Delete all indexes (stringcombos keys) up to the one recently found.
    stringcombos.delete_if{|k,v| k < found_at_location + 1 }
  end

  # Actually declare a winner (lowest LD score under 12).
  # This should be an array: [combo, combo_nums]
  # combo_nums itself is an array: [<LD score>, <position index in HTML>]
  (combo, combo_nums) = ldscores.sort_by {|combo, nums| nums[0]}[0].to_a
  unless combo
    mismatch_list << pgnum
    next    #!! Actually goes to top of the whole loop!
  end
  # This means fixmatches[number on page] = ["matching string", index]
  fixmatches[pgnum] = [combo, combo_nums[1]]

  # If uniqueness is questionable, allow user to confirm match, or not.
  if review_scans and ! combo_nums[0].nil?
    # Iterate only those with a score (combo_nums[0]) of 5 or higher.
    if 4 < combo_nums[0] and combo_nums[0] < 12
      puts rap %{\nPlease review for page numbering. Phrases should roughly match.}
      puts "#{pgstrings[pgnum]}  (Tesseract text)"
      puts "#{combo}  (your HTML draft text)"
      print "\nDo they match? <Enter> for 'yes', anything else for 'no': "
      answer = gets.chomp
      winner_approved = (answer == '' ? true : false)
      if ! winner_approved
        fixmatches.delete(pgnum)
        puts "*** Deleted mismatch. You'll have to add this by hand."
        mismatch_list << pgnum
        next
      else
        puts "Approved."
      end
    end
  end
end

# Let user know precisely which mismatches were *not* accepted.
if mismatch_list.count > 0
  puts rap %{\n*** PLEASE NOTE: the following pages need <a name="#pg##"></a> inserted by hand just before the first word (or partial word, or image/table) as shown in the scan for the page.\n\nAdd <a> tags BY HAND to these pages:}
  mismatch_list.each do |pgnum|
    puts "p. #{pgnum}: #{pgstrings[pgnum].gsub(/[\n\r]/, ' ').gsub(/ +/, ' ').strip}"
  end
  $editdata[$v]['pageNumbersToAdd'] = mismatch_list
end

# Actually insert the page numbers!
#
# Strategy: the procedure seems very straighforward. Since there *will* be
# mismatches due to html interpositions in some six-word strings (e.g.,
# short headers at the beginning of a page), you can't simply search for the
# string...except that you can. You start with six words. If no match, move
# to five, etc. You stop when you get an exact match. If there are duplicate
# matches, you do a look-ahead to the *next* <p> tag. If you still can't
# figure it out, you leave it up to the user, who will have to do some hand-
# placement anyway.


# Iterate through fixmatches. Actually insert page numbers.
# Remember fixmatches[pgnum] = [matching string, index in HTML]
print "\nInserting page numbers..."
pg_a_to_place_by_hand = {}
fixmatches.keys.reverse.each do |pgnum|
  print "."
  # Assumes *there is* a key for the first page.
  # Since you have the index, simply use it to insert the string.
  winstring = fixmatches[pgnum][0]
  windex = fixmatches[pgnum][1]
  html = html.insert(windex, "<a name=\"pg#{pgnum}\"></a>")
end

# Reconstruct $doc from 'html' blob.
$doc = Loofah.document(html)

# Finally, insert the first page's page number at the top.
first_page_num = tesstexts.keys.sort[0]
$doc.css('body').children.first
    .add_previous_sibling("<a name=\"pg#{first_page_num}\"></a>")


# Why page matching isn't so great:
# Things that would have to be fixed to avoid the necessity of more
# "hand-coding": (1) make sure there isn't any stray text from the adjacent
# page in a scan; (2) estimate (based on average words per page) where the
# page marker *should* be, and let the user specify the (HTML!!) string to
# search for; OR (3) use Levenstein distance matching, somehow. At this
# point, I'm not going to try any harder to fix it.

# Tell user which pages couldn't be matched. LD could help with these.
if pg_a_to_place_by_hand.nil? or pg_a_to_place_by_hand == []
  puts "\n\n*** Failed to find matches (place these by hand):"
  pg_a_to_place_by_hand.sort_by{|k,v| k}.each do |k,v|
    puts "p. #{k}: #{v}"
  end
  pg_a_to_place_by_hand.keys.each {|key|
                                   $editdata[$v]['pageNumbersToAdd'] << key}
  $editdata[$v]['pageNumbersToAdd'].sort! unless
    $editdata[$v]['pageNumbersToAdd'].nil?
end
puts rap %{\nMarking pages as numbered. Any non-numbered pages (listed above) will be recorded in 'editdata.json'.}
$editdata[$v]['pagesNumbered'] = true
go_on

# Commit changes.
from_doc_to_html_and_text_globals


end # of @insert_page_numbers #########################################



#######################################################################
# Since a completely-numbered file is OEDP policy and also required for
# dividing the document into sub-articles, determine that all pages are
# numbered. Returns 'true' if all numbers present, 'false' otherwise.
def page_numbers_all_present(quiet = false)
  # Strategy: for each scan, there should be a tesstext file and a
  # corresponding <a name="#pg<n>"></a> anchor in the html (i.e., $doc or
  # $html).

  it_is_fine = true       # Well, are they all there or not?
  # Construct array of expected strings (based on zwi/data/media/scans).
  # Get list of files.
  scanfilenames = Dir.glob("#{$wkdir}zwi/data/media/scans/*")
  scanfilenames.sort_by!{|s| s.scan(/(\d+)\.\w+$/)[0][0].to_i}
  pagestrings = scanfilenames.map do |fn|
    # Only pagenums should be in the file paths.
    num = fn.scan(/(\d+)\.\w+$/)[0][0].to_i
    [num, "<a name=\"pg#{num}\"></a>"]
  end

  # Reconstruct $editdata list of missing page numbers.
  $editdata[$v]['pageNumbersToAdd'] = []

  # Check if page strings are all present. If any are missing, print,
  # and when finished checking, exit with stern reminders to fix it.
  pagestrings.each_with_index do |data, i|
    pgnum, str = data
    prev = pagestrings[i-1][1]    # I.e., the page string for the prev page.
    # If the string can be found *anywhere* in the inner_html, that should
    # be fine.
    if $doc.inner_html.match(str).nil?
      puts "*** Not found: #{str}" unless quiet
      $editdata[$v]['pageNumbersToAdd'] << pgnum
      it_is_fine = false   # It is NOT fine, dammit!
    end
    # Check that this pagenum follows a previous pagenum.
    unless i == 0     # Test page order only beginning with second item.
      if $doc.inner_html.match(/#{prev}.*?#{str}/m).nil?
        puts "PAGE NUMBER OUT OF ORDER:"
        puts "This page number:      #{prev}"
        puts "Does not precede this: #{str}"
        it_is_fine = false
      end
    end
  end

  return it_is_fine
end
