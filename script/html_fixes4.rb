def make_basic_html_fixes



# THINGS PERFORMED ON THE WHOLE HTML BLOB (NOT AN ARRAY).
# First, create blob out of $html array.
if $editdata[$v]['htmlBlobDone']
  puts "Already did HTML blob fixes."
else
  htmlblob = $html.join("\n")

  # (1) Simple heuristic to repair some (not all) cases of broken paragraphs.
  # When a para ends with [\-\;\:\,] etc. and the next tag is a para start,
  # eliminate the paragraph break.
  broken_para_regex = /(.{25}[\-\;\:\,\—\–]\s*?)(\<\/p\>\s*?\<p\>)(.{25})/m
  broken_paras_found = false
  if htmlblob.match(broken_para_regex)
    results = htmlblob.scan(broken_para_regex)
    # Reversing is necessary since the string is edited in place.
    results.reverse.each do |result|
      firstbit = result[0]
      deleteme = result[1]
      lastbit = result[2]
      puts "Remove paragraph break (so, remove '</p><p>')?"
      puts "#{firstbit}#{deleteme}#{lastbit}"
      print "<Enter> to accept, anything else to reject: "
      answer = gets.chomp
      if answer == ''
        htmlblob.gsub!("#{firstbit}#{deleteme}#{lastbit}", "#{firstbit}#{lastbit}")
        puts "\nFixed broken paragraph."
        broken_paras_found = true
      end
    end
  end

  # (2) Repair words broken across page breaks.
  word_across_break_regex = /(\w+)(\-\s*?)(\<a name\=\"pg\d+\"\>\<\/a\>\s*?\w+)/
  word_across_break_found = false
  if htmlblob.match(word_across_break_regex)
    results = htmlblob.scan(word_across_break_regex)
    puts "\nWord(s) broken across page break(s):"
    results.each_with_index do |result, i|
      firstbit = result[0]
      deleteme = result[1]
      lastbit = result[2]
      puts "(#{i+1})" if results.length > 1
      puts "      IS:   #{firstbit}#{deleteme}#{lastbit}"
      puts "     FIX?   #{firstbit}#{lastbit}"
      print "Make change? <Enter> for 'yes': "
      answer = gets.chomp
      if answer == ''
        htmlblob.gsub!("#{firstbit}#{deleteme}#{lastbit}", "#{firstbit}#{lastbit}")
      else
        puts "Ignoring."
      end
    end
  end

  # Reconstruct $html.
  $html = htmlblob.split("\n")
  go_on if broken_paras_found
end

# THINGS PERFORMED ON A \n-SEPARATED HTML ARRAY.
# Do repeated changes to HTML *without* using Nokogiri(!).
# Yes, this should have been done with Nokogiri. But it works.
if $editdata[$v]['htmlFixesDone']
  puts "Already did automated HTML fixes."
  $fixed = $html.clone       # Need to create '$fixed' for later use if
                             # bypassing HTML fixes.
else
  puts "\nMaking automated HTML fixes..."

  # Walks through each html line ('html' not having been touched yet),
  # makes changes, commits '$fixes_to_do' from previous section to the line,
  # finally writing it to the newly-populated $fixed.
  $html.each do |line|
    # Simply get rid of ' 's (nonbreaking spaces). These should have
    # been marked up by the ABBYY user.
    # encoding=utf-8
    line.gsub!(/\u{00A0}/, '')

    # italics => <em>
    begins1 = '<span class="font'
    begins2 = '" style="font-style:italic;">'
    ends = '</span>'
    # Make repeated pass-throughs until all instances are found.
    # gsub! doesn't work.
    while line.match(/#{begins1}\d+#{begins2}(.+?)#{ends}/)
      line.sub!(/#{begins1}\d+#{begins2}(.+?)#{ends}/, "<em>#{$1}</em>")
      print "i"
    end

    # bold => <strong>
    begins1 = '<span class="font'
    begins2 = '" style="font-weight:bold;">'
    ends = '</span>'
    # Make repeated pass-throughs until all instances are found.
    # gsub! doesn't work here, dunno why.
    while line.match(/#{begins1}\d+#{begins2}(.+?)#{ends}/)
      line.sub!(/#{begins1}\d+#{begins2}(.+?)#{ends}/,
                "<strong>#{$1}</strong>")
      print "b"
    end

    # font-variant:small-caps => class="smallcaps"
    begins1 = '<span class="font'
    begins2 = '" style="font-variant:small-caps;">'
    ends = '</span>'
    # Make repeated pass-throughs until all instances are found.
    # gsub! doesn't work.
    while line.match(/#{begins1}\d+#{begins2}(.+?)#{ends}/)
      line.sub!(/#{begins1}\d+#{begins2}(.+?)#{ends}/,
                "<span class=\"smallcaps\">#{$1}</span>")
      print "s"
    end

    # Since ABBYY seems to use <span class="font<n>" ...> for inline style
    # markup, all styles should have been caught and moved to <em>, <strong>,
    # and appropriate classes such as "smallcaps".
    #
    # Unwanted span classes: e.g., "font4"
    begins = ''
    ends = ''
    while line.match(/(\<span class\=\"font\d+\"\>)(.+?)(\<\/span\>)/)
      line.sub!("#{$1}#{$2}#{$3}", $2)
      print "f"
    end

    # Insert space between comma and word: "Wyandots,or" => "Wyandots, or"
    while line.match(/([[:alpha:]]+)\,([[:alpha:]]+)/)
      line.sub!(/([[:alpha:]]+)\,([[:alpha:]]+)/, "#{$1}, #{$2}")
      print ","
    end

    # Delete nonfunctional <br clear="all">s.
    while line.match(/\<br clear\=\"all\"\>/)
      line.slice!(/\<br clear\=\"all\"\>/)
      print "c"
    end

    # Insert opening tag <small> for [[small]] pseudotag.
    while line.match(/(\[\[small\]\])/i)
      line.sub!($1, '<small>')
      print "<sm>"
    end
    # Insert closing tag </small> for [[/small]] pseudotag.
    while line.match(/(\[\[\/small\]\])/i)
      line.sub!($1, '</small>')
      print "</sm>"
    end

    # SUBARTICLES
    # On this and the following, see (4) SUBARTICLES, prepare_metadata2c.rb.
    # (b) Replace [[subtitle]] pseudotags with <span class="subtitle">.
    # Finished up in make_nokogiri_changes5.rb.
    while line.match(/(\[\[subtitle\]\])/i)
      line.sub!($1, '<span class="subtitle">')
      print "<st>"
    end
    # Insert closing tag </span> for [[/subtitle]] pseudotag.
    while line.match(/(\[\[\/subtitle\]\])/i)
      line.sub!($1, '</span>')
      print "</st>"
    end
    # Insert <a name="subdesc"></a> for [[subdesc]] pseudotag.
    while line.match(/(\[\[subdesc\]\])/i)
      line.sub!($1, '<a name="subdesc"></a>')
      print "<sd>"
    end

    # FOOTNOTES
    # When both [[r\d+]] and [[/r\d+]] are found, replace.
    # Similarly with [[n\d+]]. Adds <small> after the latter.
    # NOTE: this is finished in make_nokogiri_changes5.rb, where the note
    # itself is moved just after the reference symbol/number.
    # First, reference marks:
    while line.match(/(\[\[r(\d+)\]\]).+?(\[\[\/r\2\]\])/i)
      first = $1
      num = $2
      last = $3
      line.sub!(first, "<a name=\"footnote#{num}\"></a><a class=\"refmark clickable\" href=\"\#bookmark#{num}\">")
      line.sub!(last, "</a>")
      print "<refmark>"
    end
    # Next, corresponding note marks next to the note itself:
    while line.match(/(\[\[n(\d+)\]\]).+?(\[\[\/n\2\]\])/i)
      first = $1
      num = $2
      last = $3
      #*</a>
      line.sub!(first, "<a name=\"bookmark#{num}\"></a><a class=\"notemark clickable\" href=\"\#footnote#{num}\">")
      # NOTE: makes the rest of the note <small>. Nokogiri automatically
      # closes the tag. A bit messy but works.
      line.sub!(last, "</a><small>")
      print "<note>"
    end

    # Make user-supplied changes throughout.
    if $fixes_to_do
      $fixes_to_do.each do |pair|
        line.gsub!(pair[0], pair[1])
      end
    end
    if $uc_fixes_to_do
      $uc_fixes_to_do.each do |pair|
        line.gsub!(pair[0], pair[1])
      end
    end

    # SIMPLE FIND-AND-REPLACE GOES HERE.
    # Remove spaces from inside curly quotes, etc.
    line.gsub!(/\“ /, '“')
    line.gsub!(/\‘ /,'‘')
    line.gsub!(/ \’/,'’')
    line.gsub!(/ \”/, '”')
    line.gsub!(/ \,/, ',')
    line.gsub!(/ \?/, '?')
    line.gsub!(/ \!/, '!')
    line.gsub!(/ \;/, ';')
    line.gsub!(/ \:/, ':')
    # Clean up tables
    line.gsub!(/table border\='1'/, 'table')
    line.gsub!(/ style="vertical-align:\w+;"/, '')
    # Do more hyphens (logic in plaintext_changes3.rb removes one per line;
    # this should do the rest).
    if $info['EnDash'] == 'always'
      while line.match(/(\d+\-\d+|[a|p]\.m\.\-\d+)/)
        hyp = $1
        replacement = hyp.gsub('-', '–')
        line.gsub!(hyp, replacement)
      end
    end
    $fixed << line
  end
  $editdata[$v]['htmlFixesDone'] = true
end     # At this point, $fixed is an array of HTML lines.
puts


end
