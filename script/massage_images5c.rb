def massage_images


# Section tasks:
# (1) wrap <img> in <figure>;
# (2) after each <img>, look for a [[cap]] before encountering the next
# <img>; if found, put the enclosed text inside <figcaption> and put that
# inside the <figure>, and mark it all up properly;
# (3) rename images and alt tags in HTML, based on captions and title;
# (4) move files: make copies of image files with new names; move to
# /zwi/data/media/images; after all those are done, move directory of
# originals to /archive/drafts/<orig dirname>.


# (1) Wrap <img> in <figure> & assign 'big' or 'small'; switch to max-width.
image_nodes = $fixed.css('img')
puts "Wrapping images in <figure>..." if image_nodes.length
image_nodes.each do |image_node|
  # Wrap image in <figure>.
  fig_node = $fixed.create_element('figure')
  fig_node.inner_html = image_node.dup
  image_node.replace(fig_node)

  # Get image width.
  width = image_node.attributes["style"].text
                    .scan(/width\:(\d+)/)[0][0].to_i
  # Get height.
  height = image_node['style'].scan(/height\:(\d+)/)[0][0].to_i

  # Make image 'big' (thus aligned center) if > 350px.
  if width > 350
    fig_node.add_class('big')
  # ...otherwise class = 'small'
  else
    fig_node.add_class('small')
    # Ask if image aligned right.
    if fig_node['class'].include? 'small'
      system("open #{$wkdir}#{image_node['src']}")
      puts "\nPlease examine the image I just opened."
      print "Is this caption's image aligned (l)eft or (r)ight? "
      answer = gets.chomp
      fig_node.add_class('align-right') if answer == 'r'
      # Replace "width" > "max-width:100%", and "height" > "max-height".
      fig_node.at_css('img')['style'] =
                                  "max-width:100%;max-height:#{height}pt;"
    end
  end
  print 'f'
end


# (2) Move [[cap]] into <figure> with <figcapture>.
# Strategy: iterate over all nodes in order; whenever <figure> is spotted,
# assign it a slot in fig_arr. Then look for [[cap]]. If seen, remove
# [[cap]] plus text, and use it to construct <figcapture>. Finally, append
# to <figure>. Mark up caption as well.
fig_arr = []
$fixed.at('*').traverse do |node|                 # Iterate all nodes.
  fig_arr << [node, ''] if node.name == 'figure'  # Find latest fig (->arr).
  # Find <p> tag that contains [[cap]] and [[/cap]] (required).
  if node.name == 'p' and node.text.include? '[[cap]]' and
                          node.text.include? '[[/cap]]'

    # Grab caption IF a fig was seen AND it was open to a caption. Mark up.
    if ! fig_arr[-1][1].nil? and fig_arr[-1][1] == ''
      fc = $fixed.create_element('figcaption')    # Make <figcaption>.
      captext = node.content          # Copy contents of [[cap]] pseudo-tag.
      captext.match(/\[\[cap\]\](.+)\[\[\/cap\]\]/mi)
      # Put the contents of [[cap]] into <figcaption> node.
      fc.inner_html = captext.gsub!(/\[\[cap\]\](.+)\[\[\/cap\]\]/mi, $1)
      fig_arr[-1][1] = fc     # Will be enclosing <figure> for this caption.
      figure = fig_arr[-1][0] # Assign nickname for readability.
      # If fig class="big", make caption class="big". (Small is unmarked.)
      fc.add_class('big') if figure['class'].include? 'big'
      # If fig class="small", make *figure* class="has-caption".
      figure.add_class('has-caption') if figure['class'].include? 'small'
      figure.add_child(fc)
      # Delete [[cap]] from present node.
      node_text = node.content
      node_text.gsub!(/\[\[cap\]\](.+)\[\[\/cap\]\]/, '')
      node.content = node_text
      # Delete present node if it is now empty or has only whitespace.
      node.remove if node.content == '' or node.content.match(/^\s+$/)

      # If small or large, ask if caption is centered.
      puts "\nCaption: #{captext}"
      print "Is this caption (c)entered with respect to the image? "
      answer = gets.chomp
      fc.add_class('center') if answer == 'c'

    else
      puts "*** WARNING: I saw a caption without a preceding image. Ignored."
      puts "Content:\n#{node.content}"
    end
  end
end


# Array of images: [['oldname', 'newname'], ['oldname2', 'newname2']]
old_to_new = []


# (3) Rename images and alt tags in HTML, based on captions and title.
# Also, make images clickable.
fig_nodes = $fixed.css('figure')
fig_nodes.each_with_index do |fnode, i|
  # Start of filename (based on title) for captionless images.
  capless_filename = str_to_img_name($metadata['Title'])
  inode = fnode.at_css('img')
  # Extract extension. Assumes 'src' ends in extension.
  ext = inode['src'].scan(/\.(\w+)$/)[0][0]
  oldname = inode['src']
  # If caption exists, use it to fill 'alt' and to rename the image.
  if fnode.at_css('figcaption')
    cap = fnode.at_css('figcaption').content
    inode['alt'] = cap[0..100]        # Fill 'alt' text from caption.
    filename = str_to_img_name(cap)   # Get a good filename from caption.
    src = "data/media/images/#{filename}-#{i+1}.#{ext}"
    inode['src'] = src
  else                                # No caption; name after title.
    inode.delete('alt')               # No alt text :-( .
    inode['src'] =
      "data/media/images/#{capless_filename}-#{i+1}.#{ext}"
  end
  old_to_new << [oldname, inode['src']]

  # Make image clickable.
  # First, create new anchor tag.
  imga = $fixed.create_element('a')
  imga['href'] = inode['src']         # Point at image itself.
  # Then copy inode into the tag.
  imga.inner_html = inode.dup
  # Then replace <img> (i.e., inode) with <a> (i.e., imga).
  inode.replace(imga)
end
puts "Styled images and made them clickable."
puts "Renamed #{fig_nodes.count} images (based on captions where available)."

# Move files.
old_dir = ''                      # For moving old image directory.
old_to_new.each do |pair|
  old, noo = pair[0], pair[1]     # Old name, new name; these include paths.
  # Strategy: copy from old (incl path; no need to edit) to the zwi name,
  # incl. zwi/data/etc.
  if File.exist?("#{$wkdir}#{old}") and ! File.exist?("#{$wkdir}zwi/#{noo}")
    FileUtils.cp("#{$wkdir}#{old}", "#{$wkdir}zwi/#{noo}")
  elsif File.exist?("#{$wkdir}zwi/#{noo}")
    puts "Already moved #{old} to zwi directory; excellent."
  else
    puts "*** Odd. Not sure where #{old} went. Not moved to zwi directory."
  end
  # Get name of old image folder from old.
  old_dir = old.scan(/(.+)\/.+\.\w{3,4}$/)
  old_dir = old_dir[0][0]
end

# Move old images.
# First, make the archive/drafts directory if necessary.
FileUtils.mkdir("#{$wkdir}archive/drafts/") unless
                                      Dir.exist?("#{$wkdir}archive/drafts")
if Dir.exist?("#{$wkdir}#{old_dir}") and
    (! Dir.exist?("#{$wkdir}archive/drafts/#{old_dir}") or
     Dir.glob("#{$wkdir}archive/drafts/#{old_dir}/*").empty? )
  # I don't *think* I have to force the following...
  FileUtils.mv("#{$wkdir}#{old_dir}", "#{$wkdir}archive/drafts/#{old_dir}")
  puts "Moved original image directory to archive/drafts/#{old_dir} ."
else
  puts "Already moved original image directory to archive/drafts/#{old_dir} ."
end
# Done. Can't be re-run, because it will rename, markup, etc., stuff twice.
$editdata[$v]['preparedImages'] = true


end # of massage_images


# Converts <img> width and height to max-width:100% and max-height.
def maxify_img_dimensions
  imgs = $fixed.css('img')        # Get array of <img>s.
  return unless imgs              # Skip method if no <img>s.
  imgs.each do |img|              # Iterate all <img>s.
    # Skip this img if max-width and max-height found.
    if ! img['style'].nil? and
         img['style'].include? ("max-width") and
         img['style'].include? ("max-height")
      next                        # Work has been done on this <img>.
    end
    # Get height.
    ht = img['style'].scan(/height\:(\d+)/)[0][0].to_i
    # Replace "width" > "max-width:100%", and "height" > "max-height".
    img['style'] = "max-width:100%;max-height:#{ht}pt;"
    puts "Edited image dimensions (width -> max-width, etc.)."
  end
end
