module Helpers

  # Little helper for showing files wrapped.
  def rap(str)
    Strings.wrap(str, 76)
  end

  # Levenshtein distance calculator
  def ld(s, t)
    v0 = (0..t.length).to_a
    v1 = []
    #p v0
    s.chars.each_with_index do |s_ch, i|
      v1[0] = i + 1
      t.chars.each_with_index do |t_ch, j|
        cost = s_ch == t_ch ? 0 : 1
        v1[j + 1] = [v1[j] + 1, v0[j + 1] + 1, v0[j] + cost].min
      end
      v0 = v1.dup
      #p v1
    end
    v0[t.length]
  end

  # UX assistance
  def go_on
    print "\n<Enter> to go on... "
    gets
  end

  # Good grief, why isn't this in Ruby?
  def titleize(str)
    str[0].upcase + str[1..-1].downcase
  end

  def get_available_paths
    paths = []
    if File.exist?('./paths')
      paths = File.readlines('./paths').map(&:chomp)
      paths.select!{|path| Dir.exist?(path)}
    end
    paths.uniq!
    return paths
  end

  def add_new_path_to_paths(paths, path)
    # Add a slash at the end if there isn't one.
    path += '/' if path[-1] != '/'
    $wkdir = path.dup
    # Append to top of path array.
    paths = paths.unshift($wkdir)
    paths.uniq!           # Delete dup.
    # Rewrite paths file.
    File.open('./paths', 'w+') {|f| paths.each {|line| f.puts line}}
  end

  def remove_path_from_list(paths, path)
    paths -= [path]
    paths.uniq!           # Delete dup.
    # Rewrite paths file.
    File.open('./paths', 'w+') {|f| paths.each {|line| f.puts line}}
  end

  # Prepends $wkdir in the 'paths' file.
  def append_to_top_of_path_array(paths)
    paths = paths.unshift($wkdir)
    paths.uniq!               # Delete dup.
    # Rewrite paths file.
    File.open('./paths', 'w+') {|f| paths.each {|line| f.puts line}}
  end

  # As the method name says, this removes an <a name="desc"></a> that is by
  # itself in a paragraph to the next par.
  def move_lone_desc_to_next_line
    lonely_desc = $doc.at_css("p a[@name='desc']")
    return unless lonely_desc
    if lonely_desc.parent.content.nil? or
       ! lonely_desc.parent.content.match?(/\w/)
      # A kluge to get the next par.
      if (! lonely_desc.parent.next.nil? and
            lonely_desc.parent.next.name == 'p')
        next_par = lonely_desc.parent.next
      elsif (! lonely_desc.parent.next.next.nil? and
               lonely_desc.parent.next.next.name == 'p')
        next_par = lonely_desc.parent.next.next
      end
      return unless next_par
      return unless next_par.content
      next_par.inner_html = '<a name="desc"></a>' + next_par.inner_html
      lonely_desc.parent.remove
    else
      return
    end
  end

  # Commit changes by creating new versions of $html and $text as well.
  # This will transfer changes made to $doc to the later-used objects.
  def from_doc_to_html_and_text_globals
    $html = $doc.inner_html.split(/[\r\n|\n|\r]/)
    $text = $doc.text.split("\n")
  end

  # Simply put contents of info.json into $info.
  def load_info
    if File.exist?("#{$wkdir}../../info.json")
      $info = JSON.parse(File.read("#{$wkdir}../../info.json"))
      puts "Found 'info.json' for this encyclopedia. Loaded."
    elsif File.exist?("#{$wkdir}../../../info.json")
      $info = JSON.parse(File.read("#{$wkdir}../../../info.json"))
      puts "Found 'info.json' for this encyclopedia. Loaded."
    else # Warn the user.
      puts rap %{*** NOTE: you need to place info.json in the grandparent (or *maybe* the great-grandparent) directory. You will not be able to prepare a complete ZWI file until this is done.}
    end
  end

  # Since Nokogiri converts &nbsp; and doesn't return this character, this
  # replaces them, from &nbsp; => ^nbsp; .
  def encode_nbsp_in_text_array(text_array)
    text_array.map! do |line|
      encode_nbsp_in_string(line)
    end
    return text_array
  end

  # Used above.
  def encode_nbsp_in_string(str)
    str.gsub(/\&nbsp\;/, '^nbsp;')
  end

  # Put in title case, and strip any concluding punctuation (usually).
  def make_phrase_Title_Case(title_str)
    little_words = %w{the a an and but of or to in by with from for as if that which}
    # If all caps, make it all lowercase. (Could be a bit better.) Assume
    # title is properly titleized unless all uppercase.
    title_str_words = title_str.split(' ')
    unless title_str.gsub(/[^a-zA-Z0-9]/, '').match(/[a-z]/)
      title_str_words.map! do |word|
        word.downcase!
        unless little_words.include? word
          word = titleize(word)
        end
        word
      end
    end
    # Rejoin words after titleizing properly.
    title_str_edited = title_str_words.join(' ')
    # Remove any final period, unless there is already one period in the
    # title.
    unless title_str_edited[0..-2].match?(/\./)
      title_str_edited.gsub!(/[\.\,\;\′]\s*?$/, '')
    end
    title_str_edited.strip!
    # Ensure the first letter of the phrase is capitalized.
    title_str_edited = title_str_edited[0].upcase + title_str_edited[1..-1]
    return title_str_edited
  end

  # This simply takes a string and removes anything of the form [[.+?]].
  def remove_pseudotags(str)
    fixed = str.gsub!(/\[\[.+?\]\]/, '')
    if fixed
      return fixed
    else
      return str
    end
  end

  # Prepare string to show user available paths.
  def show_available_zwifolders_and_originals(paths)
    path_string = ''
    # These paths are assumed groovy since they were used before.
    pwd = Dir.pwd
    pwd_arr = pwd.split('/')
    paths.each_with_index do |path, i|
      # Toss part of path that overlaps with pwd. (Else path too long.)
      toss = '/'
      pwd_arr.each do |part|
        next if part == ''
        testtoss = "#{toss}#{part}/"
        if path.include? testtoss
          toss = testtoss
        else
          break
        end
      end
      path_string += "(#{i+1}) #{path.gsub(/#{toss}/, '[...] ')}\n"
      break if i > 8
    end
    puts "\n" + path_string
  end

  # Given an article title or caption, output an appropriate image filename.
  def str_to_img_name(str)
    little_words = %w{the a an and but of or to in by with from for as if then that which all some none no is was will be were has have}
    regex = Regexp.union(little_words)
    str = Tate::transliterate(str)
    str.downcase!
    str.gsub!(/\b#{regex}\b/, '')
    str.gsub!(/[[:punct:]]/, '')
    str.gsub!('  ', ' ')
    str.strip!
    str.gsub!(' ', '-')
    str = str[0..25] if str.length > 25
    str[25] = '' if str[25] == '-'
    return str
  end

  # Used in &convert_pseudotag below, this looks up next available ID in the
  # present path directory, increments it there, and returns it here. Note:
  # the ID in $info['NextID'] is *not* one that has been used yet. It is *one
  # more* than the last one that has been used. Also, it is an integer, not a
  # string.
  def get_next_id
    # Read info.json in order to find out the next available ID ('NextID').
    begin
      if File.exist?("#{$wkdir}../../info.json")
        $info = JSON.parse(File.read("#{$wkdir}../../info.json"))
      else
        $info = JSON.parse(File.read("#{$wkdir}../../../info.json"))
      end
    rescue
      puts rap %{It appears you have not created any IDs for this encyclopedia. If that is correct, and you want to fix the situation by hand, quit. I can create a new JSON file in the grandparent directory to the present one, and set the first ID to '1'.}
      print "\nDo this now? <Enter> for 'yes': "
      answer = gets.chomp
      # Go ahead and create the JSON file.
      if answer == ''
        $info = {} unless !$info.nil?
        $info['NextID'] = 1 unless !$info['NextID'].nil?
        File.write("#{$wkdir}/../../info.json", JSON.pretty_generate($info))
        puts "Saved new 'info.json' in the grandparent directory."
      else
        puts rap %{\nVery well. We cannot proceed without an ID on each <article>. Exiting.}
        exit
      end
    end                   # of rescue.
    # Actually assign and increment ID.
    id = $info['NextID']
    # Increment and save 'NextID'
    $info['NextID'] += 1
    if File.exist?("#{$wkdir}/../../info.json")
      File.write("#{$wkdir}/../../info.json",
                 JSON.pretty_generate($info))
    elsif File.exist?("#{$wkdir}/../../../info.json")
      File.write("#{$wkdir}/../../../info.json",
                 JSON.pretty_generate($info))
    end
    # Return the id, PADDED WITH ZEROS.
    return id.to_s.rjust(6, "0")
  end

  # This complex bit of code converts [[article]] ... [[/article]] into a
  # proper <article>; and similarly with other tags, later, perhaps. Should
  # avoid problems of previous solution. Passed var 'type' expected to be
  # either 'article' something else, at present; the method should be
  # easily extensible.
  # If no match for the pseudotag is found, return false.
  def convert_pseudotag(type)
    # Strategy:
    # (1) Iterate nodes, looking for first instance of [[type]].
    # (2) Choose 'start', a direct child of <"top">: the node or an ancestor.
    # (3) Copy from 'start' until you reach [[/type]] = 'type_nodes'.
    # (4) Create 'type' node, set dups of 'type_nodes' as children.
    # (5) Insert 'type' just before 'start'.
    # (6) Delete each of 'type_nodes'. That should do it.

    top = case type
          when 'article'
            'body'
          when 'section'
            'article'
          end

    pseudotag = type == 'article' ? 'article' : 'subart'

    # (1) Iterate nodes, looking for first instance of [[type]].
    type_spotted_node = ''
    $doc.css('h1, h2, h3, h4, h5, h6, p').each do |node|
      if node.content.match(/(\[\[#{pseudotag}\]\])/i)
        m = node.content.match(/(\[\[#{pseudotag}\]\])/i)
        next unless node.content and $1
        # Go ahead and delete [[type]] now.
        node.inner_html = node.inner_html.gsub($1, '')
        type_spotted_node = node
        break     # No need to see further nodes.
      end
    end

    # If no match spotted, return false.
    unless type_spotted_node != ''
      return false
    end

    # (2) Choose 'start', an immediate child of top.
    considering_node = type_spotted_node
    # Iterate recursively "upward" until direct descendant of <body> found.
    until considering_node.parent.name == top
      considering_node = considering_node.parent
    end
    start = considering_node

    # (3) Create 'type'.
    type_element = $doc.create_element("#{type}")
    # Append ID to <article>s.
    if type == 'article'
      type_element['id'] = get_next_id
    end

    # (4) Copy from 'start' until you reach [[/type]]; add dup of node to the
    # newly-created type_element until end reached.
    delete_these = []
    copying = false
    stopping = false      # Indicates whether closing pseudotag is spotted.
    $doc.traverse do |node|
      # Skip document node; and don't look at the node unless it's a direct
      # child of <body>.
      next if node.name == 'document'
      next unless node.parent.name == top
      copying = true if node == start
      if copying
        # Check if this node contains [[/type]] (= stop)
        if ! node.text.match(/(\[\[\/#{pseudotag}\]\])/i).nil?
          stopping = true
          # Go ahead and delete [[/type]] now.
          node.content = node.content.gsub($1, '')
        end
        # Copy dup to the children to include in the type_element.
        type_element.add_child(node.dup)
        delete_these << node
        break if stopping
      end
    end

    # Do nothing unless the closing pseudotag was spotted.
    if stopping
      # (5) Insert 'type_element' just before 'start'. Work is already done.
      start.before(type_element)

      # (6) Delete each of 'type_nodes'. That should do it.
      delete_these.each(&:remove)

      print pseudotag[0]
      return true
    else
      puts rap %{\n\n*** NOTICE: did not find [[#{pseudotag}]] and [[/#{pseudotag}]] in a proper pair. Please fix how these pseudotags are used, and then try running the script on the file again.}
      exit
    end
  end




end
