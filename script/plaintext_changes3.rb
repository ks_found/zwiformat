def make_plaintext_changes


$fixes_to_do = []
$uc_fixes_to_do = []
# Check for "l" (lowercase "L") adjacent to any digit, and for "1" (numeral)
# adjacent to any letter of the alphabet.
if $editdata[$v]['numeralIssuesDone']
  puts "Already fixed numeral issues."
else
  puts "Testing for numeral issues..."
  $text.each do |line|
    if line.match( /\b((\d{1,3}|(\d\d\d)+)[\. ]\d\d\d([\,\. ]\d\d\d)+?)\b/)
      puts "Number error? #{$1}"
      print "Change to? <Enter> to leave unchanged: "
      change = gets.chomp
      unless change.empty?
        $fixes_to_do << [$1, change]
      end
      # Delete up to and including this word, then try again.
      idex = line =~ /\W(.*?#{$1}.*?)\W/
      next unless $1
      line.slice!(0..idex + $1.length)
      redo if line.match(/\b((\d{1,3}|(\d\d\d)+)[\. ]\d\d\d([\,\. ]\d\d\d)+?)\b/)
    else
      error = ''
    end
  end
  # Done with these issues.
  $editdata[$v]['numeralIssuesDone'] = true
end

# These fixes are changed throughout the entire document, so it is expected
# that they are 100% unique *or* can certainly be fixed throughout.
if $editdata[$v]['letterIssuesDone']
  puts "Already fixed letter issues."
else
  puts "Testing for letter issues..."
  $text.each do |line|
    # Letter 'l' next to numeral (numeral '1' not recognized).
    if line.match(/\b(\w*?(l\d|\dl)\w*?)\b/)
      puts "Found a letter 'l' next to a numeral: #{$1}"
      print "Change to? <Enter> to leave unchanged: "
      change = gets.chomp
      unless change.empty?
        $fixes_to_do << [$1, change]
      end
      # Delete up to and including this word, then try again.
      idex = line =~ /\W(\w*?#{$1}\w*?)\W/
      next unless $1
      line.slice!(0..idex + $1.length)
      redo if line.match(/\b(\w*?(l\d|\dl)\w*?)\b/)
    end
  end

  # Replace ordinary hyphen with en dash when separating numerals (with
  # user input).
  endash_prob_found = false
  unless $info['EnDash'] == 'never'
    $text.each do |line|
      if line.match(/(\d+\-\d+|[a|p]\.m\.\-\d+)/)
        hyp = $1
        puts "One or more numerical-type hyphens found." unless
          endash_prob_found
        endash_prob_found = true
        # Show context to the user.
        puts "\"#{hyp}\" in: #{line.gsub(hyp, ">>>#{hyp}<<<")}"
        # Handle optional case.
        def ask_user_in_optional_case(hyp)
          print "Change hyphen to en dash? <Enter> for 'yes': "
          answer = gets.chomp
          if answer == ''
            $fixes_to_do << [hyp, hyp.gsub('-', '–')]
          else
            puts "Very well, skipping."
          end
        end
        # If en dash policy is "always," change it.
        if $info['EnDash'] == 'always'
          $fixes_to_do << [hyp, hyp.gsub('-', '–')]
        elsif $info['EnDash'] == 'optional'
          ask_user_in_optional_case(hyp)
        # Give the user the option of setting policy.
        elsif $info['EnDash'] == 'never'
          # Do nothing!
        else
          puts rap %{You have the option specifying that #{$metadata['BookTitle']} (i.e., the volume this article is from) should always place en dashes in place of hyphens when they occur between numbers.}
          answer = ''
          until answer == 'a' or answer == 'o' or answer == 'n'
            print "\nSet this as policy [a]lways, [o]ptional, or [n]ever? "
            answer = gets.chomp
            if ! (answer == 'a' or answer == 'o' or answer == 'n')
              puts "Only 'a' or 'o' please."
            end
            if answer == 'a'
              $info['EnDash'] = 'always'
              puts "Setting set."
              $fixes_to_do << [hyp, hyp.gsub('-', '–')]
            elsif answer == 'o'
              $info['EnDash'] = 'optional'
              puts "Setting set. Now, in the above (\"#{hyp}\") case:"
              ask_user_in_optional_case(hyp)
            else
              $info['EnDash'] = 'never'
              puts "Setting set."
              # Note, doing nothing!
            end
            # Save new $info setting to info.json.
            if File.exist?("#{$wkdir}/../../info.json")
              File.write("#{$wkdir}/../../info.json",
                         JSON.pretty_generate($info))
            elsif File.exist?("#{$wkdir}/../../../info.json")
              File.write("#{$wkdir}/../../../info.json",
                         JSON.pretty_generate($info))
            end
          end
        end
      end
    end
  end
  puts "\n" if endash_prob_found


  # DE- AND RE-HYPHENATE words wrongly handled by ABBYY.
  print "Fixing hyphenation..."
  finewords = []    # User-approved words.
  # Hyphenated strings such that, when dehyphenated, they make a word
  # already in the document, or (failing that) in the dictionary. Also,
  # words split across lines that are *incorrectly* dehyphenated by ABBYY;
  # rehyphenate these.
  # Load US dictionary. (Stolen from spellchecker below.)
  dict = File.readlines('script/dictionaries/uslong.txt')
  print '.'
  places = File.readlines('script/dictionaries/usplacenames.txt')
  print '.'
  dictplaces = dict + places
  dict.map(&:chomp!)
  dict.map(&:downcase!)
  places.map(&:chomp!)
  places.map(&:downcase!)
  dictplaces = dict + places
  tesstext_with_cols =
                  File.readlines("#{$wkdir}archive/tesstext_with_cols.txt")
  # Prep list of hyphenated words at end of columns (split across lines);
  # of hyphenated words of which both parts of the word appear on the same
  # line; and of words *without* hyphens, but which match words split
  # across lines. ONLY TESSTEXT DATA FOR THIS.
  end_col_hwords = []    # These are hyphenated words, split at column end.
  midline_hwords = []    # Hyphenated words *not* split at column end.
  unhyphend_in_text = [] # Unhyphen'd words found in text matching words
                         # split across column.
  print '.'

  # Prepare end_col_hwords and midline_hwords. Again, these are,
  # first, tesstext words split across lines (with hyphens at that place),
  # and second, words *not* split across lines, but with hyphens.
  tesstext_with_cols.each_with_index do |line, i|
    if line.match(/([a-zA-Z0-9]+\-[a-zA-Z0-9]+).+/)
      midline_hwords << $1.downcase
    end
    if line.match(/([a-zA-Z0-9]+)\-\s+?$/)
      start_of_word = $1
      next unless tesstext_with_cols[i+1]
      next unless tesstext_with_cols[i+1].match(/^([a-zA-Z0-9]+)/)
      rest_of_word = $1
      end_col_hwords << "#{start_of_word}-#{rest_of_word}".downcase
    end
  end
  midline_hwords.uniq!    # Compact and order the lists.
  midline_hwords.sort!
  end_col_hwords.uniq!
  end_col_hwords.sort!
  print '.'

  # Compile list of all unique "words" found in the tesstext.
  all_tesstext_words = []    # Probably useful elsewhere.
  tesstext_with_cols.each do |line|
    line.split(/[\n\s_  ]+/).each do |word|
      if word.match(/^[[:punct:]]+(.*?)$/)
        word = word.gsub(/^[[:punct:]]+(.*?)$/, $1)
      end
      if word.match(/^(.*?)[[:punct:]]+$/)
        word = word.gsub(/^(.*?)[[:punct:]]+$/, $1)
      end
      next if all_tesstext_words.include? word.downcase
      all_tesstext_words << word.downcase
    end
  end
  all_tesstext_words.uniq!
  all_tesstext_words.sort!   # Compact and order the list.
  print '.'

  # Prepare unhyphend_in_text. This is an array of words from the tesstext
  # such that an instance is split across lines, but another instance is
  # found somewhere *not* split across lines, and without a hyphen.
  # For each whole, unhyphenated word not hyphenated across tesseract
  # file lines, see if it is the same as any word in end_col_hwords.
  end_col_hwords.each do |end_col_hword|
    testword = end_col_hword.gsub("-", "")
    if all_tesstext_words.include? testword
      unhyphend_in_text << testword
    end
  end
  unhyphend_in_text.uniq!
  unhyphend_in_text.sort!
  print '.'

  # Now we actually fix things wrongly "fixed" by ABBYY: words intended by
  # the editors as having hyphens (when split across lines) are "fixed" by
  # ABBYY by removing the hyphens. The editor intention is confirmed by
  # other instances of the same word appearing *with* hyphens when not split
  # across lines ("in the middle of the line").
  htmlwords = $text.join.split(/[\n\s+  ]+/)
  htmlwords.map! do |word|
    # Remove punctuation from start and end of "word" (e.g., commas).
    if word.match(/^[[:punct:]]+(.*?)$/)
      word = word.gsub(/^[[:punct:]]+(.*?)$/, $1)
    end
    if word.match(/^(.*?)[[:punct:]]+$/)
      word = word.gsub(/^(.*?)[[:punct:]]+$/, $1)
    end
    word
  end
  warned = false          # For warning about hyphenation issue.
  print '.'

  spellerrors = []
  if File.exist?("#{$wkdir}spellerrors.txt")
    spellerrors = File.readlines("#{$wkdir}spellerrors.txt") # Needed in elsif below.
  elsif ! warned
    puts rap %{\nWe will not automatically fix a certain kind of hyphenation issue (word incorrectly dehyphenated by ABBYY) until after the spell-check routine is done (below).\n\nPlease redo checks sometime later to automatically fix them, and after you do spelling fixes.}
    warned = true
  end
  spellerrors.map(&:chomp!)

  end_col_hwords.each do |end_col_hword|
    # TEST: e.g., 'salt-water' and 'mer-cury'; both appear at end of column.
    # Strategy: iterate through all the tesstext words split across lines.
    # For each, construct a version *without* hyphens (this should catch all
    # those wrongly fixed by ABBYY). Check if that version found among
    # the *HTML article words*. If yes, then change all instances of the
    # unhyphenated version to the hyphenated version.
    dechw = end_col_hword.gsub('-','') # To test: dehyphenated end col...
    front, back = end_col_hword.split('-')          # Needed in elsif below.
    # What if neither the hyphenated version nor the dehyphenated version is
    # found either in the rest of the document, or in the dictionary(!), BUT
    # where the two parts of the word *are* in the dictionary? Strategy: for
    # all and *only* such words, ask the user whether he wants to delete or
    # retain the hyphenation. Should probably show in context.
    # NOTE: this assumes the word *wasn't* caught above.
    if (((dictplaces.include? front) ||
         (dictplaces.include? front.downcase)) &&
        ((dictplaces.include? back) &&
         (dictplaces.include? back.downcase)) &&
        (spellerrors.include? dechw) )
      puts "\nReplace '#{dechw}' with '#{front}-#{back}' throughout?"
      print "<Enter> for 'Yes': "
      answer = gets.chomp
      if answer == ''
        $fixes_to_do << [dechw, "#{front}-#{back}"]
        next
      end
    end
    # TEST: e.g., 'saltwater' and 'mercury'.
    # (a) 'saltwater' is found in HTML (problem!), while
    # (b) 'salt-water' is found in tesstext (good!) as well, thus
    # (c) fix: 'saltwater' -> 'salt-water'
    # Also: (a) 'mercury' is in HTML (no problem!), while
    # (b) 'mer-cury' should *not* be found in tesstext, thus
    # (c) don't "fix" 'mercury' to 'mer-cury'.
    if ( (htmlwords.include? dechw) &&
         (all_tesstext_words.include? end_col_hword) )
      puts "\nWant to change '#{dechw}' to '#{end_col_hword}' throughout?"
      print "<Enter> for 'yes' or anything else for 'no': "
      answer = gets.chomp
      if answer == ''
        $fixes_to_do << [dechw, end_col_hword]
        # Also construct a similar "uppercase fixes to do" in case the word
        # recurs in uppercase.
        uc_dechw  = "#{dechw[0].upcase}#{dechw[1..-1]}"
        uc_echw = "#{end_col_hword[0].upcase}#{end_col_hword[1..-1]}"
        $uc_fixes_to_do << [uc_dechw, uc_echw]
        print '.'
      end
      next
    end
  end

  # Dehyphenate words that ABBYY should have dehyphenated.
  # Iterate the words in the present text of the HTML. Check only words that
  # are hyphenated and broken across lines. Does related stuff too.
  $text.each do |line|
    line.split(/[\n\s+  ]+/).each do |word|
      # Remove punctuation from start and end of "word" (e.g., commas).
      if word.match(/^[[:punct:]]+(.*?)$/)
        word = word.gsub(/^[[:punct:]]+(.*?)$/, $1)
      end
      if word.match(/^(.*?)[[:punct:]]+$/)
        word = word.gsub(/^(.*?)[[:punct:]]+$/, $1)
      end
      # Skip unhyphenated words, of course.
      next unless word.match(/^(\w+)\-(\w+)$/)
      # If a word was already confirmed by the user as "fine" to hyphenate,
      # stet (i.e., next; move on to next word).
      next if finewords.include? word.downcase
      # Skip hyphenated words that *don't* come at end of column
      # (somewhere in the article).
      next unless end_col_hwords.include? word.downcase
      # Skip hyphenated words that *are* within the line *as hyphenated*.
      next if midline_hwords.include? word.downcase
      teststring = "#{$1}#{$2}" # Removes hyphen, for testing.
      # Remove hyphens from hyphenated words split across lines *if* an
      # unhyphenated instance is found among the article's words. You can
      # speed up the search by testing not against *all* the article's
      # words, but just those that occur in at the end of a line and also
      # in unhyphenated form (already prepared: 'unhyphend_in_text').
      if unhyphend_in_text.include? teststring.downcase
        $fixes_to_do << [word, teststring]
        # Also construct a similar "uppercase fixes to do" in case the word
        # recurs in uppercase.
        uc_word  = "#{word[0].upcase}#{word[1..-1]}"
        uc_teststring = "#{teststring[0].upcase}#{teststring[1..-1]}"
        $uc_fixes_to_do << [uc_word, uc_teststring]
        next      # No need to ask user about a word that is unhyphenated
                  # elsewhere in the text!
      end
      # At this point, the only thing left to check would be hyphenated split
      # words that don't have any matching strings (hyphenated or not) in
      # other positions in the text. Many articles won't get to here.

      # This simply tests whether a dehyphenated word is in the dict.
      # Logic is: if the *un*hyphenated version of the word is in the
      # dictionary, while the *hyphenated* version is *not*, ask.
      if (((dictplaces.include? teststring) ||
          (dictplaces.include? teststring.downcase)) &&
         ((! dictplaces.include? word) &&
          (! dictplaces.include? word.downcase)))
        puts "\nReplace '#{word}' with '#{teststring}' throughout?"
        print "<Enter> for 'Yes': "
        answer = gets.chomp
        if answer == ''
          $fixes_to_do << [word, teststring]
        else
          finewords << word.downcase
          # Add plural and nonplural versions so as not to annoy user.
          if word[-1] == 's'
            finewords << word[0..-2].downcase
          else
            finewords << "#{word}s".downcase
          end
        end
      end

    end
  end

  # Fix hyphenated word + space + lowercase.
  # Relatively simple case, handled separately.
  undehyps = $doc.text.scan(/([a-z]+)\- ([a-z]+)/)
  undehyps.each do |front, back|
    if dictplaces.include?(front + back)
      puts "Found \"#{front}- #{back}\". Change to #{front}#{back}?"
      print "<Enter> to approve, anything else to skip: "
      answer = gets.chomp
      if answer == ''
        $fixes_to_do << ["#{front}- #{back}", "#{front}#{back}"]
      end
    end
  end

  # Report to user what will be done.
  unless $fixes_to_do.empty?
    will_do = []    # Array of text strings ("old --> new").
    ($fixes_to_do + $uc_fixes_to_do).each do |fix|
      will_do << "#{fix[0]} ➔ #{fix[1]}"
    end
    puts Strings.wrap("\nWill change throughout: #{will_do.join(" | ")}",74)
    puts "\nDone with \"plaintext\" changes."
    go_on
  end

  $editdata[$v]['letterIssuesDone'] = true
end
# Note, the above uses '$text' to prepare $fixes_to_do.
# It does not write to any file or to '$text'.

puts


end
