# SPELL CHECKING

def initialize_spell_checker
  # Preparatory:
  # $spellfixes used throughout section, initialized here.
  # Hash structure: $spellfix['badstring'][position #] = fix
  # Also, load dictionaries and check against them.
  if $editdata[$v]['spellcheckDone'] == false
    puts "\nRunning spellcheck.\n"
    # Load dictionaries (U.S. only for now).
    dict = File.readlines('script/dictionaries/uslong.txt')
    places = File.readlines('script/dictionaries/usplacenames.txt')
    userdict = (File.exist?('script/dictionaries/user.txt') ?
      File.readlines('script/dictionaries/user.txt') : [])
    # Compact dictionary array (=dictplaces).
    dict.map(&:chomp!)
    dict.map(&:downcase!)
    places.map(&:chomp!)
    places.map(&:downcase!)
    userdict.map(&:chomp!)
    userdict.map(&:downcase!)
    ignored = $editdata[$v]['ignoredWords'] ?
              $editdata[$v]['ignoredWords'] : []
    dictplaces = dict + places + userdict + ignored
    dictplaces.sort!
    dictplaces.uniq!
    azdicts = {}
    # Split up dictionary to speed up search.
    dictplaces.each do |word|
      next if word[0].nil?
      start = word[0]
      start.downcase!
      azdicts[start] = [] unless azdicts[start]
      azdicts[start] << word
    end
    puts "Loaded dictionaries."
    $fixed.content.split(/[\n\s+  [[:punct:]]]+/).each do |word|
      next if word.match(/[\d+°]/)
      next if word[0].nil?
      next if word[0].downcase.nil?
      next if azdicts[word[0].downcase].nil?
      start = word[0].downcase
      thisdict = azdicts[start]
      unless (thisdict.include? word.downcase) or (thisdict.include? word)
        # Both initializes data array *and* records the word.
        print '.'
        $spellfixes[word] = {}
      end
    end
    # List of spelling errors written to file.
    File.open("#{$wkdir}spellerrors.txt", "w+") {|f| f.puts($spellfixes.keys)}
    ess = $spellfixes.keys.length != 1 ? 's' : ''
    puts "\nFound #{$spellfixes.keys.length} potential misspelling#{ess}."
  end
end

# Helper: prints error instances in context, skipping those already done or
# deleted.
def print_matching_errors_in_context(err)
  positions = $spellfixes[err].keys.sort!
  # Iterate positions of matches, showing context.
  positions.each_with_index do |pos, i|
    next unless ($spellfixes[err][pos].empty? ||
                 $spellfixes[err][pos] != "<<DELETE>>" )
    puts ("\n                               === #{i} ===".yellow) if
                                                 positions.count > 1
    lbefore = (pos < 180 ? pos - 1 : 180)
    lafter = ( ($fixed.content.length - pos) < 180 ?
               ($fixed.content.length - (pos + err.length) - 1) : 180 )
    # Construct string with context
    rapstr = rap %{...#{$fixed.content[pos-lbefore..pos-1]}>>#{err}<<#{$fixed.content[pos+err.length..pos+err.length+lafter]}...\n}
    # Little trick to allow colorize to work with wrapping tool.
    rapstr1, rapstr2 = rapstr.split(">>#{err}<<")
    puts "#{rapstr1}>>#{err.green}<<#{rapstr2}"
  end
end # No return value; just prints.

# Given string, return page number of first occurrence.
def get_page_number_of_error(err)
  # Iterate $fixed nodes.
  pagenum = 0
  $fixed.at_css('body').traverse do |node|
    if node.name == 'a' and node['name'] and node['name'].match(/pg(\d+)/)
      pagenum = $1.to_i
    elsif
      # Exits traversal when err is *first* spotted.
      if node.content.match(/\b#{err}\b/)
        break
      end
    end
  end
  return pagenum
end

# Simply opens the scan in using system's default image viewer.
def open_scan(err)
  # Find the page number for the first instance of this error.
  pgnum = get_page_number_of_error(err)
  puts "Opening scan p. #{pgnum}."
  system("open '#{$wkdir}zwi/data/media/scans/#{pgnum}'.*")
end

# Helper: ignores all instances of a given word. No return.
def ignore_word(err)
  errdown = (err.downcase == err ? true : false)
  $spellfixes[err].keys.each {|pos| $spellfixes[err][pos] = '<<IGNORE>>'}
  if errdown
    if $spellfixes[titleize(err)]
       $spellfixes[titleize(err)].keys.each {|pos|
                          $spellfixes[titleize(err)][pos] = '<<IGNORE>>'}
    end
  else
    if $spellfixes[err.downcase]
       $spellfixes[err.downcase].keys.each {|pos|
                          $spellfixes[err.downcase][pos] = '<<IGNORE>>'}
    end
  end
  puts "Ignoring all instances of '#{err}' in this document."
end

# Helper: ignores a given word, and adds it to user dictionary. No return.
def ignore_and_write_to_user_dictionary(err)
  ignore_word(err)
  errdown = (err.downcase == err ? true : false)
  # Append word to dictionary. Don't care about capitalization.
  File.open("script/dictionaries/user.txt", "a") do |f|
    f.puts err
  end
  puts "Added '#{err}' to user dictionary."
end

# Helper: inserts <<DELETE>>, much like <<IGNORE>>.
def delete_word_from_document(err)
  until ! $spellfixes[err].keys.any? {|k|
          $spellfixes[err][k] != '<<DELETE>>'}
    print_matching_errors_in_context(err)
    # If only one match, prompt "OK?".
    if $spellfixes[err].keys.count == 1
      print "OK to delete? <Enter> for \"OK\" or 'q' to quit: "
      input = gets.chomp
      break if input == 'q'
      pos = $spellfixes[err].keys[0]
      $spellfixes[err][pos] = '<<DELETE>>'
    # If multiple matches, let user select and delete individually.
    else
      print "Enter # to delete, or 'q' to quit: "
      input = gets.chomp
      break unless input.match(/^\d+$/)
      ind = input.to_i
      unless ind < $spellfixes[err].keys.length
        puts "*** #{input} is not in range."
        next
      end
      pos = $spellfixes[err].keys[ind]
      $spellfixes[err][pos] = '<<DELETE>>'
      puts "*** Deleted."
    end
  end
end

# Main spell check method; for users. Shows all matches of the error
# string; iterates through them; and prompts user for fix. If user doesn't
# want to fix, word can be ignored as correct or added to dictionary.
def fix_word(err, systematic = false)
  puts "\nExamining: '#{err}'"

  # Check if this item is already finished. If so, clear work on it and
  # tell user.
  if ! $spellfixes[err].any? {|k,v| v==''} # If none (not any) is blank.
    puts "Looks like you already edited '#{err}', but want to redo it. OK."
    $spellfixes[err].keys {|key| $spellfixes[err][key] = ''}
    puts "Values are now #{$spellfixes[err]}"
  end

  finished_editing_item = false   # User-determined or all positions edited.
  # Construct 'posarr' (array of *unedited* positions left to edit).
  posarr = []     # Might be just one.
  $spellfixes[err].keys.each {|pos|
    posarr << pos if $spellfixes[err][pos] == '' }

  # Let user specify which num he would like to fix. If multiple available,
  # option for 'a'll appears. Otherwise, singleton instances are edited
  # alongside multiple instances.
  until finished_editing_item
    # Print matching words in their various positions in context.
    print_matching_errors_in_context(err)
    puts "\nInstances left: #{posarr.count}"
    # If multiple positions are left to edit, get user input on which one
    # to edit (or all). Then populate array of positions to edit (='posarr').
    if posarr.count > 1
      print "\nWhich number to edit (or 'a'll of these, or 'q'uit)? "
      editnum = gets.chomp
      break if editnum == 'q'
      next unless editnum.match(/^(\d+|a)$/)
      # Is the number chosen to edit on the list?
      next unless (0..$spellfixes[err].keys.length).include? editnum.to_i
    elsif posarr.count == 1
      # Determine the only instance left to edit (will be the only key of
      # $spellfixes[err] when there is only one instance to start).
      last_one = $spellfixes[err].select {|k,v| v == ''}
      editnum = 0
    else # exit 'until' loop.
      break
    end
    # Display user options.
    # Note choices. Changes ARE NOT written to file yet. They need to be
    # done in reverse order, so cannot be executed until the end of the
    # spellcheck subroutine.
    options =
      %w[open-scan ignore-all add-to-dictionary-(and-ignore-all) delete-from-the-text quit-editing-this-word]
    print "\n=========\n"
    # Prepare and print options for editing 'err'.
    options_string = posarr.length > 1 ?
      "Options: type something to replace '#{err}' in ##{editnum}, or " :
      "Options: type something to replace '#{err}', or "
    options.each do |opt|
      options_string += "(#{opt[0]})#{opt[1..-1].gsub('-',' ')} "
    end
    if editnum == 'a'
      options_string +=
        "\n\nNote: this will be done to ALL instance of '#{err}'."
    end
    puts rap options_string
    puts "========="
    print "\nHow to fix? "
    choice = gets.chomp
    num = choice.to_i unless choice.nil?
    # Dispatch table executes user choice.
    case choice
    # User types a word (to replace 'err').
    when /^[^\d]{2,}$/
      puts "OK. Replacing '#{err}' with '#{choice}'."
      if $spellfixes[err].count == 1 || editnum != 'a'
        # Assigns 'choice' to the only remaining position, in place of 'err'.
        $spellfixes[err][posarr[editnum.to_i]] = choice
        # Done editing only instance of 'err'.
        finished_editing_item = true if $spellfixes[err].count == 1
        posarr.delete_at(editnum.to_i)
      else
        # Iterate through multiple matches (count > 1 or editnum == 'a').
        posarr.each do |pos|
          $spellfixes[err][pos] = choice
        end
        posarr = []
      end
    when 'o'
      open_scan(err)
    when 'i'
      # Assigning 'false' to error = ignore all.
      ignore_word(err)
      finished_editing_item = true
    when 'a'
      ignore_and_write_to_user_dictionary(err)
      finished_editing_item = true
    when 'd'
      delete_word_from_document(err)
      finished_editing_item = true
    when 'q'
      finished_editing_item = true
    end
  end
  # Exit from editing this word if all instances are fixed.
  unless $spellfixes.keys.any? {|pos| ! $spellfixes[err][pos].nil?}
    finished_editing_item = true
  else
    # Reprint the errors, minus the fixed item. The method handles that.
    print_matching_errors_in_context(err)
  end
  puts "Done editing '#{err}'."
  go_on unless systematic
end # of main 'fix_word' method

# Extracts from user a $spellfixes key number within range, returns err word.
def get_error
  print "Which number? "
  num = gets.chomp
  if num.nil?
    puts "*** Answer not recognized."
    return nil
  elsif ! num.match(/^\d+$/)
    puts "*** Number only please."
    return nil
  elsif $spellfixes.keys[num.to_i].nil?
    puts "*** #{num} not in range."
    return nil
  else
    return $spellfixes.keys[num.to_i]
  end
end

def get_errors_with_spaces
  nums = []
  print "Which number(s)? (separated by spaces) "
  answer = gets.chomp
  if answer.nil?
    puts "*** Answer not recognized."
    return nil
  elsif ! answer.match(/^[\d ]+$/)
    puts "*** Numbers and spaces only, please."
    return nil
  end
  nums = answer.split(' ').map(&:to_i)
  nums.sort!.reverse!
end

# Shows error list string and invites user to choose from among fixes.
def review_errors
  return unless $spellfixes.length > 0
  # Show all enumerated errors.
  errstr = ''
  # First, reconstruct the error list string. Removes any completely done.
  # Any that were ignored, deleted, etc., won't be in the string anymore.
  $spellfixes.keys.each_with_index do |err, i|
    next if ! $spellfixes[err].any? {|k,v| v==''}
    errstr += "#{i} #{err} "
  end
  print rap %{\n#{errstr}}
  # Display user options.
  options = %w[systematically-review ignore delete-string-in-text
               add-to-dictionary mark-ALL-as-done quit-spellcheck]
  print "\n=========\n"
  options_string = 'Options: <Enter> for next item; or type number or string to fix, or '
  options.each do |opt|
    options_string += "(#{opt[0]})#{opt[1..-1].gsub('-',' ')} "
  end
  puts rap options_string
  puts "========="
  print "\nYour choice? "
  choice = gets.chomp
  num = choice.to_i unless choice == ''
  # Dispatch table executes user choice.
  case choice
  when ''
    err = ''
    $spellfixes.keys.each_with_index do |word, i|
      if $spellfixes[word].any? {|k,v| v==''}
        err = word
        break
      end
    end
    return false unless err != ''
    fix_word(err)
  when /^\d+$/
    if ! $spellfixes.keys[num].nil?
      fix_word($spellfixes.keys[num])
      return true
    else
      puts "*** That isn't a number in the list."
      return true
    end
  when /.{2,}/
    # Ensure word is in list.
    if ! $spellfixes.keys.include? choice
      puts "*** Not a word on the list."
      return true
    else
      fix_word(choice)
      return true
    end
  when 's'
    puts "Systematically reviewing all errors from start to finish."
    # Construct list of items to review (=to_review).
    to_review = []
    $spellfixes.keys.each do |err|
      next if ! $spellfixes[err].any? {|k,v| v==''}
      to_review << err
    end
    count = to_review.length
    to_review.each_with_index do |err, i|
      next if ! $spellfixes[err].any? {|k,v| v==''}
      # This line actually sends the error word to be reviewed.
      fix_word(err, true)   # true = systematic review; skips @go_on.
      puts "That was ##{i+1} of #{count}."
      print "Go on? <Enter> for yes; 'q' to quit: "
      answer = gets.chomp
      answer == '' ? next : (return true)
    end
    return false    # Reached only when user finishes all of to_review.
  when 'i'
    nums = get_errors_with_spaces
    if nums.nil?
      return true
    end
    nums.each do |num|
      ignoring = $spellfixes.keys[num].dup
      ignore_word(ignoring)
    end
    return true
  when 'a'
    nums = get_errors_with_spaces
    if nums.nil?
      return true
    end
    nums.each do |num|
      adding = $spellfixes.keys[num].dup
      ignore_and_write_to_user_dictionary(adding)
    end
    return true
  when 'd'  # DONE?
    puts rap %{*** This will delete one instance of the word from the document itself.}
    # Delete from working (article) variable (incl. prev. space, if any).
    err = get_error
    if err.nil?
      return true
    end
    delete_word_from_document(err)
    go_on
  when 'm'
    print "\nThis will delete the list of errors. <Enter> for 'OK': "
    ok = (gets.chomp == '' ? true : false)
    if ok
      $spellfixes = {}
      puts "*** We have cleared the list of errors."
    end
    return false
  when 'q'
    puts "Quitting spellchecker."
    return false
  else
    puts "*** Sorry, didn't recognize that."
    return true
  end
end # of @review_errors

# Essentially, at end of spell checker run, data is transferred from
# $spellfixes to the text of $fixed.
def commit_spelling_changes
  # This just rewrites $spellfixes.
  spfin = {}
  $spellfixes.each do |word, poses|
    poses.each do |pos, str|
      spfin[pos] = {old: word, new: str}
    end
  end
  # In reverse order, make changes.
  puts "*** Committing spelling changes, if any: "
  spfin.sort.reverse.each do |pos, words|
    neww = words[:new]
    oldw = words[:old]
    next if neww.nil?
    next if neww == ''
    # Add ignored words to $editdata.
    if neww == '<<IGNORE>>'
      $editdata[$v]['ignoredWords'] = [] unless $editdata[$v]['ignoredWords']
      unless $editdata[$v]['ignoredWords'].include? oldw
        $editdata[$v]['ignoredWords'] << oldw
      end
    elsif
      len = oldw.length
      # Find corresponding paragraph with unique search. First, try looking
      # ahead for 10 more characters. If found, groovy. If not, try looking
      # behind for 10 characters. If not found, report to user.
      # $fixed.at('p:contains(oldw)')
      nexteight = $fixed.content[pos+len..pos+len+8]
      lasteight = $fixed.content[pos-8..pos-1]
      element = ''
      fixtext = ''
      found = false
      ahead = false
      behind = false
      # This looks ahead for a match.
      if ! $fixed.at("p:contains(#{(oldw+nexteight).dump})").nil?
        element = $fixed.at("p:contains(#{(oldw+nexteight).dump})")
        print "'#{oldw}' > '#{neww}' ... "
        ahead = true
        found = true
      # If not found, this looks behind for a match.
      elsif ! $fixed.at("p:contains(#{(lasteight+oldw).dump})").nil?
        element = $fixed.at("p:contains(#{(lasteight+oldw).dump})")
        behind = true
        found = true
        print "'#{oldw}' > '#{neww}' ... "
      end
      if neww == '<<DELETE>>'
        neww = ''
      end
      if found
        fixtext = element.content
        if ahead
          fixtext.gsub!((oldw+nexteight), (neww+nexteight))
        elsif behind
          fixtext.gsub!((lasteight+oldw), (lasteight+neww))
        end
        element.content = fixtext     # This actually edits $fixed.
      else
        print "*** NOT FOUND - #{oldw} > #{neww} - PLEASE FIX BY HAND ***"
      end
    end
  end
end

def main_enclosing_spellchecker_method
  if $editdata[$v]['spellcheckDone']
    puts "Spell check already done."
  elsif $spellfixes.keys.length == 0
    $editdata[$v]['spellcheckDone'] = true
    puts "Spell check completed."
  else
    print "\nReview words flagged by spellchecker? <Enter> for yes: "
    dospellcheck = gets.chomp
    dospellcheck = (dospellcheck == '' ? true : false)

    return unless File.exist?("#{$wkdir}spellerrors.txt")

    # Construct $spellfixes! If user wants to do spellcheck.
    if dospellcheck
      # Construct $spellfixes from the contents of the file.
      errors = File.readlines("#{$wkdir}spellerrors.txt").map(&:chomp!)
      errors.each {|err| $spellfixes[err] = {}}
      # For each error, find the positions in $fixed.content of all instance.
      # These change between edits; save to to $spellfixes, but not to file.
      errors.each do |err|
        # Note, the following wizardry is how we make an array of all the
        # positions of all the matches for err in the content of $fixed. Gee!
        positions =
          $fixed.content
                .enum_for(:scan, err).map { Regexp.last_match.begin(0) }
        # Toss partial matches, like "oi" in "oil" and "tions" in "sections".
        # Strategy: iterate positions; toss position if position - 1 is a
        # letter, or of position + length is a letter.
        positions.reject! do |pos|
          ($fixed.content[pos-1].match(/[\w]/) ||
            $fixed.content[pos+err.length].match(/[\w]/))
        end
        # Save position data to $spellfixes. Accepts replacement string.
        positions.each do |pos|
          # Hash structure: $spellfix['badstring'][position #] = 'fix'
          $spellfixes[err][pos] = ''
        end
      end
    end # Note, compiling positions is done once per execution of the script,
        # until the user declares the spell check done.

    # Simply a wrapper. (Used to have more stuff.)
    while dospellcheck
      dospellcheck = review_errors
    end

    # Write changes to $fixed (for saving at end of script).
    commit_spelling_changes

    print "\nMark spellcheck 100\% complete? <Enter> for 'Yes': "
    answer = gets.chomp
    if answer == ''
      $editdata[$v]['spellcheckDone'] = true
    end
  end
end
