def prepare_files



# CREATE ALL DIRECTORIES USED BELOW.
# Prepare ZWI and archives folders and files.
Dir.mkdir("#{$wkdir}zwi") unless Dir.exist?("#{$wkdir}zwi")
Dir.mkdir("#{$wkdir}zwi/data") unless Dir.exist?("#{$wkdir}zwi/data")
Dir.mkdir("#{$wkdir}zwi/data/media") unless
                                Dir.exist?("#{$wkdir}zwi/data/media")
Dir.mkdir("#{$wkdir}zwi/data/media/images") unless
                                Dir.exist?("#{$wkdir}zwi/data/media/images")
Dir.mkdir("#{$wkdir}zwi/data/media/scans") unless
                                Dir.exist?("#{$wkdir}zwi/data/media/scans")
Dir.mkdir("#{$wkdir}zwi/data/css") unless
                                Dir.exist?("#{$wkdir}zwi/data/css")
Dir.mkdir("#{$wkdir}archive") unless
                                Dir.exist?("#{$wkdir}archive")
Dir.mkdir("#{$wkdir}archive/drafts") unless
                                Dir.exist?("#{$wkdir}archive/drafts")
Dir.mkdir("#{$wkdir}archive/oldscans") unless
                                Dir.exist?("#{$wkdir}archive/oldscans")
# Copy CSS to zwi/data/css. Recopied each time script is run.
FileUtils.cp("script/poststyle.css", "#{$wkdir}zwi/data/css/poststyle.css")


# General prep.
# Remove all these useless <div> elements—essential for <article> and
# <section> to be used as expected below. Also delete useless <span
# class="font<n>"> as well.
divs_removed = false
$doc.css('div').find_all.each do |div|
                                   # NOTE: enumerate *allowed* div's here.
  if ! (div.to_html.match(/display\:\s*flex/) or
        div.to_html.match(/dual\-images/) )
    print "Removing <div> cruft:\n   " unless divs_removed
    divs_removed = true
    print "<div>"
    div.replace(div.children)
  end
end
puts if divs_removed
spans_removed = false
$doc.css('span[@class]').each do |span|
  if span['class'].match?(/font\d+/) and span['style'].nil?
    print "Removing <span> cruft:\n   " unless spans_removed
    spans_removed = true
    print "<span>"
    span.replace(span.children)
  end
end
puts if spans_removed


# SCANS PREP: check if scans work is done.
# Used in a few places below.
approved_image_types = %w{jpg jpeg JPG JPEG png PNG tif tiff TIF TIFF}
# First, check if scans have already been moved and renamed. If so, set
# scans_dir_ok to "true" so user can skip moving and renaming them.
scans_dir_ok = false
if Dir.exist?("#{$wkdir}zwi/data/media/scans") &&
   ! Dir.glob("#{$wkdir}zwi/data/media/scans/*").empty?
  scansfiles = Dir.glob("#{$wkdir}zwi/data/media/scans/*")
  # And first first, check if the filenames there are all groovy.
  if scansfiles.any? { |fn|
                ! fn.match?(/\d+\.(#{approved_image_types.join('|')})/) }
    puts rap(%{\n*** PLEASE READ THE WHOLE THING: While a scans directory exists at /zwi/data/media/scans, there are files in it that are NOT of the form, for example, "1.jpg" or "234.png" (numbers only + acceptable image extension). IF you are confident the other files in the directory are acceptably named; are in order; and are such that the filename of the scan corresponds to the page number of the book scanned, THEN simply remove the file(s) that are badly named (they don't belong here).\n\nHowever, if you have put all your original scans with all the original names inside /zwi/data/media/scans, and you are running this script on some ABBYY output (including any images and your original scans) for the first time, then probably you tried creating the /zwi/data/media/scans directory yourself, by hand. Don't bother doing that. The script will do it for you. Just put all your original scans, with their original names (as long as they are in proper alphanumeric order) in /scans. The script will create all necessary directories.\n\nQuitting. Sorry.})
    exit
  else
    scans_dir_ok = true
  end
end

# Execute the following only if the scans dir is *not* already populated.
unless scans_dir_ok
  # At this point, we've confirmed the user needs to move some scans from
  # scans/ to zwi/data/media/scans/.

  # First, check if initial scans are in a scans/ folder. If not, exit.
  scansdir = File.join($wkdir, "scans", "**",
                       "*.{#{approved_image_types.join(',')}}")
  scannames = Dir.glob(scansdir)
  unless Dir.exist?("#{$wkdir}scans") && ! scannames.empty?
    puts rap(%{\n*** ERROR: SCANS MISSING. This script requires the use of scans in order to determine page numbers. Please get the original scans and place them in a new 'scans' subdirectory of the directory where you put the ABBYY html file. (Do not put the html file inside the 'scans' directory.) Make sure each page of 'scans' is in the original order of the page numbers, or just renamed with the page number, as '74.jpg' (but we will do this for you automatically, as long as the images are in order). The scans as outputted by AFR should be properly numbered.\n\nQuitting. Sorry. })
    exit
  end
  puts "Found #{scannames.count} image files in 'scans/'."
  # OK: confirmed that scans have been found in scans/.

  # Rename scans. Begin by asking the user to make sure the image files are
  # in order.
  scannames = scannames.sort_by{|s| s.scan(/\d+/).map{|s| s.to_i}}
  # Determine whether *all* the files in the $wkdir/scans/ dir are OK.
  # If there is still a $wkdir/scans/ dir at all, then probably not.
  unless scannames.all? { |filename|
         filename.match?(/scans\/\d+\.(#{approved_image_types.join('|')})/) }
    puts rap(%{\nOK, we found some scans in the 'scans/' folder that had nonstandard filenames. If you haven't looked at the order in which they appear, here are some rules: (1) they should be put in numerical or alphabetical order so that the scans are ordered exactly as the pages are (so the scan of p. 1 is the first in your sorting order, the second is p. 2, etc.); (2) do not skip pages or delete pages when editing ABBYY (if you output pages from ABBYY); this is not essential but will make for fewer mistakes in page numbering for you to correct.})
    print "\n<Enter> to continue or 'q' to quit: "
    answer = gets.chomp
    exit if answer == 'q'
    puts rap(%{\n*** Groovy. Renaming the files. Get ready: make sure files are in alphanumeric order.})
    isanum = false
    pagenum = 0
    while isanum == false do
      system("open '#{scannames[0]}'")
      print "\nLook at the first scan.\nWhat page number is it of? "
      pagenum = gets.chomp
      puts "You said page #{pagenum}."
      pagenum = pagenum.to_i
      if pagenum > 0
        isanum = true
      else
        puts "*** A numeral > 0, please."
      end
    end

    # Now actually do the renaming; put in proper alphanumeric order first.
    newnames = {}
    extension = ''
    scannames.each_with_index do |filename, i|
      filename.match(/.+?\.(#{approved_image_types.join('|')})/)
      newnames[filename] =
                        "#{$wkdir}zwi/data/media/scans/#{pagenum + i}.#{$1}"
      extension = $1
    end
    # Move old images to original_scans. Copy to new filenames.
    Dir.mkdir("#{$wkdir}archive/oldscans") unless
                                  Dir.exist?("#{$wkdir}archive/oldscans")
    scannames.each do |filename|
      filename.match(/#{$wkdir}scans\/(.*)$/)
      FileUtils.mv("#{$wkdir}scans/#{$1}", "#{$wkdir}archive/oldscans")
      FileUtils.cp("#{$wkdir}archive/oldscans/#{$1}",
                   "#{newnames[filename]}")
    end
    FileUtils.rm_r("#{$wkdir}scans") if Dir.glob("#{$wkdir}scans/*") == []

    # Ask user to double-check.
    goon = false
    all_scans = Dir.glob("#{$wkdir}zwi/data/media/scans/*").sort_by do |path|
      path.scan(/\/(\d+)\.\w+$/)[0][0].to_i
    end
    system("open '#{all_scans[-1]}'")
    print rap %{\nIs the last page, #{all_scans[-1].scan(/\/(\w+\.\w+)$/)[0][0]}, correctly named? <Enter> for yes: }
    goon = (gets.chomp! == '' ? true : false)
    # Repeatedly check in case there were repeated pages skipped or added.
    while goon == false
      puts rap %{We reckon there are two ways this could have happened. Either (a) one or more pages, which were numbered in the original text, were not included among the scans (probably because they were blank); or (b) one or more pages, which were not numbered in the original text (because, for example, they are plates), were included among the scans.}
      print "\nAre we dealing with situation (a) or (b)? Type a or b: "
      answer = gets.chomp
      if answer == 'a'
        # Solicit last correct page number and number of pages skipped.
        print rap %{\nType the last *correctly*-named image (in which the filename equals the page number in the image), a space, and then the number of pages skipped (e.g., 65 1 means that the scan of page 65 is correctly titled '65.jpg'; then one page, page 66, was skipped; and the scan of page 67 is incorrectly titled '66.jpg')); or <Enter> if done: }
        twopages = gets.chomp
        break if twopages == ''
        (puts "\n*** Type, for example, 65 1" and redo) unless
          twopages.match(/(\d+) (\d+)/)
        lastright = $1.to_i     # The last correct filename, according to user.
        correction = $2.to_i    # The number of pages skipped, according to user.
        # Assumes the names are already in the correct format (e.g., '73.jpg').

        # Actually move the files.
        # First, construct an array of the newnames and replace old hash.
        newnames = newnames.values.sort
        newnames.slice!(0..newnames
                    .find_index("#{$wkdir}scans/#{lastright}.#{extension}"))
        # Reverse, in order not to overwrite files. All numbers in filenames
        # are moving up.
        newnames.reverse.each do |badname|
          badname.match(/scans\/(\d+)\.(.+)$/)
          badnum = $1.to_i
          extension = $2
          goodname = "#{$wkdir}scans/#{badnum + correction}.#{extension}"
          FileUtils.mv(badname, goodname)
        end
        puts rap %{\nOK, renamed #{newnames.count} image files. Please check the last image's filename again.}
      elsif answer == 'b'
        puts rap %{\n\nFor now, this script cannot process any pages that are unnumbered. This means that you will have to do a few things by hand:\n\n(1) Delete the scans of pages that lack numbers in the original, so that *only* pages with numbers in the original are in 'zwi/data/media/scans/'. PLEASE DO THIS NOW, while the script is paused.\n\n(2) If, in your editorial judgment, there ought to be page numbers, you will have insert them by hand. For example, if there are two plate pages between p. 16 and p. 17, then you might put <a name="pg16a"></a> and <a name="pg16b"></a> before the content of those plate pages. zwiformat.rb does not do this automatically.\n\nAGAIN, NOW DELETE THE PAGES MENTIONED ABOVE.}
        go_on
        # RENAME FILES AFTER DELETION OF NON-NUMBERED SCANS.
        # Overview: this sorts the filenames in the directory; finds the
        # lowest-numbered and the total file count; steps through the
        # numbers starting at the lowest-numbered, with an upper bound of
        # that number + the file count; when a number is reached such that a
        # corresponding filename is missing (because deleted), then find the
        # lowest-numbered of the remaining filenames; rename it to the
        # current number; then increment and repeat.
        #
        # Get total file count and lowest numbered.
        present_filepaths =
          Dir.glob("#{$wkdir}zwi/data/media/scans/*").sort_by do |path|
            path.scan(/\/(\d+)\.\w+$/)[0][0].to_i
          end
        filenums = present_filepaths.map do |path|
          path.match(/\/(\d+)\.\w+$/)
          $1.to_i
        end
        scan_count = filenums.count
        low_pgnum = filenums[0]
        # Iterate expected numbers.
        (low_pgnum..low_pgnum+scan_count-1).each do |num|
          # Skip this number if file already exists with the number.
          next if Dir.glob("#{$wkdir}zwi/data/media/scans/#{num}*") != []
          # Recalculate present filepaths and filenums, which can change with
          # each iteration.
          present_filepaths =
            Dir.glob("#{$wkdir}zwi/data/media/scans/*").sort_by do |path|
              path.scan(/\/(\d+)\.\w+$/)[0][0].to_i
            end
          filenums = present_filepaths.map do |path|
            path.match(/\/(\d+)\.\w+$/)
            $1.to_i
          end
          # File *doesn't* exist. Fetch next available filename.
          # To do that, toss all filenames lower than num.
          filenums.select! {|fn| fn > num}
          next_available_filenum = filenums[0]
          # Get next_available_filename and extension of file matching
          # next_available_filenum.
          ext = ''
          next_available_filename = ''
          present_filepaths.each do |fp|
            # Get extension from the first matching filename.
            if ! fp.match(/#{next_available_filenum}\.(\w+)$/).nil?
              ext = $1
              next_available_filename = fp
              break
            end
          end
          # Rename that file with current num + ext.
          if ext
            FileUtils.mv( next_available_filename,
                          "#{$wkdir}zwi/data/media/scans/#{num}.#{ext}" )
            all_scans =
              Dir.glob("#{$wkdir}zwi/data/media/scans/*").sort_by do |path|
                path.scan(/\/(\d+)\.\w+$/)[0][0].to_i
              end
            system("open '#{all_scans[-1]}'")
            print rap %{\nIs the last page, #{all_scans[-1].scan(/\/(\w+\.\w+)$/)[0][0]}, now correctly named? <Enter> for yes: }
            goon = (gets.chomp! == '' ? true : false)
          else
            puts "Hmm. We were unable to rename a file. Maybe quit?"
          end
        end
        puts rap %{Very good. We have renumbered the files in 'zwi/data/media/scans/'.}
      else
        puts "Please type a or b."
      end
    end
  end
end


# PREPARE TESSERACT FOLDER AND FILES (=tesstexts) FROM SCANS.
# Create tesstexts.
# Saves the text to files so you don't have to do it again, having done it.
# Enclosing IF statement!
if ! Dir.exist?("#{$wkdir}archive/tesstexts") or
     Dir.glob("#{$wkdir}archive/tesstexts/*").empty?

  # First, ask user for any repeating header and footer info.
  # Guess at header contents (= title?).
  if ! $metadata['Title'] and $metadata['Title'] != ''
    title = $metadata['Title']
  else
    title = $doc.css('title').text
  end
  headers = []
  puts rap %{\n\nWhat header text, if any, is repeated on each (or every other) page? Please look in your scans in the 'scans' directory. An article title is often in the header; I have \"#{title}\" right now. (Don't worry about capitalization or punctuation.)}
  print "\n<enter> to accept the title, or specify ('none' for none): "
  answer = gets.chomp
  headers << (answer == '' ? title : (answer == 'none' ? '' : answer) )
  print "\nAnything other repeating header/footer?\nSpecify or <Enter> for 'no': "
  answer = gets.chomp
  headers << answer unless answer == ''

  # Iterate through scans, getting and massaging text.
  scanfilenames =
    Dir.glob("#{$wkdir}zwi/data/media/scans/*").sort_by do |path|
      path.match(/(\d+)\.\w+$/)
      $1.to_i
    end
  tesstext_with_cols = ''       # Used in dehyphenator.
  puts "*** Running tesseract on scans. Please wait."
  scanfilenames.each do |file|
    # Extract text and get page number.
    pagenum = file.scan(/(\d+)\.\w+$/)[0][0]
    print "#{pagenum}... "
    begin
      system("tesseract '#{file}' '#{$wkdir}archive/temp' > /dev/null 2>&1")
    rescue StandardError => e
      puts rap %{\n*** ERROR: We tried to run Tesseract on the scans, but encountered an error. Has Tesseract been installed? It is required for this script to work. Consult the error message below. Quitting.\n}
      puts "Message:\n========"
      puts e.message
      puts "========\nEnd of error message."
      exit
    end
    pagetext = File.read("#{$wkdir}archive/temp.txt")
    next if pagetext.nil?
    file.match(/zwi\/data\/media\/scans\/(\d+?)\.\w+$/)

    tesstext_with_cols << pagetext
    # Compact page, stripping newlines.
    next if pagetext.empty?
    next unless pagetext.length > 100

    # Remove page number from start and end (first 100 chars).
    # For each 'word' in the first 100 chars, test levenshtein distance.
    # If above benchmark, delete. Must be exact if single digit.
    if pagenum.length == 1    # If single-digit num, l dist is meaningless.
      pagetext[0..100].slice!(pagenum)
    else
      # Construct hash of scores.
      tesswordscores = {}
      pagetext[0..100].split(' ').each do |tessword|
        ldscore = ld(tessword, pagenum)
        tesswordscores[ldscore] = []
        tesswordscores[ldscore] << tessword
      end
      # Delete scores > 1.
      tesswordscores.select! { |k, v| k < 2 }
      unless tesswordscores.empty?
        # Choose the first of the lowest-scoring matches to delete.
        lowest_score = tesswordscores[tesswordscores.keys.sort[0]][0]
        rewritten = pagetext[0..100].gsub!(lowest_score,'')
        pagetext.gsub!(pagetext[0..100], rewritten) # Must be an easier way...
        pagetext.gsub!('  ', ' ')
      end
    end

    # Remove headers.
    # This is more complicated but follows a similar principle: for header
    # string of n words, test it against available sequences of n words in the
    # first 100 characters. Delete any matching sequences.
    unless headers.empty?
      headers.each do |header|
        headerwords = header.split(' ')
        # Iterate through word-strings in first 100 chars.
        # Generate strings.
        firstwords = pagetext[0..100].split(' ')
        # Strategy: Iterate firstwords.count - headerwords.count times.
        # Like, if there are 20 words in the first 100 chars, and there are five
        # words in the header, iterate 15 times. Should work.
        #
        # Construct hash of scores.
        phrasescores = {}
        hcount = headerwords.count
        (firstwords.count - hcount).times do |n|
          # Construct phrase.
          thisphrase = firstwords[n..n+hcount-1].join(' ')
          ldscore = ld(thisphrase.downcase, header.downcase)
          phrasescores[ldscore] = []
          phrasescores[ldscore] << thisphrase
        end
        # Delete scores > 1.
        phrasescores.select! { |k, v| k < 8 }
        unless phrasescores.empty?
          # Choose the first of the lowest-scoring matches to delete.
          lowest_score = phrasescores[phrasescores.keys.sort[0]][0]
          rewritten = pagetext[0..100].gsub!(lowest_score,'')
          pagetext.gsub!(pagetext[0..100], rewritten) # Must be an easier way...
          pagetext.gsub!('  ', ' ')
          pagetext.strip!
        end
      end
    end
    # Write text to file so you won't have to do this again.
    FileUtils.mkdir("#{$wkdir}archive/tesstexts") unless
                                Dir.exist?("#{$wkdir}archive/tesstexts")
    File.write("#{$wkdir}archive/tesstexts/#{pagenum}.txt", pagetext)
  end # Now the tesstexts are ready.
  FileUtils.rm("#{$wkdir}archive/temp.txt")       # Not needed anymore.
  puts rap(%{\nCreated #{Dir.glob("#{$wkdir}archive/tesstexts/*").count} texts in 'archive/tesstexts'.})
  File.write("#{$wkdir}archive/tesstext_with_cols.txt", tesstext_with_cols)
  puts "Created 'archive/tesstext_with_cols.txt' for use in dehyphenator."
else
  puts rap(%{Found #{Dir.glob("#{$wkdir}archive/tesstexts/*").count} texts in 'archive/tesstexts'.})
end # ...of enclosing IF statement (way above): this is to avoid repeatedly
    # creating tesstexts.



#######################################
# PAGE NUMBERING CALLS (2a)
# Note, the 'pagesNumbered' boolean is only a test of whether
# insert_page_numbers has been run already. If not, do not run again.
if $editdata[$v]['pagesNumbered']
  puts "Automatic page numbering attempted (not necessarily confirmed)."
elsif page_numbers_all_present(true)    # "true" here = quiet
    puts "Page not marked as numbered, but numbering confirmed. Updating."
    $editdata[$v]["pagesNumbered"] = true
    $editdata[$v]["pageNumberingConfirmed"] = true
else
  puts "*** Assuming page numbering has not been started. Doing now."
  # The script *should not* attempt to insert page numbers a second time.
  insert_page_numbers
end

# The script should not be able to get past this point until all page
# numbers are actually marked up in the HTML.
# It looks like all the pages are numbered in the HTML...
if page_numbers_all_present
  puts "Page numbering confirmed."
  $editdata[$v]["pageNumberingConfirmed"] = true
# ...else, oops, missing page numbers!
# GETTING READY TO EXIT!
else
  # Write $doc to file unless 'draft_for_pagenums.html' already exists or
  # 'draft<n>.html' is the $filename.
  if !File.exist?("#{$wkdir}draft_for_pagenums.html") &&
         !$filename.match?(/draft\d+.html$/)
    $doc = HtmlBeautifier.beautify($doc)
    $doc.chomp!     # Stray \n at end of file removed.
    File.write("#{$wkdir}draft_for_pagenums.html", $doc)
    # The following should save 'pagesNumbered' as true.
    File.write("#{$wkdir}editdata.json", JSON.generate($editdata))
    file_notice =
            "'draft_for_pagenums.html'"
  elsif File.exist?("#{$wkdir}draft_for_pagenums.html")
    file_notice = "'draft_for_pagenums.html'"
  else
    file_notice = "'#{$filename.match(/(draft\d+.html)$/)}'"
  end

  # Inform the user of the bad news.
  puts rap %{\n*** NOTE: Not all the correct locations for the page numbers could be found automatically, or they were not in order. Please refer to the list above to place the missing page anchor(s) in #{file_notice}. Due to KSF policy as well as this script's functionality, we cannot proceed until you have inserted all of the page numbers in this file, and in the correct order. After you have done so, re-run the script, making sure to choose #{file_notice}.\n\nExiting.\n\n}
  exit #!
end
# OUTTA HERE!



#######################################
# INSERT ARTICLE AND DESCRIPTION SECTION TAGS
#
# Replace <p>[[article]]</p> and <p>[[desc]]</p> with <article> and
# <a name="desc"></a>.
# Very simple. Depends on user to write pseudo-tags precisely enough.
puts "Preparing files..."

# Actually replace [[article]] and [[desc]] (to <article> and
# <a name="desc"></a> )
#
# This complex helper does the work. Repeatedly call, until it returns false.
# &convert_pseudotag returns 'true' if pseudotag converted, 'false' if not.
puts "\nConverting [[article]] to HTML5 tags..." unless
                                  $doc.text.match(/\[\[article\]\]/i).nil?
loop do
  break unless convert_pseudotag('article')  # In helpers0.rb.
end

puts "\nConverting [[desc]] to HTML5 tags..." unless
                                    $doc.text.match(/\[\[desc\]\]/i).nil?
# This is MUCH simpler. Simply replace [[desc]] with <a name="desc"></a> .
$doc = Loofah.document($doc.to_html
                            .gsub(/(\[\[desc\]\])/i, '<a name="desc"></a>'))
# If the result is on its own line (surrounded by <p></p>) then move it.
move_lone_desc_to_next_line
from_doc_to_html_and_text_globals            # Commit changes made by helper.

# Copy page numbers inside beginning of <article>s.
# Copy (do not move!) immediately previous page number anchor inside the
# top of <article>. Necessary, esp. when the document has multiple articles
# which are automatically made into separate zwiformat folders.
#
# Strategy: starting from each <article>, iterate through previous siblings
# for matching <a> tags; when found, copy the *last* in the array of matches.
# Warn user to confirm page number at top of article. Copy it into the first
# descendant position of the article.
$doc.css('article').each do |article_node|  # Do this for all <article>s.
  # Skip article and report to reader if already placed.
  if ! article_node.children[0].nil? and
       article_node.children[0].to_html.match?(/(<a name="pg\d+"><\/a>)/)
    print "a" if $doc.css('article').count > 1
    next
  else
    found = false
    testnode = article_node
    initial_case_done = false # First, check the present node in case the
                              # page number is just inside the <article> tag.
    until found               # I.e., a matching pg found for this <article>.
      # Break out of loop if reached end of elements.
      #unless $doc.css('body') == testnode
      #  found = true
      #end
      # First, set the node to test.
      break if testnode.previous_element.nil?
      testnode = (initial_case_done ? testnode.previous_element :
                  testnode)
      # See if previous node (=testnode) contains <a name="pg\d+">.
      a_names = []
      # If testnode.css("a...") has no content, just use the whole to_html;
      if testnode.css("a[@name]").empty?
        a_names << testnode.to_html
      # otherwise, testnode.css has content; use that.
      else
        a_names = testnode.css("a[@name]").map(&:to_html)
      end
      # Actually add page numbers. Strategy varies based on whether this
      # is the initial case.
      if initial_case_done == false
        initial_case_done = true
        # First, make copy of $doc; replace <a name> etc. matches with
        # FOOBARKAKA. This is to see if there is any text between the start
        # of the article and first-occurring page number. NOTE: This means
        # that, at present, the article might not get an initial page number # at all, if wrongly edited.
        foobarhtml = testnode.dup.to_html
        foobarhtml.gsub!(/<a name="pg\d+"><\/a>/, "FOOBARKAKA")
        foobarhtml.gsub!(/\n/, '')
        foobartext = Loofah.document(foobarhtml).text
        # Case: <article> not checked, and there is no text (only
        # whitespace) between the start of <article> and the pagenum. In
        # this case, append the pagenum as appropriate. (Works, I think.)
        if foobartext.match?(/^\s+?FOOBARKAKA/m)
          a_names.each do |a_name|  # First caught please.
            if a_name.match(/(<a name="pg\d+"><\/a>)/)
              article_node.children[0].previous = $1
              # Let user know what's going on.
              print "#{article_node.children[0].to_html.scan(/(\d+)/)[0][0]} "
              found = true
              break
            end
          end
        end
      else              # So, initial case has been done.
        # Case: <article> checked, and either (a) no pagenums found, or
        # (b) there is text between the start of <article> and the pagenum.
        # In either case, find and use most recent pagenum.
        a_names.reverse.each do |a_name|  # Reverse, to catch the last page.
          if a_name.match(/(<a name="pg\d+"><\/a>)/)
            found = true
            article_node.children[0].previous = $1
            # Let user know what's going on.
            print "#{article_node.children[0].to_html.scan(/(\d+)/)[0][0]} "
            break
          end
        end
      end
    end
  end
end
# Commit changes.
from_doc_to_html_and_text_globals


#######################################
# ADD <meta name="generator">.
# If there is already a generator listed (true of ABBYY-generated HTML),
# then simply append to the text therein.
if ! $doc.at_css('meta[@name="generator"]').nil?
  gen = $doc.at_css('meta[@name="generator"]')
  # Exact string expected.
  unless gen['content'].include?(
                          "zwiformat.rb (Knowledge Standards Foundation)")
    $doc.at_css('meta[@name="generator"]')['content'] +=
            "; as edited by zwiformat.rb (Knowledge Standards Foundation)"
    puts "\nAppended to generator notice."
  end
else
  # Construct new generator meta element.
  gen = $doc.create_element('meta')
  gen['name'] = "generator"
  gen['content'] = "zwiformat.rb (Knowledge Standards Foundation)"
  # Append to beginning of <head>.
  $doc.at_css('head').children[0].before(gen)
  puts "Added generator notice."
end



#######################################
# DECIDE WHETHER TO SEPARATE.
# At this point, if $separation_data['Separable'] is nil, i.e., it has not
# been determined whether or not the HTML file in the working directory
# contains one or many articles, then we decide if the document is separable.
# If there are many articles, then:
#   (a) set $separation_data['Separable'] to 'true',
#   (b) save this in "#{$wkdir}separation_data.json",
#   (c) launch &separate_articles (which decides what to do)
# But if just one, then do (a) (but set to 'false') and (b) above, and go on.
# If *none*, then stop in yer tracks and sternly reprimand the user.
if $doc.css('article').nil? or $doc.css('article').count == 0
  puts rap %{\n*** NOTE: We found no <article> markup. This script cannot proceed further until you have remedied the situation.\n\nSorry. Exiting.\n}
  exit
elsif $doc.css('article').count == 1
  puts "\nWe found exactly one article in the working directory. Very good."
  $separation_data['Separable'] = false
  # Record findings in separation_data.json.
  File.write("#{$wkdir}separation_data.json",
              JSON.pretty_generate($separation_data,
              opts = {space: false, space_before: false}))

  # Move from 'originals' to 'zwifolders' if necessary.
  unless $wkdir.include?("zwifolders/")   # Is zwifolders/ in working path?
    separate_articles   # !IMPORTANT
  end

else
  puts "We found #{$doc.css('article').count} articles in this directory."
  $separation_data['Separable'] = true
  # Record findings in separation_data.json.
  File.write("#{$wkdir}separation_data.json",
              JSON.pretty_generate($separation_data,
              opts = {space: false, space_before: false}))
  # Move separate articles to 'zwifolders'.
  separate_articles
end


#######################################
# CONVERT TITLE, AUTHOR, AND OTHER PSEUDOTAGS.
# This has to be at bottom of files_prep because assigning these values, if
# there are multiple articles (just determined), would result in errors.

# Note, $metadata is now loaded at the bottom of load_document1.rb.
if $editdata[$v]['preparedMetadata'] == false
  prepare_metadata
else
  puts "Already converted title, author, and other pseudotags."
end




end # of files preparation
