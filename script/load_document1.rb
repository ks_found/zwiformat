def load_document


# Create draft editdata file (e.g., whether paragraphs have been fixed).
$editdata = {}
$v = '0'             # Version number. Value will be overwritten later.
if ! File.exist?("#{$wkdir}editdata.json")
  # Create metadata file.
  $editdata['0'] = {}
  $editdata['0']['timeStarted'] = Time.now.to_i
  $editdata['1'] = {}
  $editdata['1']['numeralIssuesDone'] = false
  $editdata['1']['letterIssuesDone'] = false
  $editdata['1']['htmlBlobDone'] = false
  $editdata['1']['htmlFixesDone'] = false
  $editdata['1']['complexChangesDone'] = false
  $editdata['1']['pagesNumbered'] = false
  $editdata['1']['pageNumberingConfirmed'] = false
  $editdata['1']['pageNumbersToAdd'] = []
  $editdata['1']['pagesNumbered'] = false
  $editdata['1']['preparedMetadata'] = false
  $editdata['1']['preparedImages'] = false
  $editdata['1']['brokenParsReviewed'] = false
  $editdata['1']['brokenParsReviewed_b'] = false
  $editdata['1']['spellcheckDone'] = false
  $editdata['1']['ignoredWords'] = []
  json = JSON.generate($editdata)
  File.write("#{$wkdir}editdata.json", json)
else
  # Load editdata file into '$editdata'.
  $editdata = JSON.parse(File.read("#{$wkdir}editdata.json"))
end

# Get path/name of file to open.
# $filename = latest file of filename of form "draft\n+.html".
# If none, then prompt user for filename.
$filename = ''
htmlfiles = []
htmfiles = []
htmlfiles = Dir.glob(File.absolute_path("#{$wkdir}*.html")) +
            Dir.glob("#{$wkdir}archive/drafts/*.html")
htmfiles = Dir.glob("#{$wkdir}*.htm") +
           Dir.glob("#{$wkdir}archive/drafts/*.htm")
allfiles = htmlfiles + htmfiles
$draftfiles = []

# First, check to see if there are any html files; if so, check if any have
# well-formed filenames (e.g., draft3.html).
if htmlfiles != []
  $draftfiles = htmlfiles.select {|file| file.match(/draft\d+.html$/)}
  # Sort $draftfiles by the draft number (which requires some regexen).
  unless $draftfiles == []
    $draftfiles = $draftfiles.sort_by do |filename|
      filename.match(/(\d+)/)
      $1.to_i
    end
  end
  if $draftfiles == []
    puts "\nNo draft files (filenames formatted like \"draft1.html\") found.\n"
   $v = '1'     # No need to load data; data for this version already loaded.
  else
    # First, determine number of highest-numbered draft.
    # Whatever the number edited is, the saved draft will be one more
    # than the highest-numbered draft.
    highest_filename = $draftfiles[-1]
    highest_filename.match(/(\d+)\.html$/)
    highest_num = $1
    $v = (highest_num.to_i + 1).to_s       # Always the case!
    # Let user choose which draft to open.
    if $draftfiles.count == 1
      print "\nOpen #{$draftfiles[0].gsub($wkdir, '')}? <Enter> for yes: "
      answer = gets.chomp
      if answer.empty? || "YESYesyes".include?(answer)
        $filename = $draftfiles[0]
        $filename.match(/(\d+)\.html$/)
        # Copy previous version ($v) $editdata to new version!
        $editdata[$v] = $editdata[$1].clone
      else
        puts "\nOK, not opening anything for now."
        exit
      end
    else               # 2+ drafts available. Ask user to choose one to open.
      filelist = ''
      # Sort $draftfiles by draft number.
      $draftfiles.sort_by!{|f| f.match(/(\d+)\.html$/); $1.to_i}
      $draftfiles.each_with_index do |file, i|
        if file.match(/(#{Regexp.quote($wkdir)}archive\/drafts\/)/)
          filelist += "(#{i+1}) #{file.gsub($1, '')} "
        else
          filelist += "(#{i+1}) #{file.gsub($wkdir, '')} "
        end
      end
      puts "\n" + rap(filelist)
      print "\nEdit which? <Enter> for highest number (anything else to cancel): "
      answer = gets.chomp.to_i
      if answer.between?(1, $draftfiles.length)
        $filename = $draftfiles[answer - 1]
        $editdata[$v] = $editdata[answer.to_s].clone
      elsif answer == 0
        $filename = $draftfiles[-1]
        $editdata[$v] = $editdata[highest_num.to_s].clone
      else
        puts "\nOK, canceling."
        exit
      end
    end
  end
end # Now $filename should be draft<n>.html, if user chose it.


# Next, if no filename emerged from the above and if there are indeed any htm
# or html files, let the user choose one of them to edit.
if $filename == '' && allfiles.length > 0
  # If there is only one html file.
  if allfiles.count == 1
    print "\nOpen '#{allfiles[0].gsub($wkdir, '')}'? <Enter> to accept: "
    answer = gets.chomp
    if answer == '' || "YESYesyes".include?(answer)
      $filename = allfiles[0]
      $v = '1'
    end
  else
    filelist = ''
    allfiles.each_with_index do |file, i|
      filelist += "(#{i+1}) #{file.gsub($wkdir, '')} "
    end
    puts "\n" + rap(filelist)
    print "Which html file do you want to edit? <Enter> to cancel: "
    answer = gets.chomp.to_i
    if answer.between?(1, allfiles.length) && answer != 0
      $filename = allfiles[answer.to_i - 1]
    else
      puts "\nOK, canceling."
      exit
    end
  end
# Due to above logic, there must not be any htm or html files,
# i.e., (allfiles.length == 0).
elsif ! $filename.length
  puts "\nThere are no html files in the current directory.\n"
  exit
end

# If script has not exited, filename is a valid filename chosen by the user.
puts "\nOpening '#{$filename.gsub($wkdir, '')}'."

# Ask if user wants to redo previous procedures.
if $editdata[$v]['numeralIssuesDone']   # Don't ask on first run, of course.
  print rap %{\nDo you want to redo previously-performed document checks? This *does not* re-add page numbers, re-review broken paragraphs, examine new images or captions, or reconsider ignored words. If you want to do any of those, start over from scratch.\n<Enter> for 'NO', anything else for 'yes': }
  answer = gets.chomp
  if answer != ''
    $editdata[$v]['numeralIssuesDone'] = false
    $editdata[$v]['letterIssuesDone'] = false
    $editdata[$v]['htmlFixesDone'] = false
    $editdata[$v]['complexChangesDone'] = false
    $editdata[$v]['preparedMetadata'] = false
    $editdata[$v]['spellcheckDone'] = false
  end
end

# INITIALLY LOAD FILE INTO GLOBALS.
# (This almost certainly needs to be cleaned up and simplified. Hasn't been
# changed since I first started working on the first version of the script.)
$html = File.readlines($filename)     # In "html_fixes" is used to edit HTML.
                                      # Will be replaced by $fixed.
# Preserve nonbreaking spaces found in HTML.
$html = encode_nbsp_in_text_array($html)
$htmlblob = File.read($filename)      # Just used here to make $text (q.v.).
$original = $htmlblob.dup
$htmlblob = encode_nbsp_in_string($htmlblob)
# Loofah is a wrapper for Nokogiri that makes scraping even easier.
$doc = Loofah.document($htmlblob)     # Used to be more important. Used to
                                      # look stuff up, before $fixed is
                                      # converted into a Nokogiri object.
$textblob = $doc.text                 # Exported plaintext (using Loofah).
# An array of the "plaintext" lines of the article.
$text = $textblob.split(/[\r\n|\n|\r]/)  # For searching plaintext by line.
$fixed = []       # An array of the "fixed" lines of the article html,
                  # constructed out of $html.
$spellfixes = {}  # Info about specific spelling fixes to do.
$metadata = {}    # Data about the article, to write in metadata.json.


# LOAD EXISTING METADATA IF AVAILABLE.
# Essential for various reasons; this is actually where metadata.json is
# constructed.
if File.exist?("#{$wkdir}../../metadata.json")
  $metadata = JSON.parse(File.read("#{$wkdir}../../metadata.json"))
  puts "Found global 'metadata.json' template. Loaded."
elsif File.exist?("#{$wkdir}../../../metadata.json")
  $metadata = JSON.parse(File.read("#{$wkdir}../../../metadata.json"))
  puts "Found global 'metadata.json' template. Loaded."
else # Warn the user.
  puts rap %{*** NOTE: you need to place a metadata.json template in the grandparent directory. You will not be able to prepare a complete ZWI file until this has been put in the parent directory of the present directory. (The great-grandparent directory is also OK.)}
end
# Merge in the existing ZWI directory's metadata.json file, if it exists.
if File.exist?("#{$wkdir}zwi/metadata.json")
  # Note, colliding values in the merge are *overwritten by* the local ZWI.
  $metadata.merge!(JSON.parse(File.read("#{$wkdir}zwi/metadata.json")))
  puts "Merged with existing zwi/metadata.json (latter values preferred)."
end

# LOAD info.json into $info.
load_info

end
