# Mostly metadata and ZWI methods, used during script finishing routine.
module SaveAndExitHelpers


# Big method...
def prep_article_txt_and_description
  # PREP DESCRIPTION
  # Assume description is inside <p>.
  duper = $fixed.dup  # In order to be able to manipulate text without
                      # messing up the document.
  if duper.at_css('p a[@name="desc"]')
    duper = $fixed.dup  # In order to be able to manipulate text.
    start_node = duper.at_css('p a[@name="desc"]').parent
    # Copy content of node from after <a name="desc"></a> .
    start_node.inner_html = start_node
                            .to_html[/\<a name\=\"desc\"\>\<\/a\>(.*)$/, 0]
    # Continue copying into next nodes.
    next_content = ''
    if start_node.next
      next_content = start_node.next.content +
        (start_node.next.next.nil? ? '' : start_node.next.next.content)
        # That oughta do it LOL
    end
    start_node.content = start_node.content + next_content
    desc_nodes = start_node
  else
    # Fallback: simply start copying from the first paragraph tag.
    desc_nodes = duper.css('article p')
  end

  description = desc_nodes.text[0..350]
  # Borrowed from desc_getter.rb.
  description = description
    .gsub(" ", " ")    # Not sure where < 0xa0 > chars come from.
    .gsub(/\[\w+\]/, "")    # Strip ftnts.
    .gsub(/\s+/, " ")       # Replace one-or-more whitespace with " ".
    .gsub(/\n+/, " ")       # Replace newlines with space.
    .gsub(/\s+/, " ")       # Replace multiple whitespaces with one space.
    .strip
  if description.length > 300
    final_string = description[-41..-1]
    last_period = last_q_mark = last_bang = last_colon = last_semic = 0
    # Find the last matching period-then-' '
    last_period = final_string.rindex(". ") if final_string.match('\. ')
    last_q_mark = final_string.rindex("? ") if final_string.match('\? ')
    last_bang   = final_string.rindex("! ") if final_string.match('\! ')
    last_colon  = final_string.rindex(": ") if final_string.match('\: ')
    last_semic  = final_string.rindex("; ") if final_string.match('\; ')
    # Get the index of the last period, etc.
    last = [last_period, last_q_mark, last_bang, last_colon, last_semic].max
    # Skip manipulation if not spotted in 40 chars.
    if last > 0
      to_delete = final_string[last+1..-1]
      # Delete to_delete from *end* of *desc* string (only).
      description = description.chomp(to_delete)
    end
  end

  # Should exist and be over 100 chars now, but if not...
  if description.length > 0
    puts rap "\nExtracted and will save description:\n#{description}"
    $metadata['Description'] = description
    # Add to <head>, in <meta name="description"> tag.
    if ! $fixed.at_css('meta[@name="description"]').nil?
      $fixed.at_css('meta[@name="description"]')['content'] = description
    else
      node = $fixed.create_element('meta')
      node['name'] = "description"
      node['content'] = description
      $fixed.at_css('head title').before(node)
    end
    if description.length < 100
      puts rap %{\n*** NOTE: Description was just #{description.length} characters. Here it is in full:\n     >>#{description}<<\nThis is not typically long enough to be an encyclopedia article.}
    end
  else
    puts "*** No description found, hence not saved."
  end

  # WRITE ARTICLE.TXT.
  # Needs much less prep than the description.
  # Note, by this point there should be *only one* article in the text.
  # Regardless, it will print only the first article.
  if $fixed.css('article')
    article = $fixed.at_css('article').text
    File.write("#{$wkdir}zwi/article.txt", article)
    puts "\nOverwrote 'zwi/article.txt' with the latest."
  else
    puts rap %{\nNOTE: When editing ABBYY, please surround the entire article with [[article]] and [[/article]] pseudo-tags. Put the opening tag above the title, but in the <body> of the document. It should be placed on its own line, so that ABBYY renders it like this: <p>[[article]]</p>\n\nFor this article, however, in the HTML, go ahead and surround the main body of the article with <article> and </article>. Make sure these are not inside of <p> (paragraph) tags; if there is only one article in the document, the opening tag will likely just below <body> and the closeing tag will be just before </body>.\n\nOnce this is done, please re-run the script on the latest draft (which you edited) and this will produce article.txt (which is necessary for a complete ZWI file).}
  end
end


# Extract subarticle description.
# Given a section, extract the 350 characters that follow
# <a name="subdesc"></a>. From &prep_article_txt_and_description above :-(.
def extract_subdesc(section)
  # Assume description is inside <p>.
  duper = section.dup  # In order to be able to manipulate text without
                       # messing up the document.
  if duper.at_css('p a[@name="subdesc"]')
    start_node = duper.at_css('p a[@name="subdesc"]').parent
    # Copy content of node from after <a name="desc"></a> .
    start_node.inner_html = start_node
                          .to_html[/\<a name\=\"subdesc\"\>\<\/a\>(.*)$/, 0]
    # Continue copying into next nodes.
    next_content = ''
    if start_node.next
      next_content = start_node.next.content +
        (start_node.next.next.nil? ? '' : start_node.next.next.content)
        # That oughta do it LOL
    end
    start_node.content = start_node.content + next_content
    desc_nodes = start_node
  else
    # Fallback: simply start copying from the first paragraph tag.
    desc_nodes = duper.css('article p')
  end

  description = desc_nodes.text[0..350]
  # Borrowed from desc_getter.rb.
  description = description
    .gsub(" ", " ")    # Not sure where < 0xa0 > chars come from.
    .gsub(/\[\w+\]/, "")    # Strip ftnts.
    .gsub(/\s+/, " ")       # Replace one-or-more whitespace with " ".
    .gsub(/\n+/, " ")       # Replace newlines with space.
    .gsub(/\s+/, " ")       # Replace multiple whitespaces with one space.
    .strip
  if description.length > 300
    final_string = description[-41..-1]
    last_period = last_q_mark = last_bang = last_colon = last_semic = 0
    # Find the last matching period-then-' '
    last_period = final_string.rindex(". ") if final_string.match('\. ')
    last_q_mark = final_string.rindex("? ") if final_string.match('\? ')
    last_bang   = final_string.rindex("! ") if final_string.match('\! ')
    last_colon  = final_string.rindex(": ") if final_string.match('\: ')
    last_semic  = final_string.rindex("; ") if final_string.match('\; ')
    # Get the index of the last period, etc.
    last = [last_period, last_q_mark, last_bang, last_colon, last_semic].max
    # Skip manipulation if not spotted in 40 chars.
    if last > 0
      to_delete = final_string[last+1..-1]
      # Delete to_delete from *end* of *desc* string (only).
      description = description.chomp(to_delete)
    end
  end
  puts "Subarticle description:"
  puts description
  return description
end

# Changes that can be made only to text, which Nokogiri will mess up.
def make_final_html_entity_changes(text)
  # Change $ to &#36;
  # Workaround for MathJax's dumb coopting of '$'. Can't do this except in
  # Nokogiri object itself, and Nokogiri insists on converting the entity.
  text = text.gsub('$', '&#36;')
  text = text.gsub('^nbsp;', '&nbsp;')
  return text
end

# From https://github.com/rubyzip/rubyzip
# This is a simple example which uses rubyzip to
# recursively generate a zip file from the contents of
# a specified directory. The directory itself is not
# included in the archive, rather just its contents.
#
# Usage:
#   directory_to_zip = "/tmp/input"
#   output_file = "/tmp/out.zip"
#   zf = ZipFileGenerator.new(directory_to_zip, output_file)
#   zf.write()
class ZipFileGenerator
  # Initialize with the directory to zip and the location of the output archive.
  def initialize(input_dir, output_file)
    @input_dir = input_dir
    @output_file = output_file
  end

  # Zip the input directory.
  def write
    entries = Dir.entries(@input_dir) - %w[. ..]

    ::Zip::File.open(@output_file, create: true) do |zipfile|
      write_entries entries, '', zipfile
    end
  end

  private

  # A helper method to make the recursion work.
  def write_entries(entries, path, zipfile)
    entries.each do |e|
      zipfile_path = path == '' ? e : File.join(path, e)
      disk_file_path = File.join(@input_dir, zipfile_path)

      if File.directory? disk_file_path
        recursively_deflate_directory(disk_file_path, zipfile, zipfile_path)
      else
        put_into_archive(disk_file_path, zipfile, zipfile_path)
      end
    end
  end

  def recursively_deflate_directory(disk_file_path, zipfile, zipfile_path)
    zipfile.mkdir zipfile_path
    subdir = Dir.entries(disk_file_path) - %w[. ..]
    write_entries subdir, zipfile_path, zipfile
  end

  def put_into_archive(disk_file_path, zipfile, zipfile_path)
    zipfile.add(zipfile_path, disk_file_path)
  end
end


# Cleanup old HTML files.
def move_unused_files_to_archive(filename_to_save)
  # When passing $filename (a path), strip everything but the filename itself.
  filename_to_save = filename_to_save.scan(/([\w\.]+?)$/)[0][0]
  # Get list of html files in the working directory.
  htmlfiles_in_working_dir = Dir.glob("#{$wkdir}*.html") +
                             Dir.glob("#{$wkdir}*.htm")
  old_count = htmlfiles_in_working_dir.count
  # Exclude filename_to_save.
  htmlfiles_in_working_dir.reject! {|fn| fn == "#{$wkdir}#{filename_to_save}"}
  # Make the move.
  htmlfiles_in_working_dir.each do |path|
    filename = path.scan(/([\w\.]+?)$/)[0][0]
    FileUtils.mv(path, "#{$wkdir}archive/drafts/#{filename}")
  end
  puts "Moved older HTML file(s)." if htmlfiles_in_working_dir.count > 0
  htmlfiles_in_working_dir.count != old_count ? (return true): (return false)
end

def write_metadata
  # First, check if 'Publisher' is there. If not, a metadata.json template
  # was not found; do not write zwi/metadata.json without that data!
  if $metadata['Publisher'].nil?
    puts "Publisher not found!"
    return
  end
  # Time to add TimeCreated and LastModified.
  if $metadata['TimeCreated'].nil? or $metadata['TimeCreated'] == ''
    $metadata['TimeCreated'] = Time.now.to_i.to_s
  end
  $metadata['LastModified'] = Time.now.to_i.to_s
  # Need to re-sort metadata here.
  format_metadata
  File.write("#{$wkdir}zwi/metadata.json", JSON.pretty_generate($metadata, opts = {space: false, space_before: false}))
  # Remove needless space inside blank arrays.
  mdtextlines = File.read("#{$wkdir}zwi/metadata.json")
  # Actually remove the offending bits.
  mdtextlines = mdtextlines.gsub(/\[\n\n  \]/m, "[]")
  File.write("#{$wkdir}zwi/metadata.json", mdtextlines)
  puts "Saved 'zwi/metadata.json'."
end

# In case new fields were created and appended to bottom, this puts them
# all in the right/expected order.
def format_metadata
  $metadata['GeneratorName'] = 'Zwiformat.rb'
  # All fields, in order.
  fields = %w{ZWIversion GeneratorName Title Lang Content Primary Revisions CollectionTitle Publisher Editors Authors LastModified TimeCreated Categories Topics Ratings Description Comment License SourceURL SourceFileURL LocalFileURL ShortTitle Disambiguator ALternativeTitles InvertedTitle Subarticles Redirect RedirectURL RedirectTitle BookTitle BookTitleLong SeriesTitle BookEdition BookEditors BookVolumeNumber BookPublisher BookCopyrightDate CopyrightBy BookPrintingDate PublicationDate}
  # Iterating in order, reconstruct the data in $metadata in a holder.
  metadata_holder = {}
  fields.each do |field|
    unless $metadata[field].nil?
      metadata_holder[field] = $metadata[field].dup
    end
  end
  # If there are any remaining fields, append them to bottom.
  $metadata.keys.each do |mdkey|
    # Basically, metadata_holder should now have all the fields that
    # $metadata has. But if there are any others in $metadata, copy them
    # to the bottom of metadata_holder.
    next unless metadata_holder[mdkey].nil?
    metadata_holder[mdkey] = $metadata[mdkey].dup
  end
  # Finally, copy the holder into $metadata. No return needed since this
  # manipulates a global.
  $metadata = metadata_holder.dup
end

# Creates zwi/media.json out of current files.
def create_media_json
  # Get array of paths to all data files.
  media_list = Dir.glob("#{$wkdir}zwi/data/**/*.*")
  if media_list.empty?
    puts rap %{*** WARNING: unable to write zwi/media.json, because no media files found. There should be at least a CSS file.}
    return
  end
  # Discard $wkdir and 'zwi/' in order to construct media.json keys.
  media_list_no_zwi = media_list.map do |path|
    path.gsub(/.*\/zwi\//, '')
  end
  media_hash = {}
  # Iterate just the paths that will be saved in media.json.
  media_list_no_zwi.each do |path|
    # But you need to reconstruct the full path to get the content.
    content = File.read("#{$wkdir}zwi/#{path}")
    # Construct the media_hash based on the shortened path.
    media_hash[path] = Digest::SHA1.hexdigest(content)
  end
  File.write("#{$wkdir}zwi/media.json", JSON.pretty_generate(media_hash,
                                 opts = {space: false, space_before: false}))
  puts "Saved 'zwi/media.json'."
end

# In metadata.json, point Content['article.html'] and Content['article.txt']
# to hashes.
def create_article_hashes
  articles = ['article.html', 'article.txt']
  $metadata['Content'] = {}
  articles.each do |article|
    article_text = File.read("#{$wkdir}zwi/#{article}")
    $metadata['Content'][article] = Digest::SHA1.hexdigest(article_text)
  end
  puts "Added article hashes to metadata.json."
end

# Creates signature.json with standard fields and, most importantly, the
# JSON Web Token (JWT). Extensively revised June 2023.
def sign_zwi
  # Values hard-coded for Oldpedia. Should be drawn from DID???
  sig = {"kid": "did:psqr:oldpedia.org#publish",
         "identityName":
           "Oldpedia by the Knowledge Standards Foundation (KSF)",
         "identityAddress":
           "E-mail: info@encyclosphere.org, PO Box 31, Canal Winchester, OH 43110, USA" }
  media_content = File.read("#{$wkdir}zwi/media.json")
  metadata_content = File.read("#{$wkdir}zwi/metadata.json")
  # Create payload of JWT. Also hard-coded.
  timestamp = Time.now
  payload = {
    'iss' => 'Oldpedia by the Knowledge Standards Foundation (KSF)',
    'address' => 'E-mail: info@encyclosphere.org, PO Box 31, Canal Winchester, OH 43110, USA',
    'iat' => timestamp.to_i,
    'metadata' => Digest::SHA1.hexdigest(metadata_content),
    'media' => Digest::SHA1.hexdigest(media_content)
  }
  pk = File.read("identity/publish.private.jwk")
  pk = JSON.parse(pk)
  # Export key.
  key = create_ec_key(pk['x'], pk['y'], pk['d'])
  # Prep header
  header_sig_fields = sig.clone
  header_sig_fields.delete(:identityName)
  header_sig_fields.delete(:identityAddress)
  header = header_sig_fields
             .merge({ typ: 'JWT', jwk: pk.except('d', 'alg', 'kid') })
  # Actually get the signature token.
  token = JWT.encode(payload, key, 'ES384', header)
  sig['token'] = token
  sig['alg'] = 'ES384'
  sig['updated'] = timestamp.gmtime.strftime("%Y-%m-%dT%H:%M:%S.%6N")
  # Write signature.json!
  File.write("#{$wkdir}zwi/signature.json", JSON.pretty_generate(sig))
end

# Grabbed from https://github.com/jwt/ruby-jwt/blob/main/lib/jwt/jwk/ec.rb
# Adapted by Henry Sanger.
def create_ec_key(jwk_x, jwk_y, jwk_d)
  curve = 'secp384r1'
  x_octets = JWT::Base64.url_decode(jwk_x)
  y_octets = JWT::Base64.url_decode(jwk_y)
  point = OpenSSL::PKey::EC::Point.new(
    OpenSSL::PKey::EC::Group.new(curve),
    OpenSSL::BN.new([0x04, x_octets, y_octets].pack(''), 2)
  )
  sequence = if jwk_d
    # https://datatracker.ietf.org/doc/html/rfc5915.html
    # ECPrivateKey ::= SEQUENCE {
    #   version        INTEGER { ecPrivkeyVer1(1) } (ecPrivkeyVer1),
    #   privateKey     OCTET STRING,
    #   parameters [0] ECParameters {{ NamedCurve }} OPTIONAL,
    #   publicKey  [1] BIT STRING OPTIONAL
    # }
    OpenSSL::ASN1::Sequence([
                              OpenSSL::ASN1::Integer(1),
                              OpenSSL::ASN1::OctetString(OpenSSL::BN.new(JWT::Base64.url_decode(jwk_d), 2).to_s(2)),
                              OpenSSL::ASN1::ObjectId(curve, 0, :EXPLICIT),
                              OpenSSL::ASN1::BitString(point.to_octet_string(:uncompressed), 1, :EXPLICIT)
                            ])
  else
    OpenSSL::ASN1::Sequence([
                              OpenSSL::ASN1::Sequence([OpenSSL::ASN1::ObjectId('id-ecPublicKey'), OpenSSL::ASN1::ObjectId(curve)]),
                              OpenSSL::ASN1::BitString(point.to_octet_string(:uncompressed))
                            ])
  end
  OpenSSL::PKey::EC.new(sequence.to_der)
end

# Gets article title, replaces spaces and other disallowed chars with '_'.
# Does *not* append '.zwi'.
def name_zwi(title = '')
  # When called without a param, this method constructs the zwi name from
  # the contents of $metadata['Title'].
  if title == ''
    title = (!$metadata['Title'].nil?) ? $metadata['Title'].dup :
                                         'Title_Not_Found'
  end
  # Remove characters that shouldn't be in URLs or ZWI names.
  title.gsub!(/[ \/\|\?\%\*\:\|\'\"\<\>\.\,\;\=\′]/, "_")
  # Let Tate transliterate diacritical forms to ASCII forms.
  title = Tate::transliterate(title)
  # Do it again after Tate makes its changes, in case Tate outputs '?' or
  # anything else that shouldn't be in there.
  title.gsub!(/[ \/\|\?\%\*\:\|\'\"\<\>\.\,\;\=\′]/, "_")
  # Remove any doubled underscores.
  title.gsub!(/__/, '_')
  # Remove preceding/concluding '_' which looks bad.
  title = title.chomp('_').reverse.chomp('_').reverse
  # Remove preceding/concluding '-' which looks bad.
  title = title.chomp('-').reverse.chomp('-').reverse
  return title
end

# Actually create/update the ZWI file. First, make remaining components.
def write_zwi
  # Create hashes for article.html and article.txt, placed in metadata.json.
  create_article_hashes
  # Write metadata to file; should be ready now.
  write_metadata
  # Create bunch of pointers to hashes of all media files (=media.json).
  create_media_json
  # Actually sign the ZWI file.
  sign_zwi
  # Delete zip file if it exists.
  output_file = "#{$wkdir}#{name_zwi}.zwi"
  File.delete(output_file) if File.exist?(output_file)
  # Make into zip file.
  directory_to_zip = "#{$wkdir}zwi"
  ZipFileGenerator.new(directory_to_zip, output_file).write()
  puts "Saved zwi file."
end

# Since the CSS and image links are relative and are being prepared for the
# ZWI file, and make a copy of the (possibly updated) data folder and put
# it in the working directory.
def make_copy_of_data_folder
  FileUtils.rm_rf("#{$wkdir}data")
  FileUtils.cp_r("#{$wkdir}zwi/data", "#{$wkdir}data",
                 remove_destination: true)
end

# Simply pushes the file to an aggregator (found in the grandparent
# directory's info.json file).
def push_to_aggregator(autoaccept: false)
  unless autoaccept
    puts "Attempting to submit to:\n     #{$info['AggregatorURL']}"
    print "Press <Enter> to proceed or anything else to exit: "
    if gets.chomp != ''
      return
    end
  end
  begin
    puts "Executing:\ncurl -X POST -F zwi=@'#{$wkdir}#{name_zwi}.zwi' -F pass=#{Shellwords.escape($info['AggregatorPassword'])} #{$info['AggregatorURL']}"
    success = `curl -X POST -F zwi=@'#{$wkdir}#{name_zwi}.zwi' -F pass=#{Shellwords.escape($info['AggregatorPassword'])} #{$info['AggregatorURL']}`
  rescue StandardError => e
    puts rap %{\n*** ERROR: unable to submit ZWI file.\n}
    puts "Message:\n========"
    puts  e.message
    puts "========\nEnd of error message."
  end
  if success.include?('"Success": true')
    puts "\nSuccess!\n#{success}"
    puts "\nArticle should appear soon at:\n#{$metadata['SourceURL']}"
    unless autoaccept
      print "\nView article on website? <Enter> for yes: "
      answer = gets.chomp
      if answer == ''
        exec("open '#{$metadata['SourceURL']}' > /dev/null 2>&1")
      end
      go_on
    end
  else
    puts "\nLooks like the submission did not go through.\n#{success}"
  end
end

# If $wkdir ends with "article\d+/", then rename according to title.
def reset_directory_name
  if $wkdir.match(/(article\d+\/)$/)
    # Calculate new path.
    newpath = $wkdir.gsub($1, "#{name_zwi}/")
    if Dir.exist?($wkdir) and ! Dir.exist?(newpath)
      FileUtils.copy_entry($wkdir, newpath)
      FileUtils.remove_dir($wkdir)
      $wkdir = newpath
      add_new_path_to_paths(get_available_paths, newpath)
      puts "Copied this directory to '#{name_zwi}/'."
    elsif Dir.exist?($wkdir) and Dir.exist?(newpath)
      puts rap %{\n*** There is already an article titled '#{name_zwi}' inside the current directory. The script does not yet have a regularized way of dealing with this issue. Did you perhaps neglect to delete the old directory?\n\nIf the original source has two identically-named entries, then for now, I recommend that you supply a new name in the contents of <span class="title">, such as by adding (1) and (2) to the different, identically-named articles.\n\nSorry, quitting.}
      exit
    end
  end
end

# Invite user to review article by showing scans and, if confirmed finished,
# remove from paths. Eventually this might actually submit to Oldpedia.org.
def review_and_approve
  print "\nPerform final review? <Enter> for yes: "
  answer = gets.chomp
  return unless answer == ''
  puts "\nVery well. Opening scans in order."
  scans = Dir.glob("#{$wkdir}zwi/data/media/scans/*")
  answer = ''
  system("open '#{$wkdir}draft'*'.html' > /dev/null 2>&1")
  scans.each do |scanpath|
    system("open '#{scanpath}'")
    print "<Enter> for next; (r)emove from paths; (q)uit: "
    answer = gets.chomp
    case answer
    when 'q'
      break
    when 'r'
      remove_path_from_list(get_available_paths, $wkdir)
    when ''
    end
  end
  if answer == 'r'   # Already removed; don't need to ask again.
    exec('ruby zwiformat.rb')
  elsif answer == 'q'
    return
  else
    print "<Enter> to remove from paths and restart, or (q)uit: "
    answer = gets.chomp
    if answer == ''
      remove_path_from_list(get_available_paths, $wkdir)
      puts "We will remove the path from the path list, and restart."
      return true
    end
  end
end



end
