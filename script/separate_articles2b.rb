def separate_articles


#########################################################################
# PRELIMINARY

# Report to user, and get permission from user to proceed with separation.
# User can exit script from this method. In separation_helpers.

# This does nothing but give notices to the user and get user permission to
# proceed. Basically, the notices depend on whether the article is separable
# or not; whether $separation_data['SeparationComplete'] is defined or not (if
# not, this is being run for the first time); and whether 'SeparationComplete'
# is true or false (if true, exit; if false, warn).

# CASE: SEPARABLE
if $separation_data['Separable']
  # Consider subcases in which 'SeparationComplete' is defined.
  if ! $separation_data['SeparationComplete'].nil?
    # SUBCASE: separation has been marked complete for this document.
    if $separation_data['SeparationComplete'] == true
      puts rap %{\n*** It looks like this document, which has multiple articles, has already been successfully separated. You should have no occasion to revisit this document.}
      puts "\nExiting."
      exit

    # SUBCASE: separation has been *started* but not completed (so,
    # 'SeparationComplete' is marked as false).
    else
    # If so, WARN. (Maybe later, automatically delete after prompt.)
    puts rap %{\n*** WARNING: Separation has started, but not completed, for this document. Probably what you should do is remove any existing "separated zwifolders" from zwifolders/ (they would be titled "article<n>", like "article3"). Then restart the script, and ignore this message when it re-appears.}
    end

  # SUBCASE: 'SeparationComplete' undefined; separation has not been started.
  else $separation_data['SeparationComplete'].nil?
    # If so, INTRO USER TO WHAT'S ABOUT TO HAPPEN.
    puts rap %{\nARTICLE DIVISION:\nThis document contains #{$doc.css('article').count} articles (according to the <article> tags found). We will next proceed to "separate" this document into its component articles. This is a THREE-STEP PROCESS:\n(1) The script will move the articles to #{$doc.css('article').count} individual folders of this form: zwifolders/article<n>/\n(2) You will edit each created folder individually, also using this script. (The newly-created folders will be added to the top of the "paths" list that displays when you start the script.) When you do this editing, the script will detect the article's title, for you to confirm, and once confirmed, the whole folder will be renamed according to article title. Then, finally:\n(3) You can finish editing the article.\n\nNote, it is *your* responsibility to finish this process, although the script should help.\n}
  end

  # Get user permission to begin
  print "\nAre you ready to separate the articles? <Enter> for yes: "
  answer = gets.chomp
  if answer == ''
    puts "\nProceeding.\n"
    $separation_data['SeparationComplete'] = false
  else
    puts rap %{\n*** Very well, exiting for now. (Dividing the articles is the only thing that can be done with this document.)\n}
    exit
  end

# CASE: NOT SEPARABLE (i.e., only one article)
else
  # Consider cases in which 'SeparationComplete' is defined.
  if ! $separation_data['SeparationComplete'].nil?
    # SUBCASE: separation (moving single article to zwifolders/) already done.
    if $separation_data['SeparationComplete'] == true
      puts rap %{\n*** It looks like this document, which contains only one article, has already been successfully separated. You should have no occasion to revisit this document.}
      puts "\nExiting."
      exit

    # SUBCASE: separation (of single article) started, but not finished.
    else
    # If so, WARN. (Maybe later, automatically delete after prompt.)
    puts rap %{\n*** WARNING: This article has not been "separated" or extracted from the present document and placed in zwifolders/, but the process WAS started. Probably what you should do is remove any existing "separated zwifolders" from zwifolders/ (they would be titled "article<n>", like "article3"); change the ID number in info.json to what it was before; and delete separation_data.json. Then restart the script, and ignore this message when it re-appears.}
    end

  # SUBCASE: 'SeparationComplete' undefined; separation has not been started.
  else
   # If so, INTRO USER TO WHAT'S ABOUT TO HAPPEN.
    puts rap %{\nARTICLE DIVISION:\nThis document contains exactly one article, according to the <article> tag found. We will next proceed to export this article from where it is to the zwifolders/ directory. This is a THREE-STEP PROCESS:\n(1) The script will move the article to a folder of this form: zwifolders/article<n>/\n(2) You will edit the created folder, also using this script. (The newly-created folder should be added to the top of the "paths" list that displays when you start the script.) When you do this editing, the script will detect the article's title, for you to confirm, and once confirmed, the whole folder will be renamed according to article title. Then, finally:\n(3) You can finish editing the article in the newly-created folder.\n\nNote, it is *your* responsibility to finish this process, although the script should make it easy.\n}
  end

  # Get user permission to begin
  print "\nAre you ready to export the article? <Enter> for yes: "
  answer = gets.chomp
  if answer == ''
    puts "\n*** Proceeding with separation.\n"
    $separation_data['SeparationComplete'] = false
    File.write("#{$wkdir}separation_data.json",
                JSON.pretty_generate($separation_data,
                opts = {space: false, space_before: false}))
  else
    puts rap %{\nVery well, exiting for now. (Exporting the article is the only that can be done with this document.)\n}
    exit
  end

end # END of "not separable"


# Save HTML at this point. Seems prudent, if for no other reason.
File.write("#{$wkdir}draft_for_separation.html",
           HtmlBeautifier.beautify($doc))
puts rap %{I have saved a copy of the HTML, with ID numbers shown in <article> tags, in draft_for_separation.html. Have a look if you like.}
go_on



#########################################################################
# PERFORM SEPARATION.

# Construct hash of article IDs => HTML object (with <head> + <article>).
articles = {}
$doc.css('article').each do |article|
  new_article = $doc.dup
  new_article.at_css('body').children = article
  articles[article['id']] = new_article
end

# First, check that zwifolders/ exists. Exit with warning if not.
if Dir.exist?("#{$wkdir}../../zwifolders")
  puts "I see that zwifolders/ exists. Good."
else
  # It should have been created earlier on zwiformat.rb startup.
  puts rap %{\n*** The zwifolders/ directory seems to be missing. Please create it (or rather, just re-run the script; that should re-create it).}
  puts "\nExiting."
  exit
end

# Prep new directories.
puts rap %{*** Copying to new directories, with names such as '/zwifolders/article00001'. You will need to edit these individually; during that process, the directories will be automatically renamed after the articles.}
paths_to_save = []
# Check that user is working inside /originals, which must exist.
unless $wkdir.include? "originals/"
  puts rap "\nIt's possible the problem is that you're not working inside a /originals folder, which is required. Try moving the HTML file you are working on (or rather, its parent directory) into such a new /originals folder, placed inside the same directory where your metadata.json *template* is found. Then restart.\n\nQuitting."
  exit
end
articles.each do |id, html|
  # First, determine directory path (including name).
  dirpath = File.absolute_path("#{$wkdir}../../zwifolders/article#{id}/")
  # For use in the 'paths' data file used in the script startup.
  paths_to_save << "#{dirpath}/"  # Add slash (removed by &absolute_path).
  # For this directory, see if it exists. If yes, exit.
  if Dir.exist?(dirpath)
    puts rap %{*** I wanted to save article ##{id} to a *brand new* zwifolders/article#{id}/ directory, but I see that it already exists.\n\nUnable to continue, until this directory is deleted.\n\nExiting.}
    exit
  else                    ### MAIN SEPARATION LOGIC ###
    # (1) COPYING FILES.
    # Discover name of HTML's media directory (if any).
    wkdir_dirs = Dir.glob("#{$wkdir}*/")
    mwd_index = wkdir_dirs.index{|d| d.match(/\/([\w\- ]+?_files)\/$/) }
    if mwd_index
      media_files_dir = wkdir_dirs[mwd_index]
      new_media_files_dir = "#{dirpath}/#{$1}"
    end
    # Actually make the new, separated article's directory.
    FileUtils.mkdir(dirpath)
    # Copy over directories.
    FileUtils.cp_r("#{$wkdir}/zwi", "#{dirpath}/zwi")
    FileUtils.cp_r("#{$wkdir}/archive", "#{dirpath}/archive")
    if media_files_dir
      FileUtils.cp_r(media_files_dir, new_media_files_dir)
    end
    File.write("#{dirpath}/article#{id}.html", HtmlBeautifier.beautify(html))

    # (2) GET PAGE NUMBERS (FOR DELETIONS).
    # First, get a list of all the page numbers in the document.
    # (Note, 'html' is a Nokogiri object.)
    pgnames = html.css('a[@name]')
    pgnums = []
    pgnames.select do |node|
      node.to_html.match(/pg(\d+)/)
      pgnums << $1 if $1
    end
    pgnums.sort!

    # (3) DELETE ALL OLDSCANS.
    # Delete all the oldscans, but leave the directory.
    FileUtils.rm(Dir.glob("#{dirpath}/archive/oldscans/*"))

    # (4) DELETE ALL OTHER STUFF.
    # Iterate page numbers and delete unneeded tesstexts and images.
    pgnums.each do |pgnum|
      # (4a) DELETE UNNEEDED TESSTEXTS.
      tesstext_paths = Dir.glob("#{dirpath}/archive/tesstexts/*")
      pages_used = []                   # Will use later again.
      tesstext_paths.each do |path|
        path =~ /\/(\d+)\.txt$/
        pages_used << $1
      end
      # Basically, to find out which to delete, reject the page nums seen in
      # this article.
      to_delete = pages_used.reject{|pg| pgnums.include? pg}
      to_delete.each do |num|
        tdelpath = "#{dirpath}/archive/tesstexts/#{num}.txt"
        FileUtils.rm(tdelpath)                        # Actually delete.
      end

      # (4b) DELETE NEW SCANS.
      # Reuses page number logic from (4a). Should be fine.
      all_scan_paths = Dir.glob("#{dirpath}/zwi/data/media/scans/*")
      to_delete.each do |num|
        sdelpath = Dir.glob("#{dirpath}/zwi/data/media/scans/*")
                      .select{|path| path.match(/\/#{num}\.\w+$/)}
        FileUtils.rm(sdelpath)                         # Actually delete.
      end

      # (4c) DELETE UNNEEDED IMAGES.
      # Now images (no other media yet).
      if new_media_files_dir
        all_image_paths = Dir.glob("#{new_media_files_dir}/*")
        # Basically, to find out which to delete, reject the images linked in
        # this article.
        article_images = html.css('img[@src]')
        article_image_paths = article_images.map{|img|
                                CGI.unescape("#{dirpath}/#{img['src']}") }
        # This leaves just those images *not* used in this article.
        image_paths_to_delete = []
        image_paths_to_delete = image_paths_to_delete + all_image_paths -
                                article_image_paths
        image_paths_to_delete.each do |path|
          FileUtils.rm(path)                          # Actually delete.
        end
      end # Of deleting media files.
    end # Of deleting tesstexts & images, page by page.
  end

  # (5) RECONSTRUCT tesstext_with_cols.txt.
  tesstexts_paths = Dir.glob("#{dirpath}/archive/tesstexts/*").sort
  tesstext_with_cols_txt = ''
  tesstexts_paths.each do |path|
    tesstext_with_cols_txt << File.read(path)
  end
  File.write("#{dirpath}/archive/tesstext_with_cols.txt",
             tesstext_with_cols_txt)

  print "#{id} "
end

#########################################################################
# CLEANUP

# Update $separation_data.
$separation_data['SeparationComplete'] = true
$separation_data['IDs'] = articles.keys.sort
File.write("#{$wkdir}separation_data.json",
            JSON.pretty_generate($separation_data,
            opts = {space: false, space_before: false}))
# Save paths just created.
paths = paths_to_save + get_available_paths
paths.uniq!
# Rewrite paths file.
File.open('./paths', 'w+') {|f| paths.each {|line| f.puts line}}


if $separation_data['SeparationComplete']
  puts rap %{\n\nCONGRATULATIONS. You are done separating out the <article> or <article>s from this document. This version of the document is now finished. Please proceed to the individual article folder(s) that were just created.\n\nNote: for your convenience, the folders have been added to the 'paths' which will show up when the script is started.}
  print "\n\nWould you like to restart? <Enter> for yes: "
  answer = gets.chomp
  exec("ruby zwiformat.rb") if answer == ''
  exit
end


end
