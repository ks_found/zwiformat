# This little script takes NationalFile_20210825.txt (from the census bureau)
# and massages it to get a set of alphabetized, unique place names, excluding
# words already on the U.S. English "long list" (uslong.txt).

puts "Loading big file. Please wait..."
nf = File.readlines('dictionaries/NationalFile_20210825.txt')
puts "Read #{nf.length} lines."

puts "Extracting placenames. Please wait..."
nf2 = []
nf.each do |line|
  # Split line on pipes, select indexes [1] and [5]; put them in new array.
  words_making_up_placenames = line.split('|')[1].split(' ')
  words_making_up_placenames.each {|word| nf2 << word }
  words_making_up_counties = line.split('|')[5].split(' ')
  words_making_up_counties.each {|word| nf2 << word }
end
puts "Initial word array is #{nf2.length} long."

puts "Removing assorted leading and trailing cruft."
nf2 = nf2.map do |word|
  word.gsub!(/^[\(\"\'\#\&\-\/\\\@\‐\–\+]/, '')
  word.gsub!(/[\)\"\'\#\&\-\/\\\@\‐\–]$/, '')
  nf2.delete(word) if word.nil?
  word
end

puts "Tossing duplicates and other bad entries..."
nf3 = nf2.uniq
puts "Word array has #{nf3.length} unique items."

puts "Sorting..."
nf4 = nf3.sort
nf4 = nf4.select {|word| ! word.match(/\d\w/)}
nf4 = nf4.select {|word| ! word.match(/\w\d/)}
nf4 = nf4.select {|word| ! word.match(/^\d+$/)}
nf4 = nf4.select {|word| ! word.match(/^[\d[[:punct:]]]+$/)}
puts "Sorted."

puts "Reading in U.S. dictionary..."
uslong = File.readlines('dictionaries/uslong.txt')
uslong.map(&:chomp!)
uslong.map(&:downcase!)
puts "Read."

puts "Removing words found in U.S. dictionary..."
nf5 = nf4 - uslong
puts "Result: #{nf5.length} unique U.S. placename words."

puts "Writing words to file."
File.open('dictionaries/usplacenames.txt', "w+") do |f|
  f.puts (nf5)
end
puts "Done."
