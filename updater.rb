#############################################################################
#
# PREPARATION
#
#############################################################################

# Load dependencies. MOST OF THESE ARE NOT USED.
require 'rubygems'
require 'bundler/setup'
Bundler.require(:default)

# Libraries called (really just subparts of the single script).
# Notice the numbering scheme. This is the approximate order in which they
# are called/needed.
require './script/helpers0.rb'
include Helpers           # Module so methods can be used globally.
require './script/load_document1.rb'
require './script/files_preparation2.rb'
require './script/insert_page_numbers2a.rb'
require './script/separate_articles2b.rb'
require './script/prepare_metadata2c.rb'
require './script/plaintext_changes3.rb'
require './script/html_fixes4.rb'
require './script/make_nokogiri_changes5.rb'
require './script/massage_images5c.rb'
require './script/check_spelling5d.rb'
require './script/save_and_exit_helpers6.rb'
include SaveAndExitHelpers # Used in final section, mostly metadata/zwi.
require 'fileutils'
require 'json'

system("clear")
puts rap %{
=====

ZWI UPDATER:

This is a stand-alone script (with dependencies inside the /script
libraries) which makes a variety of changes updating existing ZWI files
so they are in line with changes discussed and adopted in May 2023.

This does not overwrite the existing /zwifolders directory, but creates
/zwifolders_new. It is user's responsibility to copy /zwifolders_new to
/zwifolders when comfy with doing so.

This script requires that you input, as a command line argument, the full
path (in quotes as necessary) of the /zwifolders directory you want to
work on.

This script requires that you do these things.
(1) Place a square image file (max 400px x 400px) named colicon.png
    or colicon.jpg in the parent directory of the /zwifolders directory.

=====

}


# LOAD /zwifolders PATH FROM ARGV.
if ARGV[0].nil?
  puts "*** Error: no path was given."
  exit
# No command line path, but user can choose from among 'paths' file paths.
else
  puts "Trying path: '#{ARGV[0]}'"
end

# When verified, load array of dirnames.
zwifolders = ARGV[0].dup
dirs = zwifolders[-1] == '/' ? Dir.glob("#{zwifolders}*") :
                               Dir.glob("#{zwifolders}/*")
# If any subdirectory lacks /zwi/metadata.json, remove and put in alt array.
dirs, rejected = dirs.partition {|d| File.exist?("#{d}/zwi/metadata.json")}
# Show alt array and ask user to confirm.
if dirs.length == 0
  puts "No zwifolders found. Quitting."
  exit
elsif rejected.length > 0
  puts "\nThe following files and directories are not zwifolders:"
  puts "  ===================="
  puts rejected
  puts "  ===================="
  puts "Continue with the #{dirs.length} directories that are zwifolders?"
  print "<Enter> for 'yes', anything else for 'no': "
  answer = $stdin.gets.chomp
  unless answer == ''
    puts "Quitting."
    exit
  end
else
  puts "Found #{dirs.length} zwifolders."
end

# Make copy of directory, and work on that.
# If new zwifolders found, ask to overwrite it.
zwifolders.chomp!('/') if zwifolders[-1] == '/'
if Dir.exist? ("#{zwifolders}_new/")
  puts "Spotted: #{zwifolders}_new/\n"
  print "Overwrite? <Enter> for 'yes': "
  answer = $stdin.gets.chomp
  unless answer == ''
    puts "Quitting."
    exit
  end
  puts "Please wait..."
  FileUtils.rm_r("#{zwifolders}_new/")
end

puts "Copying '/zwifolders' to '/zwifolders_new'"
FileUtils.cp_r(zwifolders, "#{zwifolders}_new/")



#############################################################################
#
# ITERATE ZWIFOLDERS
#
#############################################################################

# This is the all-encompassing general ZWI massager. It should call other
# methods when things get complicated. It iterates through ZWI folders
# (i.e., each directory corresponds to one ZWI file/article).
def massage_zwi(fold)
  # Note, it resets the important $wkdir "working directory" global which is
  # used *a lot* in the Zwiformat.rb script libraries.
  $wkdir = fold + (fold[-1] == '/' ? ':' : '/')
  # Make a copy of the collection (encyclopedia) icon which is required to
  # be located in the grandparent directory.
  move_colicon
  # Important. Actually edits the metadata.
  edit_metadata
  # Regenerates everything. SHOULD work.
  write_zwi
end

# Move colicon.png/jpg if not in /zwi/data.
def move_colicon
  root_dir = File.dirname(File.dirname($wkdir))
  # Is the file already in the ZWI dir?
  unless Dir.glob("#{$wkdir}zwi/data/*.{png,jpg}").any?
    # If no, does the file to move exist in the root dir?
    if Dir.glob("#{root_dir}/colicon.{png,jpg}").any?
      # If yes, move it.
      colicon = "colicon." +
        (File.exist?("#{root_dir}/colicon.png") ? "png" : "jpg")
      puts "Copying colicon."
      FileUtils.cp("#{root_dir}/#{colicon}", "#{$wkdir}zwi/data/")
    # If file doesn't exist in root dir, quit.
    else
      puts rap %{\n*** I need colicon.png placed in the root directory (#{root_dir}) in order to run this script. Quitting.}
      exit
    end
  end
end

# Edit metadata.json, changing a few field names and contents.
def edit_metadata
  # First, grab the contents.
  metadata_path = "#{$wkdir}zwi/metadata.json"
  metadata_file = File.read(metadata_path)
  $metadata = JSON.parse(metadata_file)
  # New version!
  $metadata['ZWIversion'] = '1.4'
  # Change contents of Publisher from foo to oldpedia.org/article/foo.
  # $metadata["Publisher"] = "oldpedia.org/article/#{$metadata["Publisher"]}"
  # Copy BookTitle, if it exists, to CollectionTitle. BookTitle is untouched.
  if $metadata['BookTitle']
    $metadata['CollectionTitle'] = $metadata['BookTitle'].clone
  end
  # Replace CreatorNames and ContributorNames with Editors and Authors.
  if $metadata['CreatorNames']
    $metadata['Editors'] = $metadata['CreatorNames']
    $metadata.delete('CreatorNames')
  end
  if $metadata['ContributorNames']
    $metadata['Authors'] = $metadata['ContributorNames']
    $metadata.delete('ContributorNames')
  end
end


#############################################################################
#
# DO THE THANG.
#
#############################################################################
# Collect dirs to iterate. nzwis = "New ZWI Folders". NO trailing '/'.
nzwis = dirs.map{|d| d.gsub('zwifolders', 'zwifolders_new')}
nzwis.each do |fold|
  puts "\nStarting."
  massage_zwi(fold)
  load_info unless $info # Need to load $info for push_to_aggregator to work.
  push_to_aggregator(autoaccept: true)
  puts "Finished.\n======================\n\n"
end



=begin REQUIREMENTS

For each encyclopedia collection, do the following.
• DONE: Get the 400px by 400px jpg from Henry. Place it in the top-level directory for the encyclopedia (next to the info.json config file and metadata.json template), titled colicon.png or colicon.jpg.
Given a /zwifolders directory, systematically open subdirectories one after another, and for each doing the following:
• DONE: Copy colicon.png from top-level directory to place where we decide (./zwi/data/colicon.png, perhaps).
• DONE: metadata.json:
    ◦ WAITING FOR DECISION: Change contents of Publisher from foo to oldpedia.org/article/foo
    ◦ DONE: Create new field, CollectionTitle, and copy contents of BookTitle to that field. (Leave BookTitle as is.)
    ◦ DONE: Change fieldname CreatorNames to Editors and ContributorNames to Authors. Both should remain arrays/lists.
    ◦ DONE: Order fields appropriately, according to fieldname array.
• DONE: Regenerate media.json and metadata.json.
• DONE: signature.json:
    ◦ DONE: replace the fieldname psqrKid with kid; retain the field contents
    ◦ regenerate token with:
        ▪︎ DONE: kid directly in the header
        ▪︎ DONE: replacing the fieldname identityName with iss in the payload
        ▪︎ DONE: replacing updated with iat in the payload
    ◦ DONE: overwrite signature.json (ensure kid and token are correct now)
    ◦ DONE: regenerate signature, having made the above changes
• DONE: Actually regenerate the ZWI file.
• Re-push the file to oldpedia.org.

=end
