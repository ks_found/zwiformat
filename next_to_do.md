# Doing now

# To do next
- Add overlay feature.

# To do eventually
- Troubleshoot why &nbsp; keeps getting stripped out, and fixed. FIX: Whenever using Loofah.document(), replace '&nbsp;' with a set string (maybe just "nbsp;" without the "&"). Then, whenever converting to text (not sure how to describe or search for this), replace with &nbsp;.
- Remove empty <hN> headers.
- Troubleshoot formatting issues ([[small]] not working, too-small type for unindented hanging list).
- Finish formatting "Roll of Members" article (especially the author pix).
- Write "reset.rb" script to do this, for testing:
  - Reset: (1) info.json: NextID = 1
           (2) delete created zwifolders/ (only)
           (3) delete separation_data.json
- Update "OEDP Policies and Procedures" with small new changes.
- Get Henry to fix CSS problems when the viewport is very narrow and very wide. Should we add support for mobile (when viewport is very narrow, the narrower images maybe should be treated as wide as the viewport)?
- If you don't select the last draft, prompt user to delete all later drafts.
- Add logic for title clashes.
- Add ZWI validator before saving ZWI.
- Add [[indent]].
- Or perhaps spelling fixes remove page numbers in some cases? Page numbers disappeared...or just weren't there, anyway, after being confirmed.
- Spelling bug: when editing several matches, try changing an earlier, then a later. Caused crash.
- Consider asking user about longer paragraphs that end with no punctuation and joining, if there are any left here.
- Create a marked-up "proofing version" of the HTML, which highlights (with diff colors) unrecognized words, weird punctuation (as above), etc. Should be very useful for purposes of proofing.
- Somehow implement MASSIVE look-for-problems regex:
\.\.|♦|\<sup\>|«|»|«|\^|•| \\ | \/| \| |<p> \.| \>| &gt;| [\?\!\.]|[\.\?\!] [a-z]|arc a|[a-z][A-Z]| un |per cent[^\.]| \./|<strong>| 0\.| [\.\,\;\:] |\d\d\d\d\-\d\d
- Additional, but too many false positives: /[\,\;] [A-Z]|<em>/


# FINISHED TASKS
- DONE: Settle upon pre-HTML markup style.
- DONE: Add such markup to images in file.
- DONE: Decide on image naming convention.
- DONE: Create /data, /data/media/, and data/media/images/.
- DONE: Delete `<style>`.
- DONE: Copy CSS to /data/css.
- DONE: Move scans to /scans.
- DONE: Move oldscans to /archive/oldscans.
- DONE: Move all older drafts to /archive/drafts, etc.
- DONE: Move (save) tesstexts in the correct location (archive, I guess).
- DONE: Create new section with interview with user.
- DONE: Finish importing from ../metadata.json.
- DONE: Create /zwi directory.
- DONE: Remove [[title]] etc. after use.
- DONE: Skip naming when done.
- DONE: Get lowercase little words working.
- DONE: Add markup to title and author.
- DONE: Remove both cases from user.txt dictionary. Shouldn't be necessary.
- DONE: Move /data into /zwi.
- DONE: Move /dictionaries into /libs.
- DONE: Rename /libs to /script.
- DONE: In section 2, when prompting for name of the article, ensure this is directly or indirectly determined by the content of [[title]].
- DONE: Fix image CSS (again!).
- DONE: (1) replace `[[fig]]` with `<figure>`, etc. (as described to Nancy)
- DONE: Point CSS to data/... not zwi/data/... and copy data/ to top level.
- DONE: (2) name and repoint images in HTML.
- DONE: (3) make copies of image files with new names, and move to the /data/media/images.
- DONE: What happens if `[[title]]` and `[[author]]` are *not* marked up? Tested.
- DONE: Move article.html to /zwi.
- DONE: Debug code that removed footnote stuff.
- DONE: Figure out how to use `[[top]]`, `[[body]]`, etc.

- DONE: Actually mark up sections for purposes of preparing description and article.txt.
- DONE: Move insert_pageNumbers5a.rb code to section 2 in preparation for big change.
- DONE: Fixed bug in which each new iteration of draft is different.
- DONE: Add logic to the effect that, if the user has missing page numbers, the script will exit and prompt the user to insert them before proceeding.
- DONE: Fix bugs with halting logic and wrongly-numbered first page.
- DONE: Improved page numbering algorithms.
- DONE: Extensive debugging/optimizing of improved page numbering.
- DONE: Convert `[[article]]` and `[[main]]` pseudo-tags to HTML5 tags.
- DONE: Ensure page number tags are found *inside* `<article>`.
- DONE: Actually prepare description.
- DONE: Create article.txt text file.

- DONE: Move `<title>` and `<author>` interpolation to section 2, from section 5b. This was a bunch of work.
- DONE: Improve `<head>`: Add/confirm `<title>`.
- DONE: Add `<meta name="description">`
- DONE: Add/confirm `<meta name="author">`.
- DONE: Move save-and-exit helper methods to separate module & improve metadata.json saving methods.
- DONE: Fix bug with `<article>` placement (just wraps first line).
- DONE: Do testing of the recent `<title>` and `<author>` changes.
- DONE: Add generator: "zwiformat.rb (Knowledge Standards Foundation)"
- DONE: Create media.json.
- DONE: Add "Content" hashes to metadata.json.
- DONE: Create signature.json.
- DONE: Make a .zwi-extension zip file with correct name.
- DONE: Autofixes: “ \w+ ” -> “\w+” and a few others.
- DONE: Lightly proofed (and fixed many errors, and compiled new "to fix automatically" list) advanced draft of article.
- DONE: Actually make final ZWI of "The Geology and Geography of Ohio" and submit to Oldpedia.org.

- DONE: Move user.txt from dictionaries to install-wide location.
- DONE: Replaced `[[article]]` => `<article>` logic.
- DONE: Debug issue with `<figcaption>`
- FIXED: &convert_pseudotag 'section' placement not working so that `[[main]]` is converted.
- FIXED: Path to media in &create_media_json (in exit helpers) is not being detected.
- DONE with BIG task: Edited entire script so it makes use of $wkdir.
- FIXED: Old html files are not moved to archive/drafts/.
- DONE: Page number was not copied after `<article>`.
- DONE: Remove the redundant page number from before `<article>`.
- DONE: Remove empty `<p>`s and bogus caption `<a>`s.
- DONE: If there are 0 misspellings, do not ask user if he wants to review them.
- DONE: Placement of author `</span>` screwed up when adjacent to another `</span>`.
- DONE: When a file lacks <span class="title"> or and [[title]], it should not be possible to continue.
- DONE: Make spellerrors.txt path-relative.
- DONE: Organize to do list.
- FIXED: The script is not checking page numbering properly.
- DONE: Fix all of the more urgent bugs/issues that appeared during switch to $wkdir support.
- DONE: Do quick proof of article, and then produce new ZWI.
- DONE: Change method for saving title of zwi So_It_Is_Like_This.
- DONE: Move zwis/ to zwifolders/ .
- DONE: Plan out multiple-articles-per-file.
- DONE: More carefully think through directory structure.
- DONE: Look for `zwifolders/` as a sibling directory of the parent directory of $wkdir. Prompt user's permission to create it if not found.
- DONE: Replace backtic system calls with Dir, File, and FileUtils calls.
- DONE: If $wkdir is in `/zwifolders`, check if there is only one `<article>`, and if so, go on. If not, stop in consternation.
- DONE: Open scans automatically so user can check.
- DONE: Edit page numbering to handle variable headers.
- DONE: Fix bug with page_numbers_all_present that led to page numbers not being correctly checked.
- DONE: Put IDs on the `<article>`s.
- DONE: Script should not place page numbers at top of article twice.
- DONE: Remove ID from `<section>`s
- DONE: Add "SourceURL" field to metadata, and regenerate three ZWIs.

- DONE: Script must place pagenums inside *all* articles of document, not just the first article of each page.
- DONE: Finish user_permission_to_proceed_given. Launches separation routine.
- DONE: Rename "separate" -> "separate" throughout.
- DONE: begin testing on "General Description of Ohio" article.
- DONE: BIG update, finished with 90% of the separation work: copy files, get page numbers for deletions, delete all old scans, delete unneeded new scans, images, and tesstexts, and reconstruct tesstext_with_cols.txt.
- DONE: FINISHED with separation work! Mostly assorted debugging.
- DONE: Rename /article\d+/ folders after title is determined.
- DONE: Change ID numbers so that there are four digits (prepend 0's).
- DONE: Move "add to paths" functionality to helper and add to reset_directory_title: add new path to the top of the list of paths.
- DONE: At end of script, run &review_and_approve.
- DONE: Fix missing-description bug.
- DONE: Remove accent/prime mark from filenames and <title>.
- DONE: Replace [[main]] logic with [[desc]] and `<a name="description"></a>`
- DONE: Let user restart automatically from end of script.
- FIXED: Fix description capture logic.
- DONE: Actually transfer to one of the new working directories; go through the editing process; fix any bugs.
- DONE: If no paragraph to review, don't ask "Permanently mark them as reviewed?"
- DONE: Update the readme.
- DONE: Fix bug that saves titles_with_underscores to 'Title'.
- DONE: Another round of testing.

- DONE: Fix problem where <a name="desc"> is stripped out.
- DONE: Convert diacritics to ordinary Latin in filenames.
- DONE: Fix problem where <a name="desc"> on its own line is ignored.
- DONE: Concluding period in title should not be removed when a period is used previously in the title. (Thought this was fixed, but apparently not.)
- DONE: When <p><a name="desc"></a></p> occurs on its own line, incorporate it at the beginning of the next line without the <p>s.
- DONE: Debug problem with &create_media_json.
- DONE: Open "draft3" by default, rather than "archive/drafts/draftN.html". Remove "archive/drafts/" from list (I thought this was done).
- DONE: Redundant page numbering in "Acton, John" article: <a name="pg2"></a><a name="pg2"></a>
- DONE: Solve problem about linking to pronunciation guide.
- DONE: Introduce Nancy to script, user testing.
- DONE: Add error message regarding required tesseract at prepare_metadata2c.rb line 182
- DONE: Open relevant scan when examining spelling error.
- DONE: Should skip end of part 2 when user chooses to skip.
- DONE: Always delete the CSS.
- DONE: Don't ask "permanently mark them as reviewed" about paragraph fixing.
- DONE: Added *** to "pay attention" items and tested.
- DONE: Make ZWI-saving default.
- DONE: If \w+ is followed by ' ' followed by a lowercase word, and the result of joining the words is in dictionary, propose fix.
- DONE: In prepare_metadata2c.rb, extract (from user, or with user confirmation): ShortTitle and cross-reference info.
- DEBUGGED: why aren't the comma and space saved properly in RedirectTitle?
- DONE: Saved new metadata fields in proper order.

- DONE: Add 'SourceFileURL' to metadata.json file. Also 'LocalFileURL'.
- DONE: Change metadata.json loading logic so that the pedia-wide metadata.json file is always first loaded.
- DONE: Work with Henry to push articles automatically to Oldpedia.org.
- DONE: Re-produce "A General Description of Ohio" from OCR; use to do another round of testing.
- DONE: Make README more complete/systematic in its installation/usage instructions.
- DONE: Update pseudotag instructions.
- DONE: Document 'SourceFileURL' and other new additions.
- DONE: Fixed issue when extra, non-numbered pages can be found among scans.
- DONE: Submitted two new articles. Updated policy & procedures doc.

- DONE: Add support for a new [[small]] pseudotag.
- DONE: Replace [[subart]] pseudo-tags with <section>.
- DONE: Replace [[subtitle]] pseudotags with <span class="subtitle">.
- DONE: Add subtitle to metadata 'Subarticles' array.
- DONE: Add <a name="<subarticle title>"></a> to <section>.
- DONE: Replace img width with max-width:100%; replace img height with max-height:[whatever "height" was before]. Debug problem with saving this.
- DONE: Add [[subdesc]] (subarticle description) support.
- DONE: Added Slug/Title/Description data structure to metadata: Subarticles.
- DEBUGGED: Determine why footnote markup was removed between "Glacial Man in Ohio" drafts 11 and 12.
- DONE: Go through all the Ohio encyclopedia zwifolders; making (1) centered headers; (2) subarticles; and (3) redoing image dimensions.
- DONE: Make the rest of the footnote paragraph <small>.
- DONE: New footnote support pairs (mutually links) the contents of `[[r<n>]]` and `[[n<n>]]` (e.g., `[[r3]]*[[/f3]]` is linked with `[[n3]]*[[/n3]]` ). Adds <small> as well.
- DONE: Add [[center]] support (adds this class to <p> or <hN>).
- DONE: Design notebox solution to the problem of multiple asterisks.
- DONE: (4) finish editing "The Public Lands of Ohio."
- DONE: Fix bug in which footnote text is deleted after being moved.

- DONE: Test footnotes by redoing them in other articles; make sure they're small if they're small in the original.
- DONE: Properly sort files to run Tesseract on.
- DONE: Should "Emma ABBOTT" short title be "Emma Abbott"? Decide.
- DONE: Fix issue: remove pseudotags from "standards-fixed titles."
- DONE: View pushed article on website.
- DONE: Update documentation (again).

- DONE: Replace dash with en dash when in number ranges; prompt for user input, and/or save in info.json.
- DONE: Fixed many broken paragraphs automatically (new heuristic).
- DONE: Fix words broken across pagebreaks.

- DONE: Do preliminary analysis of scan copying bug.
- FIXED (took literally 1.5 days to track it down): Fix bug in which scans do not appear after page 10. Bug hidden in regex!
- DONE: Fix bug in which the same article is marked as both "pg10" and as "pg11".
- DONE: Allow user to pick folder.

- DONE: Troubleshooted the problem Nancy had with numbers folders not being deleted; couldn't replicate.
- DONE: Remove, from filenames, leading and trailing '-' and '_'
- DONE: Troubleshoot question marks in titles (shouldn't be there).
- DONE: Numbered folders seem not to be deleted by Nancy's system after saving a draft (and creating a named folder. Move folder renamer to positions earlier in saving routine.
- DONE: Order files alphanumerically in file picker, if possible.
- DEBUGGED: When there is no matching [[/article]] tag (or any sort of tag), report the problem to the user and close gracefully.

- DONE: Do another article and do more fixes.
- DONE: Fix problem with page numbering.
- DONE: Add CSS classes for new hanging indent type.
- DONE: Fix aggressive number-next-to-letter checking.
- DONE: Fix removal of <div> surrounding dual images.
- DONE: Fix design of dual images (top align).
- DONE: Fix aggressive </p><p> hyphen removal.
- DONE: Fix another problem with [[article]] and [[desc]].

- FIXED: Finish fixing the page numbering problem. The problem seems to be this: if there is text between the beginning of the <article> text and the pagenum, then that pagenum is currently (incorrectly) taken to be the pagenum of the article. But in that case, what *should* happen is that the pagenum should be taken from the most recent pagenum *before* the <article> start. Note that the following are both correctly done at present: (a) if there are no pagenums in the <article> contents, then look (correctly) to a point previous; (b) if there is no text between the start of <article> and the first pagenum of the <article>, then that pagenum is the pagenum of the article.
- DONE: Regenerate correct articles from 000062 to the end (of musbio A's), and move to the production directory.
- DONE: Fix pgnum probs with musbio A's: check/fix (a) in html, starting pgnum; (b) zwi/data/media/scans/ has correct scans. Don't bother fixing archive/tesstexts/ (but those are wrong in some cases too).
- DONE: Re-push: x Adam de la Hale; x Suzanne Adams; x Agricola Martin; x Albani Marie Emma; x Albert Eugen; x Alberti Domenico; x Nathan Allen; x Altes Ernst; x Ambros August; Andre Johann Anton
- DONE: Get user permission to fix broken paras. etc. (Often they *shouldn't* be fixed.)
- DONE: Add fixed CSS to script/.

- DONE: Find out why 'Not found: <a name="pg108"></a>' keeps popping up when they actually there.
- DONE: Investigate why a missing page number did not result in the script stopping.
- DONE: Give Nancy instructions on how to fix the "B" articles.

- FIXED: Bug in which article on page that ends in hyphen crashes script.
- DONE: Added CSS handling for tables and various small text styles.

2023-07-17
- DONE: Determine best method to solve problem of Too Many ZWIs in a Folder.
- DONE: Generalize references to the "grandfather" folder (look for, e.g., metadata.json references).

- DONE: Test that the script still works: can handle ordinary /originals; can handle ordinary /zwifolders files; can push a valid ZWI file to Oldpedia.org.
- DONE: Test that /zwifolders is created correctly when it doesn't exist.
- DONE: Test that /zwifolders is *not* created when it already exists in great-grandparent position.
- DONE: Test that /zwifolders is *not* created when the user is not inside an /originals folder.
- DONE: Ensure that the metadata.json template and info.json are loaded when in a great-grandparent directory.
