# Say hello.
puts
puts "++++++++++++++++++++++++++++++++++++++++++++++++++++++"
puts "+ Welcome to zwiformat.rb.                           +"
puts "+                                                    +"
puts "+ Careful to save all your changes at end or you'll  +"
puts "+ have to make them all again. *** = pay attention.  +"
puts "++++++++++++++++++++++++++++++++++++++++++++++++++++++"

##############################################################################
# Make sure to run "bundle install" in the directory where zwiformat.rb is
# installed, to ensure that all required gems are included here.
require 'rubygems'
require 'bundler/setup'
Bundler.require(:default)

# Libraries called (really just subparts of the single script).
# Notice the numbering scheme. This is the approximate order in which they
# are called/needed.
require './script/helpers0.rb'
include Helpers           # Module so methods can be used globally.
require './script/load_document1.rb'
require './script/files_preparation2.rb'
require './script/insert_page_numbers2a.rb'
require './script/separate_articles2b.rb'
require './script/prepare_metadata2c.rb'
require './script/plaintext_changes3.rb'
require './script/html_fixes4.rb'
require './script/make_nokogiri_changes5.rb'
require './script/massage_images5c.rb'
require './script/check_spelling5d.rb'
require './script/save_and_exit_helpers6.rb'
include SaveAndExitHelpers # Used in final section, mostly metadata/zwi.
# Note, this script also requires that Tesseract be installed, and assorted
# other files that make up the ZwiFormat script system, such as
# dictionaries, and user-supplied files expected by the script, such as
# an HTML file and a properly-sorted set of scans in 'scans'.


##############################################################################
# SET $wkdir (WORKING DIRECTORY)
paths = get_available_paths

# Set $wkdir, i.e., the directory on which zwiformat.rb will work.
# No path given, no 'paths' file yet.
if ARGV[0].nil? and ! paths
  puts "*** Error: no path was given, and there is no path history yet."
  exit
# No command line path, but user can choose from among 'paths' file paths.
elsif ARGV[0].nil? and paths
  # Prepare string to show user available paths.
  show_available_zwifolders_and_originals(paths)
  # Let user choose from among 'paths' file paths.
  answer_valid = false
  until answer_valid
    print "Choose one of these paths, <Enter> for '1', or 'o' to open new: "
    answer = $stdin.gets.chomp
    exit if answer == 'q'
    answer = 1 if answer == ''
    if answer != 'o' and ! paths[answer.to_i-1].nil?
      $wkdir = paths[answer.to_i-1]
      answer_valid = true
      # Append to top of path array.
      append_to_top_of_path_array(paths)
    end
    # Use neat file-picker gem, terminal-file-picker, to let user pick a
    # file.
    if answer == 'o'
      # Determine directory to start in, based on $wkdir.
      # First, set $wkdir if not already set.
      $wkdir = ($wkdir ? $wkdir : (! paths[0].nil? ? paths[0] : './'))
      # Initialize file-picking object.
      picker = FilePicker.new($wkdir)
      # Call main file-picker method.
      choice = picker.pick_file
      # Validate file choice.
      if choice.match?(/originals\/.+\/.+\.html?$/) or
         choice.match?(/zwifolders\/.+\/.+\.html?$/)
        $wkdir = choice.sub(/(?<=\/)[\w ]+\.html?$/, '')
        # Append to top of path array.
        append_to_top_of_path_array(paths)
        answer_valid = true
      else
        puts rap %{The file you chose, \n#{choice}\ndoes not appear to be compatible with Zwiformat.rb. Please choose an HTM/HTML file located within a subdirectory of a directory titled "/originals" or "/zwifolders".}
      end
    end
  end
else  # Path supplied on command line.
  if Dir.exist?(ARGV[0])    # But is it a dir??
    path = File.expand_path(ARGV[0].chomp)
    add_new_path_to_paths(paths, path)
  else
    puts "Error: '#{ARGV[0].chomp}' is not a directory."
  end
end
ARGV.clear  # Clear, so I don't have to use $stdin before gets.chomp.
$wkdir.match(/.*\/(.+)\/$/)   # Get capture group for reference below.
puts "\nVery well, working on this article:
     #{$wkdir.gsub(/.*\/.+$/, $1)}"

# Load $separation_data.
$separation_data = File.exist?("#{$wkdir}separation_data.json") ?
  JSON.parse(File.read("#{$wkdir}separation_data.json")) : {}

# Create /zwifolders as a sibling of the parent folder, if it does not
# exist yet.
unless Dir.exist?(File.absolute_path("#{$wkdir}/../../zwifolders")) or
       Dir.exist?(File.absolute_path("#{$wkdir}/../../../zwifolders"))
  puts rap %{\nYou are working in:\n#{$wkdir}\nNote, zwiformat.rb is designed to work on files of a specific structure, which it creates itself and places in the /zwifolders directory. The parent of the current working directory is not a /zwifolders directory, nor could we find a /zwifolders that is a sibling of the parent directory.}
  # Ensure that the present folder is inside /originals; exit if not. (The
  # only case in which /zwifolders needs to be created is if the user is
  # presently working inside /originals. If not, something's gone wrong.)
  if Dir.exist?(File.absolute_path("#{$wkdir}/../../originals"))
    puts rap %{Therefore, the script will attempt to extract the necessary article data from the working directory and put it into a new /zwifolders directory.\n*** So we should create /zwifolders first.\n\n}
    print "Agreed? <Enter> for 'yes', anything else to exit: "
    answer = gets.chomp
    if answer == ''
      Dir.mkdir("#{$wkdir}../../zwifolders")
      puts "Very well, created."
    else
      exit
    end
  else
    puts rap %{\n\nAlso, I note both that you lack a /zwifolders directory and that your present working directory, \n#{$wkdir}\is not a descendant of an /originals directory. This makes me nervous, so I am quitting. Make sure your directory structure is set up properly.}
  end
else
  puts "\n'/zwifolders' directory found; excellent."
end


############################################################################## (1) LOAD DOCUMENT

puts "\n"
puts "++++++++++++++++++++++++++++++++++++++++++++++++++++++"
puts "(1) LOAD DOCUMENT"
load_document


############################################################################## (2) FILES PREPARATION
# (Make directories, load CSS from zwiformat files, rename scans; also,
# add <article> and <section class="main"> markup.)

puts "\n"
puts "++++++++++++++++++++++++++++++++++++++++++++++++++++++"
puts "(2) FILES PREPARATION"
prepare_files


############################################################################## (3) PLAINTEXT CHANGES
# Iterate through $text; collect specific items to fix (later).

puts "\n"
puts "++++++++++++++++++++++++++++++++++++++++++++++++++++++"
puts "(3) PLAINTEXT CHANGES"
make_plaintext_changes


############################################################################## (4) BASIC HTML FIXES

puts "\n"
puts "++++++++++++++++++++++++++++++++++++++++++++++++++++++"
puts "(4) BASIC HTML FIXES"
make_basic_html_fixes


############################################################################## (5) COMPLEX CHANGES TO HTML VIA NOKOGIRI OBJECT

puts "\n"
puts "++++++++++++++++++++++++++++++++++++++++++++++++++++++"
puts "(5) MAKING MORE COMPLEX CHANGES TO HTML"
make_nokogiri_changes


##############################################################################
# (6) SAVE AND EXIT
# The logic here is: either there were changes made or not, and either the
# filename is in standard form (like "draft1.html") or not. (1) Make this
# determination. (2) Based on combinations of these values, decide which
# filename to offer to save (= filename_to_save), what notice to show the
# user, and what options to show. Then (3) use a generalized user query
# interface, and (4) use a "case" statement to execute the commands.


# Prepare description and article.txt.
# Remarkably easy once <section class="main"> is in place.
# Go ahead and do this every time; easy to check, in case of updates.
prep_article_txt_and_description

# Key features of the content and filename.
unchanged = false
standard_form = false

# Beautify HTML first. # Note, this makes $fixed into a blob again.
$fixed = HtmlBeautifier.beautify($fixed)  # Ugly indeed without it.

$fixed.chomp!     # Stray \n at end of file removed.

# Make final text-only changes.
$fixed = make_final_html_entity_changes($fixed)

# Check if any changes were made; tell user if so.
if $original == $fixed
  unchanged = true
else
  puts "\n'#{$filename}' changed."
end

# Check if $filename is in form draft<n>.html.
standard_form = true if $filename.match(/\/?draft\d+.html$/)

# Prepare an array of possible actions based on whether $filename is in
# standard form and whether changes were made.

# If $filename is not standard form, the options are actually the same
# regardless of whether changes were made.
filename_to_save = ''
notice = ''
options = []
if ! standard_form
  filename_to_save = 'draft1.html'
  notice = "Whether changed or not, you can save 'draft1.html' and a ZWI."
  options = ["save as both '#{filename_to_save}' and ZWI",
             "save as '#{filename_to_save}'",
             "save all and submit to aggregator", "exit without saving"]

# Changes were made, and the filename is in standard form: increment the
# highest-numbered filename, as the filename to save, and prep options.
elsif ! unchanged
  maxfile = $draftfiles.max do |f1, f2|
    f1.match(/draft(\d+).html$/)
    f1_num = $1.to_i
    f2.match(/draft(\d+).html$/)
    f2_num = $1.to_i
    f1_num <=> f2_num
  end
  maxfile.match(/draft(\d+).html$/)
  # This settles the incremented draft number and inserts in filename to save.
  filename_to_save = "draft#{$1.to_i + 1}.html"
  notice = "If you want to save, we'll save to the next higher numbered draft."
  options = ["save as both '#{filename_to_save}' and ZWI",
             "save as '#{filename_to_save}'",
             "save all and submit to aggregator", "exit without saving"]

elsif unchanged
  filename_to_save = $filename.gsub($wkdir, '')
  notice = "'#{$filename.gsub($wkdir, '')}' is unchanged. You can save a final draft if you like."
  options = ["save as ZWI only", "save all and submit to aggregator",
             "exit without saving"]
end

# Present options for saving (or not), and execute.
execute = ''        # Thang to do.
# First, prepare options string
options_string = ''
options.each_with_index do |option, i|
  options_string += "(#{i+1}) #{option} "
end
puts "\n" + notice
puts "\nOptions:\n" + rap(options_string)
print "\nWhat do you want to do? <Enter> for option (1): "
answer = gets.chomp
answer = (answer == '' ? 1 : answer.to_i)
if answer.between?(1, options.length)
  execute = options[answer - 1]
end

# Actually save whatever the user specified to save.
case execute
when "exit without saving"
  puts "\nOK, exiting without saving."
  exit
when "save as ZWI only"
  File.write("#{$wkdir}zwi/article.html", $fixed)
  reset_directory_name
  write_zwi
  make_copy_of_data_folder
  move_unused_files_to_archive($filename)
  restarting = review_and_approve
  if restarting
    exec('ruby zwiformat.rb')
  else
    puts "\nExiting."
  end
  exit
when "save as '#{filename_to_save}'"
  write_metadata
  File.write("#{$wkdir}#{filename_to_save}", $fixed)
  File.write("#{$wkdir}editdata.json", JSON.pretty_generate($editdata))
  puts "\nSaved '#{filename_to_save}'."
  reset_directory_name
  move_unused_files_to_archive(filename_to_save)
  make_copy_of_data_folder
  puts "\nExiting."
  exit
when "save as both '#{filename_to_save}' and ZWI"
  File.write("#{$wkdir}#{filename_to_save}", $fixed)
  File.write("#{$wkdir}zwi/article.html", $fixed)
  File.write("#{$wkdir}editdata.json", JSON.pretty_generate($editdata))
  puts "\nSaving to both '#{filename_to_save}' and 'zwi/article.html'."
  reset_directory_name
  move_unused_files_to_archive(filename_to_save)
  write_zwi
  make_copy_of_data_folder
  restarting = review_and_approve
  if restarting
    exec('ruby zwiformat.rb')
  else
    puts "\nExiting."
  end
  exit
when "save all and submit to aggregator"
  File.write("#{$wkdir}#{filename_to_save}", $fixed)
  File.write("#{$wkdir}zwi/article.html", $fixed)
  File.write("#{$wkdir}editdata.json", JSON.pretty_generate($editdata))
  puts "\nSaving to both '#{filename_to_save}' and 'zwi/article.html'."
  reset_directory_name
  move_unused_files_to_archive(filename_to_save)
  write_zwi
  make_copy_of_data_folder
  restarting = review_and_approve
  if restarting
    push_to_aggregator
    exec('ruby zwiformat.rb')
  else
    push_to_aggregator
  end
  puts "\nExiting."
  exit
else
  puts "\nOK, exiting without saving."
  exit
end
