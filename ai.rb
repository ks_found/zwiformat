require 'openai'

# Set up your DeepAI API credentials
client = OpenAI::Client.new(api_key: ENV['GPT_API_KEY'])

def clean_html(html, client)
  prompt = "Clean up the following HTML:\n#{html}"

  response = client.completions(
      prompt: prompt,
      max_tokens: 100,
      model: "text-davinci-002"
  )

  if response.success?
    cleaned_html = response.choices[0].text
    return cleaned_html
  else
    puts "Error: #{response.error['message']}"
    return nil
  end
end

# Example usage
html_code = <<~HTML
  <html>
  <head>
  <title>Example Page</title>
  </head>
  <body>
  <h1>Welcome to my website</h1>
  <p>This is a messy HTML code.</p>
  </body>
  </html>
HTML

cleaned_code = clean_html(html_code, client)
puts cleaned_code
